#![cfg_attr(doc, feature(doc_auto_cfg))]

//! Data types and functions related to colors and images for Crystal Ball.

// Use `self` to avoid name conflict with the `image` crate.
pub use self::image::*;
pub use color::*;
use std::sync::Arc;

mod color;
mod image;

/// A general representation of textures.
///
/// This allows accessing of individual pixels of the respective texture.
pub trait Texture: Send + Sync {
    /// Return the [`Color`] value at the given position.
    ///
    /// `u` and `v` should be in the range \[0, 1\].
    /// While the implementation is left to the user,
    /// it is recommended to wrap the texture if `u` or `v` are out of range.
    fn get_pixel(&self, u: f64, v: f64) -> Color;
}

/// A trait allowing an `Arc<dyn Texture>` to be constructed either from a struct
/// implementing [`Texture`] or from an [`Arc`] itself.
pub trait IntoArcDynTexture {
    /// Perform the conversion of `self` into `Arc<dyn Texture>`.
    fn into_arc_dyn_texture(self) -> Arc<dyn Texture>;
}

impl<T: Texture + 'static> IntoArcDynTexture for T {
    fn into_arc_dyn_texture(self) -> Arc<dyn Texture> {
        Arc::new(self)
    }
}

impl<T: Texture + 'static> IntoArcDynTexture for Arc<T> {
    fn into_arc_dyn_texture(self) -> Arc<dyn Texture> {
        self
    }
}

impl IntoArcDynTexture for Arc<dyn Texture> {
    fn into_arc_dyn_texture(self) -> Arc<dyn Texture> {
        self
    }
}
