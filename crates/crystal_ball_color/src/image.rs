use crystal_ball_error::Error;
#[cfg(feature = "gltf")]
use gltf::image::Format;
use image::codecs::hdr::HdrDecoder;
use image::{ImageBuffer, ImageFormat, Rgb};
use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

use crate::{Color, Texture};

/// An image of fixed size represented as a row-major [`Vec`] of [`Color`].
/// In addition, this also stores the [`Interpolation`] used for accessing colors between pixels.
///
/// (0, 0) is considered to be the upper left corner.
#[derive(Clone, Debug)]
pub struct Image {
    pub width: u32,
    pub height: u32,
    pub pixels: Vec<Color>,
    pub interpolation: Interpolation,
}

impl Image {
    /// Create a new (black) [`Image`].
    pub fn new(width: u32, height: u32, interpolation: Interpolation) -> Self {
        Image {
            width,
            height,
            pixels: vec![Color::default(); (width * height) as usize],
            interpolation,
        }
    }

    /// Create a new [`Image`] from an already existing [`Vec`] of [`Color`].
    ///
    /// # Panics
    /// Panics if the dimensions don't match the size of `pixels`.
    pub fn from_pixels(
        width: u32,
        height: u32,
        pixels: Vec<Color>,
        interpolation: Interpolation,
    ) -> Self {
        assert_eq!(
            width * height,
            pixels.len() as u32,
            "image dimensions don't match the data size"
        );

        Image {
            width,
            height,
            pixels,
            interpolation,
        }
    }

    /// Create a new [`Image`] from a byte buffer and a given [`ColorFormat`].
    ///
    /// Colors are assumed to be encoded in sRGB.
    ///
    /// # Panics
    /// Panics if the dimensions don't match the size of `data` (respecting the [`ColorFormat`]).
    pub fn from_raw_with_format(
        width: u32,
        height: u32,
        data: &[u8],
        format: ColorFormat,
        interpolation: Interpolation,
    ) -> Self {
        let offset = format.offset();

        assert_eq!(
            width * height * offset as u32,
            data.len() as u32,
            "image dimensions don't match the data size"
        );

        let pixels = (0..height * width)
            .map(|i| {
                let x = (i % width) as usize;
                let y = (i / width) as usize;

                let start = offset * (x + y * width as usize);

                match format {
                    ColorFormat::Rgb8 | ColorFormat::Rgba8 => Color::new(
                        data[start] as f32 / 255.0,
                        data[start + 1] as f32 / 255.0,
                        data[start + 2] as f32 / 255.0,
                    ),
                    ColorFormat::Rgb16 | ColorFormat::Rgba16 => Color::new(
                        (((data[start + 1] as u16) << 8) | data[start] as u16) as f32 / 65535.0,
                        (((data[start + 3] as u16) << 8) | data[start + 2] as u16) as f32 / 65535.0,
                        (((data[start + 5] as u16) << 8) | data[start + 4] as u16) as f32 / 65535.0,
                    ),
                }
            })
            .collect::<Vec<Color>>();

        Image {
            width,
            height,
            pixels,
            interpolation,
        }
    }

    /// Create a new [`Image`] from a byte buffer.
    ///
    /// [`ColorFormat`] is assumed to be [`ColorFormat::Rgb8`]
    /// and the colors are assumed to be encoded in sRGB.
    ///
    /// # Panics
    /// Panics if the dimensions don't match the size of `data`.
    pub fn from_raw(width: u32, height: u32, data: &[u8], interpolation: Interpolation) -> Self {
        Self::from_raw_with_format(width, height, data, ColorFormat::Rgb8, interpolation)
    }

    /// Load an image from the specified path.
    ///
    /// This method automatically detects whether an image is HDR and performs the necessary conversions.
    /// HDR images are assumed to be encoded in linear RGB while SDR images are assumed to be sRGB.
    ///
    /// Returns an [`Error`] if the image can not be loaded.
    pub fn from_file<P: AsRef<Path>>(path: P, interpolation: Interpolation) -> Result<Self, Error> {
        match ImageFormat::from_path(&path)? {
            ImageFormat::Hdr => Self::from_hdr_file(path, interpolation),
            _ => Self::from_image_file(path, interpolation),
        }
    }

    /// Load an SDR image from the specified path.
    ///
    /// SDR images are assumed to be encoded in sRGB.
    ///
    /// Returns an [`Error`] if the image can not be loaded.
    fn from_image_file<P: AsRef<Path>>(
        path: P,
        interpolation: Interpolation,
    ) -> Result<Self, Error> {
        let image = image::open(path)?.into_rgb32f();
        let (width, height) = image.dimensions();

        let mut pixels = vec![Color::default(); (width * height) as usize];
        pixels.iter_mut().enumerate().for_each(|(i, color)| {
            let x = i % width as usize;
            let y = i / width as usize;

            let pixel = image.get_pixel(x as u32, y as u32);
            *color = Color::from(pixel.0).srgb_to_linear();
        });

        Ok(Image {
            width: image.width(),
            height: image.height(),
            pixels,
            interpolation,
        })
    }

    /// Load an HDR image from the specified path.
    ///
    /// HDR images are assumed to be encoded in linear RGB.
    ///
    /// Returns an [`Error`] if the image can not be loaded.
    fn from_hdr_file<P: AsRef<Path>>(path: P, interpolation: Interpolation) -> Result<Self, Error> {
        let decoder = HdrDecoder::new(BufReader::new(File::open(path)?))?;

        let width = decoder.metadata().width;
        let height = decoder.metadata().height;

        let image = decoder.read_image_hdr()?;

        let mut pixels = vec![Color::default(); (width * height) as usize];
        pixels.iter_mut().enumerate().for_each(|(i, color)| {
            let x = i % width as usize;
            let y = i / width as usize;

            let pixel = image[x + y * width as usize];

            *color = Color::new(pixel.0[0], pixel.0[1], pixel.0[2]);
        });

        Ok(Image {
            width,
            height,
            pixels,
            interpolation,
        })
    }

    /// Return the dimensions of the image.
    pub fn dimensions(&self) -> (u32, u32) {
        (self.width, self.height)
    }

    /// Return the color value of the pixel `(x, y)`.
    ///
    /// The image is considered to wrap periodically and is thus virtually infinite.
    pub fn get_pixel(&self, x: f32, y: f32) -> Color {
        match self.interpolation {
            Interpolation::Closest => self.interpolate_closest(x, y),
            Interpolation::Bilinear => self.interpolate_bilinear(x, y),
        }
    }

    /// Return the closest color value at the position `(x, y)`.
    fn interpolate_closest(&self, x: f32, y: f32) -> Color {
        self.get_pixel_periodic(x.round(), y.round())
    }

    /// Return the bilinearly interpolated color value at the position `(x, y)`.
    fn interpolate_bilinear(&self, x: f32, y: f32) -> Color {
        let x1 = x.floor();
        let x2 = x.ceil();
        let y1 = y.floor();
        let y2 = y.ceil();

        let (weight1, weight2) = if x1 == x2 {
            (1.0, 0.0)
        } else {
            ((x2 - x) / (x2 - x1), (x - x1) / (x2 - x1))
        };

        let color1 =
            self.get_pixel_periodic(x1, y1) * weight1 + self.get_pixel_periodic(x2, y1) * weight2;
        let color2 =
            self.get_pixel_periodic(x1, y2) * weight1 + self.get_pixel_periodic(x2, y2) * weight2;

        if y1 == y2 {
            color1
        } else {
            (color1 * (y2 - y) + color2 * (y - y1)) / (y2 - y1)
        }
    }

    /// Return the wrapped color value at the position `(x, y)`.
    fn get_pixel_periodic(&self, x: f32, y: f32) -> Color {
        self.pixels[x.rem_euclid(self.width as f32) as usize
            + self.width as usize * y.rem_euclid(self.height as f32) as usize]
    }

    /// Set the color value of the pixel `(x, y)`.
    ///
    /// # Panics
    /// Panics if `(x, y)` ist out of bounds.
    pub fn set_pixel(&mut self, x: u32, y: u32, color: Color) {
        assert!(y < self.height);
        assert!(x < self.width);

        self.pixels[(x + y * self.width) as usize] = color;
    }

    /// Convert the image from linear to sRGB space.
    pub fn linear_to_srgb(mut self) -> Self {
        for pixel in self.pixels.iter_mut() {
            *pixel = pixel.linear_to_srgb();
        }

        self
    }

    /// Convert the image from sRGB to linear space.
    pub fn srgb_to_linear(mut self) -> Self {
        for pixel in self.pixels.iter_mut() {
            *pixel = pixel.srgb_to_linear();
        }

        self
    }

    /// Write the image to the provided path.
    ///
    /// The file format is inferred from the path.
    ///
    /// Returns an [`Error`] if the document can not be written.
    /// If the error message is `Zero width not allowed`,
    /// it's likely you want to write the image of a render pass which has not been rendered.
    pub fn write<P: AsRef<Path>>(&self, path: P) -> Result<(), Error> {
        let mut image_buffer = ImageBuffer::new(self.width, self.height);
        image_buffer
            .enumerate_pixels_mut()
            .for_each(|(x, y, pixel)| {
                let color = self.pixels[(x + self.width * y) as usize].linear_to_srgb();
                *pixel = Rgb([
                    (color.r * 255.0).round().clamp(0.0, 255.0) as u8,
                    (color.g * 255.0).round().clamp(0.0, 255.0) as u8,
                    (color.b * 255.0).round().clamp(0.0, 255.0) as u8,
                ])
            });

        image_buffer.save(&path)?;

        println!("Saved image to {:?}", fs::canonicalize(path)?);

        Ok(())
    }

    #[cfg(feature = "oidn")]
    fn to_flattened_vec(&self) -> Vec<f32> {
        self.pixels
            .iter()
            .flat_map(|p| [p.r, p.g, p.b])
            .collect::<Vec<f32>>()
    }

    /// Denoise the image using [Open Image Denoise](https://www.openimagedenoise.org/).
    ///
    /// `albedo_image` and `normal_image` will be used if they are not [`None´].
    /// However, if `normal_image` is [`Some`] but `albedo_image` is [`None`],
    /// `normal_image` will be ignored by the denoiser.
    ///
    /// Returns an [`Error`] if the image can not be denoised.
    #[cfg(feature = "oidn")]
    pub fn denoise(
        &mut self,
        albedo_image: Option<&Image>,
        normal_image: Option<&Image>,
    ) -> Result<(), Error> {
        use oidn::{Device, RayTracing};
        use std::time::Instant;

        let width = self.width;
        let height = self.height;

        let start_time = Instant::now();

        let device = Device::new();
        let mut filter = RayTracing::new(&device);
        filter
            .srgb(false)
            .hdr(true)
            .image_dimensions(width as usize, height as usize);

        println!("Denoising...");

        let mut image_vec = self.to_flattened_vec();

        match albedo_image {
            None => {}
            Some(albedo_image) => {
                let albedo_vec = albedo_image.to_flattened_vec();
                filter.albedo(&albedo_vec);

                match normal_image {
                    None => {}
                    Some(normal_image) => {
                        let normal_vec = normal_image.to_flattened_vec();
                        filter.albedo_normal(&albedo_vec, &normal_vec);
                    }
                }
            }
        }

        filter.filter_in_place(&mut image_vec)?;

        if let Err((e, _)) = device.get_error() {
            return Err(e)?;
        }

        println!(
            "Denoising finished after {:.2?}",
            Instant::now() - start_time
        );

        for (i, channels) in image_vec.chunks(3).enumerate() {
            self.pixels[i] = Color::new(channels[0], channels[1], channels[2]);
        }

        Ok(())
    }

    /// Denoise the image without any additional information
    /// using [Open Image Denoise](https://www.openimagedenoise.org/).
    ///
    /// Returns an [`Error`] if the image can not be denoised.
    #[cfg(feature = "oidn")]
    pub fn denoise_simple(&mut self) -> Result<(), Error> {
        self.denoise(None, None)
    }

    /// Denoise the image with an additional albedo pass
    /// using [Open Image Denoise](https://www.openimagedenoise.org/).
    ///
    /// Returns an [`Error`] if the image can not be denoised.
    #[cfg(feature = "oidn")]
    pub fn denoise_albedo(&mut self, albedo_image: &Image) -> Result<(), Error> {
        self.denoise(Some(albedo_image), None)
    }

    /// Denoise the image with additional albedo and normal passes
    /// using [Open Image Denoise](https://www.openimagedenoise.org/).
    ///
    /// Returns an [`Error`] if the image can not be denoised.
    #[cfg(feature = "oidn")]
    pub fn denoise_albedo_normal(
        &mut self,
        albedo_image: &Image,
        normal_image: &Image,
    ) -> Result<(), Error> {
        self.denoise(Some(albedo_image), Some(normal_image))
    }
}

impl Texture for Image {
    fn get_pixel(&self, u: f64, v: f64) -> Color {
        self.get_pixel(
            (self.width - 1) as f32 * u as f32,
            (self.height - 1) as f32 * v as f32,
        )
    }
}

/// The interpolation used to access pixels of an [`Image`].
#[derive(Copy, Clone, Debug)]
pub enum Interpolation {
    /// Chooses the [`Color`] of the closest pixel.
    Closest,
    /// Interpolates linearly between the [`Color`]s of the 4 surrounding pixels.
    Bilinear,
}

/// The color format for a given [`Image`].
///
/// This is used for conversion when loading a byte buffer.
#[derive(Copy, Clone, Debug)]
pub enum ColorFormat {
    Rgb8,
    Rgba8,
    Rgb16,
    Rgba16,
}

impl ColorFormat {
    /// Return the color format's number of bytes per pixel.
    pub fn offset(&self) -> usize {
        match self {
            ColorFormat::Rgb8 => 3,
            ColorFormat::Rgba8 => 4,
            ColorFormat::Rgb16 => 6,
            ColorFormat::Rgba16 => 8,
        }
    }
}

#[cfg(feature = "gltf")]
impl TryFrom<Format> for ColorFormat {
    type Error = Error;

    fn try_from(format: Format) -> Result<Self, Self::Error> {
        match format {
            Format::R8G8B8 => Ok(ColorFormat::Rgb8),
            Format::R8G8B8A8 => Ok(ColorFormat::Rgba8),
            Format::R16G16B16 => Ok(ColorFormat::Rgb16),
            Format::R16G16B16A16 => Ok(ColorFormat::Rgba16),
            _ => Err(Error::from(format)),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::panic;

    use approx::assert_relative_eq;

    use crate::{Color, ColorFormat, Image, Interpolation};

    #[test]
    fn image_from_pixels() {
        let _ = Image::from_pixels(
            512,
            512,
            vec![Color::default(); 512 * 512],
            Interpolation::Bilinear,
        );
    }

    #[test]
    fn image_from_pixels_panic() {
        assert_eq!(
            panic::catch_unwind(|| {
                Image::from_pixels(
                    512,
                    512,
                    vec![Color::default(); 512 * 512 - 1],
                    Interpolation::Bilinear,
                )
            })
            .err()
            .and_then(|a| a
                .downcast_ref::<String>()
                .map(|s| { s.contains("image dimensions don't match the data size") })),
            Some(true)
        );
        assert_eq!(
            panic::catch_unwind(|| {
                Image::from_pixels(
                    512,
                    512,
                    vec![Color::default(); 512 * 512 + 1],
                    Interpolation::Bilinear,
                )
            })
            .err()
            .and_then(|a| a
                .downcast_ref::<String>()
                .map(|s| { s.contains("image dimensions don't match the data size") })),
            Some(true)
        );
    }

    #[test]
    fn image_from_raw_format() {
        for color_format in [
            ColorFormat::Rgb8,
            ColorFormat::Rgba8,
            ColorFormat::Rgb16,
            ColorFormat::Rgba16,
        ] {
            let _ = Image::from_raw_with_format(
                512,
                512,
                &vec![0; 512 * 512 * color_format.offset()],
                color_format,
                Interpolation::Bilinear,
            );
        }
    }

    #[test]
    fn image_from_raw_format_panic() {
        for color_format in [
            ColorFormat::Rgb8,
            ColorFormat::Rgba8,
            ColorFormat::Rgb16,
            ColorFormat::Rgba16,
        ] {
            assert_eq!(
                panic::catch_unwind(|| {
                    Image::from_raw_with_format(
                        512,
                        512,
                        &vec![0; 512 * 512 * color_format.offset() - 1],
                        color_format,
                        Interpolation::Bilinear,
                    );
                })
                .err()
                .and_then(|a| a
                    .downcast_ref::<String>()
                    .map(|s| { s.contains("image dimensions don't match the data size") })),
                Some(true)
            );
            assert_eq!(
                panic::catch_unwind(|| {
                    Image::from_raw_with_format(
                        512,
                        512,
                        &vec![0; 512 * 512 * color_format.offset() + 1],
                        color_format,
                        Interpolation::Bilinear,
                    );
                })
                .err()
                .and_then(|a| a
                    .downcast_ref::<String>()
                    .map(|s| { s.contains("image dimensions don't match the data size") })),
                Some(true)
            );
        }
    }

    #[test]
    fn image_from_rgb8() {
        let image = Image::from_raw_with_format(
            1,
            1,
            &[123, 42, 250],
            ColorFormat::Rgb8,
            Interpolation::Bilinear,
        );
        assert_relative_eq!(
            image.get_pixel(0.0, 0.0),
            Color::new(123.0 / 255.0, 42.0 / 255.0, 250.0 / 255.0)
        );
    }

    #[test]
    fn image_from_rgba8() {
        let image = Image::from_raw_with_format(
            1,
            1,
            &[123, 42, 250, 127],
            ColorFormat::Rgba8,
            Interpolation::Bilinear,
        );
        assert_relative_eq!(
            image.get_pixel(0.0, 0.0),
            Color::new(123.0 / 255.0, 42.0 / 255.0, 250.0 / 255.0)
        );
    }

    #[test]
    fn image_from_rgb16() {
        let image = Image::from_raw_with_format(
            1,
            1,
            &[164, 1, 57, 48, 65, 231],
            ColorFormat::Rgb16,
            Interpolation::Bilinear,
        );
        assert_relative_eq!(
            image.get_pixel(0.0, 0.0),
            Color::new(420.0 / 65535.0, 12345.0 / 65535.0, 59201.0 / 65535.0)
        );
    }

    #[test]
    fn image_from_rgba16() {
        let image = Image::from_raw_with_format(
            1,
            1,
            &[164, 1, 57, 48, 65, 231, 123, 17],
            ColorFormat::Rgba16,
            Interpolation::Bilinear,
        );
        assert_relative_eq!(
            image.get_pixel(0.0, 0.0),
            Color::new(420.0 / 65535.0, 12345.0 / 65535.0, 59201.0 / 65535.0)
        );
    }

    #[test]
    fn image_get_pixel_closest() {
        let image = Image::from_pixels(
            2,
            2,
            vec![
                Color::BLACK,
                Color::new(1.0, 0.0, 0.0),
                Color::new(0.0, 1.0, 0.0),
                Color::new(1.0, 1.0, 0.0),
            ],
            Interpolation::Closest,
        );

        assert_relative_eq!(image.get_pixel(0.0, 0.0), Color::BLACK);
        assert_relative_eq!(image.get_pixel(1.0, 0.0), Color::new(1.0, 0.0, 0.0));
        assert_relative_eq!(image.get_pixel(0.0, 1.0), Color::new(0.0, 1.0, 0.0));
        assert_relative_eq!(image.get_pixel(1.0, 1.0), Color::new(1.0, 1.0, 0.0));

        assert_relative_eq!(image.get_pixel(0.0, 0.0), Color::BLACK);
        assert_relative_eq!(image.get_pixel(-1.0, 0.0), Color::new(1.0, 0.0, 0.0));
        assert_relative_eq!(image.get_pixel(0.0, -1.0), Color::new(0.0, 1.0, 0.0));
        assert_relative_eq!(image.get_pixel(-1.0, -1.0), Color::new(1.0, 1.0, 0.0));

        assert_relative_eq!(image.get_pixel(0.4, 0.0), Color::BLACK);
        assert_relative_eq!(image.get_pixel(1.4, 0.0), Color::new(1.0, 0.0, 0.0));
        assert_relative_eq!(image.get_pixel(0.4, 1.0), Color::new(0.0, 1.0, 0.0));
        assert_relative_eq!(image.get_pixel(1.4, 1.0), Color::new(1.0, 1.0, 0.0));

        assert_relative_eq!(image.get_pixel(0.0, 0.6), Color::new(0.0, 1.0, 0.0));
        assert_relative_eq!(image.get_pixel(1.0, 0.6), Color::new(1.0, 1.0, 0.0));
        assert_relative_eq!(image.get_pixel(0.0, 1.6), Color::BLACK);
        assert_relative_eq!(image.get_pixel(1.0, 1.6), Color::new(1.0, 0.0, 0.0));
    }

    #[test]
    fn image_get_pixel_bilinear() {
        let image = Image::from_pixels(
            2,
            2,
            vec![
                Color::BLACK,
                Color::new(1.0, 0.0, 0.0),
                Color::new(0.0, 1.0, 0.0),
                Color::new(1.0, 1.0, 0.0),
            ],
            Interpolation::Bilinear,
        );

        assert_relative_eq!(image.get_pixel(0.0, 0.0), Color::BLACK);
        assert_relative_eq!(image.get_pixel(1.0, 0.0), Color::new(1.0, 0.0, 0.0));
        assert_relative_eq!(image.get_pixel(0.0, 1.0), Color::new(0.0, 1.0, 0.0));
        assert_relative_eq!(image.get_pixel(1.0, 1.0), Color::new(1.0, 1.0, 0.0));

        assert_relative_eq!(image.get_pixel(0.0, 0.0), Color::BLACK);
        assert_relative_eq!(image.get_pixel(-1.0, 0.0), Color::new(1.0, 0.0, 0.0));
        assert_relative_eq!(image.get_pixel(0.0, -1.0), Color::new(0.0, 1.0, 0.0));
        assert_relative_eq!(image.get_pixel(-1.0, -1.0), Color::new(1.0, 1.0, 0.0));

        assert_relative_eq!(image.get_pixel(0.5, 0.0), Color::new(0.5, 0.0, 0.0));
        assert_relative_eq!(image.get_pixel(1.5, 0.0), Color::new(0.5, 0.0, 0.0));
        assert_relative_eq!(image.get_pixel(0.5, 1.0), Color::new(0.5, 1.0, 0.0));
        assert_relative_eq!(image.get_pixel(1.5, 1.0), Color::new(0.5, 1.0, 0.0));

        assert_relative_eq!(image.get_pixel(0.0, 0.5), Color::new(0.0, 0.5, 0.0));
        assert_relative_eq!(image.get_pixel(1.0, 0.5), Color::new(1.0, 0.5, 0.0));
        assert_relative_eq!(image.get_pixel(0.0, 1.5), Color::new(0.0, 0.5, 0.0));
        assert_relative_eq!(image.get_pixel(1.0, 1.5), Color::new(1.0, 0.5, 0.0));

        assert_relative_eq!(image.get_pixel(0.5, 0.5), Color::new(0.5, 0.5, 0.0));
    }

    #[test]
    fn image_set_pixel() {
        let mut image = Image::new(1, 1, Interpolation::Bilinear);
        assert_relative_eq!(image.get_pixel(0.0, 0.0), Color::default());

        image.set_pixel(0, 0, Color::new(0.0, 0.5, 1.0));
        assert_relative_eq!(image.get_pixel(0.0, 0.0), Color::new(0.0, 0.5, 1.0));
    }

    #[test]
    fn image_set_pixel_panic() {
        assert!(panic::catch_unwind(|| {
            let mut image = Image::new(1, 1, Interpolation::Bilinear);

            image.set_pixel(0, 1, Color::WHITE);
        })
        .is_err());
        assert!(panic::catch_unwind(|| {
            let mut image = Image::new(1, 1, Interpolation::Bilinear);

            image.set_pixel(1, 0, Color::WHITE);
        })
        .is_err());
    }

    #[test]
    fn image_linear_to_srgb() {
        let black = Image::from_pixels(1, 1, vec![Color::BLACK], Interpolation::Bilinear);
        let white = Image::from_pixels(1, 1, vec![Color::WHITE], Interpolation::Bilinear);
        let grey = Image::from_pixels(1, 1, vec![Color::splat(0.5)], Interpolation::Bilinear);

        assert_relative_eq!(
            black.get_pixel(0.0, 0.0).linear_to_srgb(),
            black.get_pixel(0.0, 0.0)
        );
        assert_relative_eq!(
            white.get_pixel(0.0, 0.0).linear_to_srgb(),
            white.get_pixel(0.0, 0.0)
        );
        assert_relative_eq!(
            grey.linear_to_srgb().get_pixel(0.0, 0.0),
            Color::splat(0.7353569831)
        );
    }

    #[test]
    fn image_srgb_to_linear() {
        let black = Image::from_pixels(1, 1, vec![Color::BLACK], Interpolation::Bilinear);
        let white = Image::from_pixels(1, 1, vec![Color::WHITE], Interpolation::Bilinear);
        let grey = Image::from_pixels(1, 1, vec![Color::splat(0.5)], Interpolation::Bilinear);

        assert_relative_eq!(
            black.get_pixel(0.0, 0.0).srgb_to_linear(),
            black.get_pixel(0.0, 0.0)
        );
        assert_relative_eq!(
            white.get_pixel(0.0, 0.0).srgb_to_linear(),
            white.get_pixel(0.0, 0.0)
        );
        assert_relative_eq!(
            grey.srgb_to_linear().get_pixel(0.0, 0.0),
            Color::splat(0.2140411405)
        );
    }

    #[test]
    fn image_color_conversion_invert() {
        let grey = Image::from_pixels(1, 1, vec![Color::splat(0.5)], Interpolation::Bilinear);

        assert_relative_eq!(
            grey.get_pixel(0.0, 0.0).linear_to_srgb().srgb_to_linear(),
            grey.get_pixel(0.0, 0.0)
        );
        assert_relative_eq!(
            grey.get_pixel(0.0, 0.0).srgb_to_linear().linear_to_srgb(),
            grey.get_pixel(0.0, 0.0)
        );
    }
}
