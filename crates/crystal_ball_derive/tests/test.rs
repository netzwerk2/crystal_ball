#[test]
fn tests() {
    let t = trybuild::TestCases::new();
    t.pass("tests/ui/render_pass_data/derive.rs");
    t.pass("tests/ui/render_pass_data/equally_named_fields.rs");
    t.compile_fail("tests/ui/render_pass_data/no_image.rs");
    t.compile_fail("tests/ui/render_pass_data/no_samples.rs");
    t.compile_fail("tests/ui/render_pass_data/wrong_type.rs");
    t.pass("tests/ui/transformable/derive.rs");
    t.pass("tests/ui/transformable/multiple_transforms.rs");
    t.pass("tests/ui/transformable/equally_named_fields.rs");
    t.compile_fail("tests/ui/transformable/no_transform.rs");
    t.compile_fail("tests/ui/transformable/wrong_type.rs");
}
