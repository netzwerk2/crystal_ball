#![allow(dead_code)]

use crystal_ball_derive::Transformable;
use crystal_ball_math::Transform;

#[derive(Transformable)]
struct Shape {
    #[transformable]
    transform: Transform,
    #[transformable]
    transform2: Transform,
}

fn main() {}
