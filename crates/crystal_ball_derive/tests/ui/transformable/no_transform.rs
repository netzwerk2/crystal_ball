use crystal_ball_derive::Transformable;
use crystal_ball_math::Transform;

#[derive(Transformable)]
struct Shape {
    transform: Transform,
}

fn main() {}
