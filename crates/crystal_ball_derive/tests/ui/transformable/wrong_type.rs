use crystal_ball_derive::Transformable;

#[derive(Transformable)]
struct Shape {
    #[transformable]
    transform: String,
}

fn main() {}
