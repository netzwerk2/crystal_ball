use crystal_ball_derive::Transformable;
use crystal_ball_math::Transform;

#[derive(Transformable)]
pub struct Foo {
    #[transformable]
    pub transform: Transform,
    #[transformable]
    pub transform2: Transform,
}

#[derive(Transformable)]
pub struct Bar {
    #[transformable]
    pub transform: Transform,
}

#[derive(Transformable)]
pub struct Baz {
    #[transformable]
    pub transform2: Transform,
}

fn main() {}
