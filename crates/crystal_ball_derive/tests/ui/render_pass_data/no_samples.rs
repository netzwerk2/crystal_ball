#![allow(dead_code)]

use crystal_ball_color::Image;
use crystal_ball_derive::RenderPassData;

#[derive(RenderPassData)]
struct CombinedPass {
    samples: usize,
    #[image]
    image: Image,
}

fn main() {}
