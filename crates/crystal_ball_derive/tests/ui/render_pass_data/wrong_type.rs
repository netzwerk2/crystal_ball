#![allow(dead_code)]

use crystal_ball_color::Image;
use crystal_ball_derive::RenderPassData;

#[derive(RenderPassData)]
struct CombinedPass {
    #[samples]
    samples: String,
    #[image]
    image: String,
}

fn main() {}
