use proc_macro2::{Ident, Span, TokenStream};
use proc_macro_crate::{crate_name, FoundCrate};
use quote::{quote, quote_spanned};
use syn::{parse_macro_input, Data, DeriveInput, Field, Fields};

pub fn derive_transformable(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let input_struct_name = input.ident;

    let crate_name = match crate_name("crystal_ball_math")
        .expect("Error finding name of crate `crystal_ball_math`.")
    {
        FoundCrate::Itself => syn::parse_str::<syn::Path>("crate").unwrap(),
        FoundCrate::Name(name) => syn::parse_str::<syn::Path>(&name).unwrap(),
    };

    let Data::Struct(data) = input.data else {
        return quote!(compile_error!("expected struct");).into();
    };

    let Fields::Named(fields) = data.fields else {
        return quote!(compile_error!("expected named fields");).into();
    };

    let transformable_fields = fields
        .named
        .iter()
        .filter(|field| {
            field
                .attrs
                .iter()
                .any(|attr| attr.path().is_ident("transformable"))
        })
        .collect::<Vec<&Field>>();

    if transformable_fields.is_empty() {
        return quote!(compile_error!(
            "expected at least 1 `#[transformable]` attribute"
        );)
        .into();
    }

    let trait_checks = transformable_fields.clone().into_iter().map(|field| {
        let name = field.clone().ident.unwrap();
        let ty = field.clone().ty;
        let span = name.span();
        let struct_name = Ident::new(
            &format!("_Assert_{}_Transformable_{}", input_struct_name, name),
            span,
        );
        let mut crate_name = crate_name.get_ident().unwrap().clone();
        crate_name.set_span(span);

        quote_spanned!(span=> struct #struct_name where #ty: #crate_name::Transformable;)
    });

    let functions = [
        (
            "translate",
            vec![("translation", quote!(#crate_name::Vec3))],
        ),
        (
            "rotate",
            vec![
                ("origin", quote!(#crate_name::Point3)),
                ("axis", quote!(#crate_name::Vec3)),
                ("angle", quote!(f64)),
            ],
        ),
        ("rotate_x", vec![("angle", quote!(f64))]),
        ("rotate_y", vec![("angle", quote!(f64))]),
        ("rotate_z", vec![("angle", quote!(f64))]),
        ("scale_x", vec![("factor", quote!(f64))]),
        ("scale_y", vec![("factor", quote!(f64))]),
        ("scale_z", vec![("factor", quote!(f64))]),
        ("scale_xyz", vec![("scale", quote!(#crate_name::Vec3))]),
        (
            "scale",
            vec![
                ("origin", quote!(#crate_name::Point3)),
                ("scale", quote!(#crate_name::Vec3)),
            ],
        ),
        (
            "look_at",
            vec![
                ("target", quote!(#crate_name::Point3)),
                ("view_up", quote!(#crate_name::Vec3)),
            ],
        ),
    ];

    let transformable_fields = transformable_fields
        .into_iter()
        .map(|field| field.clone().ident.unwrap())
        .collect::<Vec<Ident>>();

    let functions = functions.into_iter().map(|function| {
        let (name, arguments) = function;

        let def_args = arguments_for_fn_def(arguments.clone());

        let call_args = arguments_for_fn_call(arguments);
        let body = function_body(transformable_fields.clone(), name, call_args);

        let name_ident = Ident::new(name, Span::call_site());

        // One could probably get away with multiple interpolations in interpolations instead of
        // calling 3 functions, but I'm 99% certain no one would want to touch this code again in that case.
        quote!(
            fn #name_ident(mut self, #def_args) -> Self {
                #body
                self
            }
        )
    });

    quote!(
        #(
            #[allow(nonstandard_style)]
            #trait_checks
        )*

        impl #crate_name::Transformable for #input_struct_name {
            #(#functions)*
        }
    )
    .into()
}

fn function_body(
    field_names: Vec<Ident>,
    function_name: &str,
    arguments: TokenStream,
) -> TokenStream {
    let function_name = Ident::new(function_name, Span::call_site());
    quote!(
        #(self.#field_names = self.#field_names.#function_name(#arguments);)*
    )
}

fn arguments_for_fn_def(arguments: Vec<(&str, TokenStream)>) -> TokenStream {
    let (names, type_path) = arguments
        .iter()
        .map(|(name, type_path)| (Ident::new(name, Span::call_site()), type_path))
        .unzip::<Ident, &TokenStream, Vec<Ident>, Vec<&TokenStream>>();

    quote!(#(#names: #type_path),*)
}

fn arguments_for_fn_call(arguments: Vec<(&str, TokenStream)>) -> TokenStream {
    let name = arguments
        .iter()
        .map(|(name, _)| Ident::new(name, Span::call_site()));

    quote!(#(#name),*)
}
