#![cfg_attr(doc, feature(doc_auto_cfg))]

//! A crate providing derive macros Crystal Ball.

extern crate proc_macro;

mod derive_render_pass_data;
mod derive_transformable;

/// Easily implement [`Transformable`].
///
/// The implementation simply calls the corresponding methods on all marked fields.
/// Thus, every field marked with the `#[transformable]` attribute needs to implement `Transformable`
/// A compile time error is issued when there is no field with `#[transformable]`.
///
/// # Attributes
/// - `#[transformable]`: declares which fields should be transformed.
///
/// # Examples
///
/// ```
/// use crystal_ball_derive::Transformable;
/// use crystal_ball_math::Transform;
///
/// #[derive(Transformable)]
/// pub struct Sphere {
///     #[transformable]
///     pub transform: Transform,
/// }
/// ```
#[proc_macro_derive(Transformable, attributes(transformable))]
pub fn derive_transformable(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    derive_transformable::derive_transformable(input)
}

/// Easily implement [`RenderPassData`].
///
/// The implementation simply returns the marked `#[samples]` and `#[image]` fields.
/// Therefore, if one of these attributes is missing, a compile error is used.
///
/// # Attributes
/// - `#[samples]`: field, which should be returned by `RenderPass::samples`
/// - `#[image]`: field, which should be returned by `RenderPass::image` and `RenderPass::image_mut`
///
/// # Examples
///
/// ```
/// use crystal_ball_color::Image;
/// use crystal_ball_derive::RenderPassData;
///
/// #[derive(RenderPassData)]
/// struct CombinedPass {
///     #[samples]
///     samples: usize,
///     #[image]
///     image: Image,
/// }
/// ```
#[proc_macro_derive(RenderPassData, attributes(samples, image))]
pub fn derive_render_pass(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    derive_render_pass_data::derive_render_pass_data(input)
}
