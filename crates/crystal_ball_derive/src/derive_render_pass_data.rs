use proc_macro_crate::{crate_name, FoundCrate};
use quote::{quote, quote_spanned};
use syn::{parse_macro_input, Data, Field, Fields};

pub fn derive_render_pass_data(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as syn::DeriveInput);
    let input_struct_name = input.ident;

    let crate_name = match crate_name("crystal_ball_rendering")
        .expect("Error finding name of crate `crystal_ball_rendering`")
    {
        FoundCrate::Itself => syn::parse_str::<syn::Path>("crate").unwrap(),
        FoundCrate::Name(name) => syn::parse_str::<syn::Path>(&name).unwrap(),
    };

    let Data::Struct(data) = input.data else {
        return quote!(compile_error!("expected struct");).into();
    };

    let Fields::Named(fields) = data.fields else {
        return quote!(compile_error!("expected named fields");).into();
    };

    let samples_fields = fields
        .named
        .iter()
        .filter(|field| {
            field
                .attrs
                .iter()
                .any(|attr| attr.path().is_ident("samples"))
        })
        .collect::<Vec<&Field>>();
    let image_fields = fields
        .named
        .iter()
        .filter(|field| field.attrs.iter().any(|attr| attr.path().is_ident("image")))
        .collect::<Vec<&Field>>();

    if samples_fields.len() != 1 {
        return quote!(compile_error!(
            "expected exactly one `#[samples]` attribute"
        );)
        .into();
    }
    if image_fields.len() != 1 {
        return quote!(compile_error!("expected exactly one `#[image]` attribute");).into();
    }

    let samples_span = samples_fields[0].ident.as_ref().unwrap().span();
    let image_span = image_fields[0].ident.as_ref().unwrap().span();

    let samples = samples_fields[0].ident.clone().unwrap();
    let image = image_fields[0].ident.clone().unwrap();

    let samples_tokens = quote_spanned!(samples_span=>
        fn samples(&self) -> usize {
            self.#samples
        }
    );
    let image_tokens = quote_spanned!(image_span=>
        fn image(&self) -> &Image {
            &self.#image
        }

        fn image_mut(&mut self) -> &mut Image {
            &mut self.#image
        }
    );

    quote!(
        impl #crate_name::RenderPassData for #input_struct_name {
            #samples_tokens

            #image_tokens
        }
    )
    .into()
}
