#![cfg_attr(doc, feature(doc_auto_cfg))]

//! Geometric shapes that can be placed into the scene for Crystal Ball.

pub use bvh::*;
use crystal_ball_math::{Bounds3, Hit, Ray};
pub use mesh::*;
pub use object::*;
pub use sphere::*;
use std::sync::Arc;
pub use triangle::*;
pub use triangle_mesh::*;

mod bvh;
mod mesh;
mod object;
mod sphere;
mod triangle;
mod triangle_mesh;

/// A general representation of shapes.
///
/// This allows performing ray intersections against them and accessing their bounding volumes.
pub trait Shape: Send + Sync {
    /// Return the corresponding hit, if self and ray intersect.
    /// Otherwise, return [`None`].
    fn intersects(&self, ray: Ray) -> Option<Hit>;

    /// Return the shape's bounding volume.
    fn bounds(&self) -> Bounds3;
}

/// A trait allowing an `Arc<dyn Shape>` to be constructed either from a struct
/// implementing [`Shape`] or from an [`Arc`] itself.
pub trait IntoArcDynShape {
    /// Perform the conversion of `self` into `Arc<dyn Shape>`.
    fn into_arc_dyn_shape(self) -> Arc<dyn Shape>;
}

impl<T: Shape + 'static> IntoArcDynShape for T {
    fn into_arc_dyn_shape(self) -> Arc<dyn Shape> {
        Arc::new(self)
    }
}

impl<T: Shape + 'static> IntoArcDynShape for Arc<T> {
    fn into_arc_dyn_shape(self) -> Arc<dyn Shape> {
        self
    }
}

impl IntoArcDynShape for Arc<dyn Shape> {
    fn into_arc_dyn_shape(self) -> Arc<dyn Shape> {
        self
    }
}
