use std::f64::consts::FRAC_1_PI;

use crystal_ball_derive::Transformable;
use crystal_ball_math::{Bounds3, Hit, Point2, Point3, Ray, Transform, Vec3, Vec4};

use crate::Shape;

/// A geometrically perfect sphere.
#[derive(Copy, Clone, Default, Debug, PartialEq, Transformable)]
pub struct Sphere {
    #[transformable]
    pub transform: Transform,
}

impl Sphere {
    /// Create a new [`Sphere`].
    pub fn new() -> Self {
        Sphere {
            transform: Transform::default(),
        }
    }

    /// Map a point on the unit sphere to UV coordinates.
    pub fn uv_map(&self, point: Point3) -> Point2 {
        let u = 0.5 + (-point.z).atan2(point.x) * 0.5 * FRAC_1_PI;
        let v = 0.5 - point.y.asin() * FRAC_1_PI;

        Point2::new(u, v)
    }
}

impl Shape for Sphere {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        let ray = self.transform.inverse_transform_ray(ray);

        // If the ray origin is outside of the sphere,
        // tangent_squared is the square of the vector from the ray origin to a tangent point on the sphere.
        // If it is inside, tangent_squared will be < 0, but the formulas below still hold.
        let tangent_squared = ray.origin.to_vec3().magnitude_squared() - 1.0;
        let projection = Vec3::dot(ray.direction, ray.origin.into());
        let discriminant = projection.powi(2) - tangent_squared;

        if discriminant <= 0.0 {
            return None;
        }

        let mut intersection_distance = if tangent_squared > 0.0 {
            // Ray origin is outside the sphere.
            -projection - discriminant.sqrt()
        } else {
            // Ray origin is inside the sphere.
            -projection + discriminant.sqrt()
        };

        if intersection_distance < 0.0 {
            return None;
        }

        let mut intersection_point = ray.get(intersection_distance);

        let mut normal = intersection_point.to_vec3();
        let mut tangent = Vec3::cross(Vec3::Y, intersection_point.to_vec3())
            .normalize()
            .extend(1.0);
        tangent = self.transform.transform_tangent(tangent);
        let uv = self.uv_map(intersection_point);

        let ray = self.transform.transform_ray(ray);

        intersection_point = self.transform.mat4 * intersection_point;
        intersection_distance = (intersection_point - ray.origin).magnitude();

        normal = self.transform.transform_normal(normal);

        if Vec3::dot(ray.direction, normal) > 0.0 {
            normal = -normal;
            tangent = Vec4::new(-tangent.x, -tangent.y, -tangent.z, tangent.w);
        }

        Some(Hit::new(
            intersection_point,
            normal,
            Some(tangent),
            intersection_distance,
            uv,
        ))
    }

    fn bounds(&self) -> Bounds3 {
        let min = Point3::splat(-1.0);
        let max = Point3::splat(1.0);

        self.transform.transform_bounds(Bounds3::new(min, max))
    }
}

#[cfg(test)]
mod tests {
    use std::f64::consts::{FRAC_PI_2, FRAC_PI_3, FRAC_PI_4, FRAC_PI_8};

    use approx::assert_relative_eq;

    use crystal_ball_math::{Point3, Ray, Transformable, Vec3, Vec4};

    use crate::{Shape, Sphere};

    #[test]
    fn sphere_translate() {
        let translation = Vec3::new(2.0, -1.0, 0.0);
        let sphere = Sphere::default().translate(translation);

        assert_relative_eq!(
            sphere.transform.mat4 * Point3::ZERO,
            translation.to_point3()
        );

        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(1.0, 0.0, 0.0),
            Point3::new(1.0, 0.0, 0.0) + translation
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(-1.0, 0.0, 0.0),
            Point3::new(-1.0, 0.0, 0.0) + translation
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, 1.0, 0.0),
            Point3::new(0.0, 1.0, 0.0) + translation
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, -1.0, 0.0),
            Point3::new(0.0, -1.0, 0.0) + translation
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, 0.0, 1.0),
            Point3::new(0.0, 0.0, 1.0) + translation
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, 0.0, -1.0),
            Point3::new(0.0, 0.0, -1.0) + translation
        );
    }

    #[test]
    fn sphere_rotate() {
        let sphere = Sphere::default()
            .rotate_x(FRAC_PI_2)
            .rotate_y(FRAC_PI_2)
            .rotate_z(FRAC_PI_2);

        assert_relative_eq!(sphere.transform.mat4 * Point3::ZERO, Point3::ZERO);

        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(1.0, 0.0, 0.0),
            Point3::new(0.0, 0.0, -1.0)
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(-1.0, 0.0, 0.0),
            Point3::new(0.0, 0.0, 1.0)
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, 1.0, 0.0),
            Point3::new(0.0, 1.0, 0.0),
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, -1.0, 0.0),
            Point3::new(0.0, -1.0, 0.0)
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, 0.0, 1.0),
            Point3::new(1.0, 0.0, 0.0)
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, 0.0, -1.0),
            Point3::new(-1.0, 0.0, 0.0)
        );
    }

    #[test]
    fn sphere_scale() {
        let scale = Vec3::new(2.0, -1.0, 0.5);
        let sphere = Sphere::default().scale_xyz(scale);

        assert_relative_eq!(sphere.transform.mat4 * Point3::ZERO, Point3::ZERO);

        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(1.0, 0.0, 0.0),
            Point3::new(1.0 * scale.x, 0.0, 0.0)
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(-1.0, 0.0, 0.0),
            Point3::new(-1.0 * scale.x, 0.0, 0.0)
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, 1.0, 0.0),
            Point3::new(0.0, 1.0 * scale.y, 0.0)
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, -1.0, 0.0),
            Point3::new(0.0, -1.0 * scale.y, 0.0)
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, 0.0, 1.0),
            Point3::new(0.0, 0.0, 1.0 * scale.z)
        );
        assert_relative_eq!(
            sphere.transform.mat4 * Point3::new(0.0, 0.0, -1.0),
            Point3::new(0.0, 0.0, -1.0 * scale.z)
        );
    }

    #[test]
    fn sphere_ray_intersection() {
        let origin = Point3::new(2.0, -1.0, 0.0);

        let mut sphere = Sphere::new().translate(origin.to_vec3()).scale_z(2.0);

        let hit_1 = sphere
            .intersects(Ray::new(origin + Vec3::new(0.5, 0.0, -4.0), Vec3::Z, 1.0))
            .unwrap();

        assert_relative_eq!(hit_1.distance, 2.26911, max_relative = 1e-3);
        assert_relative_eq!(
            hit_1.position,
            Point3::new(2.5, -1.0, -1.73089),
            max_relative = 1e-3
        );
        assert_relative_eq!(
            hit_1.normal,
            Vec3::new(0.7546014663, 0.0, -0.6561833792),
            max_relative = 1e-2
        );
        assert_relative_eq!(
            hit_1.tangent.unwrap(),
            Vec4::new(-0.6546536707079771, 0.0, -0.7559289460184545, 1.0)
        );

        let hit_2 = sphere.intersects(Ray::new(origin, Vec3::Z, 1.0)).unwrap();
        assert_relative_eq!(hit_2.position, origin + Vec3::new(0.0, 0.0, 2.0));
        assert_relative_eq!(hit_2.distance, 2.0, max_relative = 1e-8);
        assert_relative_eq!(hit_2.normal, -Vec3::Z);
        assert_relative_eq!(hit_2.tangent.unwrap(), Vec4::new(-1.0, 0.0, 0.0, 1.0));

        sphere = sphere
            .scale_z(0.5)
            .rotate(origin, Vec3::X, FRAC_PI_8)
            .rotate(origin, Vec3::Y, FRAC_PI_4)
            .rotate(origin, Vec3::Z, FRAC_PI_3);

        let hit_3 = sphere
            .intersects(Ray::new(Point3::new(2.0, -1.0, 0.0), Vec3::Z, 1.0))
            .unwrap();
        assert_relative_eq!(
            hit_3.position,
            Point3::new(2.0, -1.0, 1.0),
            max_relative = 1e-7
        );
        assert_relative_eq!(hit_3.distance, 1.0, max_relative = 1e-8);
        assert_relative_eq!(hit_3.normal, -Vec3::Z, epsilon = 1e-15);
        assert_relative_eq!(Vec3::dot(hit_3.normal, hit_3.tangent.unwrap().xyz()), 0.0);

        let hit_4 = sphere.intersects(Ray::new(origin + Vec3::new(0.0, 2.0, -4.0), Vec3::Z, 1.0));
        assert!(hit_4.is_none());
    }
}
