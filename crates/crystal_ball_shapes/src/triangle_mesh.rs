#[cfg(doc)]
use crate::Mesh;
use crystal_ball_math::{Point2, Point3, Transform, Transformable, Vec3, Vec4};
use std::sync::Arc;

use crate::Triangle;

/// A mesh consisting of multiple triangles.
///
/// When applying transformations such as [`Transformable::rotate_x`],
/// [`TriangleMesh::vertex_mean`] is used as the origin.
///
/// This struct does not store any [`Triangle`]s directly.
/// Instead, it contains the necessary buffers to create the individual triangles.
#[derive(Clone)]
pub struct TriangleMesh {
    pub indices: Vec<u32>,
    pub vertices: Vec<Point3>,
    pub normals: Vec<Vec3>,
    pub tangents: Vec<Vec4>,
    pub uvs: Vec<Point2>,
}

impl TriangleMesh {
    /// Create a new [`TriangleMesh`].
    pub fn new(
        indices: Vec<u32>,
        vertices: Vec<Point3>,
        normals: Vec<Vec3>,
        tangents: Vec<Vec4>,
        uvs: Vec<Point2>,
    ) -> Self {
        Self {
            indices,
            vertices,
            normals,
            tangents,
            uvs,
        }
    }

    /// Create a new [`TriangleMesh`] consisting of a single isosceles right triangle
    /// with normals pointing in +Y direction.
    ///
    /// The right-angled corner is at (1, 1) while (0, 0) is the hypotenuse's center.
    pub fn triangle(half_leg_length: f64) -> TriangleMesh {
        let indices = vec![0, 1, 2];
        let vertices = vec![
            Point3::new(half_leg_length, 0.0, -half_leg_length),
            Point3::new(-half_leg_length, 0.0, half_leg_length),
            Point3::new(half_leg_length, 0.0, half_leg_length),
        ];
        let normals = vec![Vec3::Y; 3];
        let tangents = vec![Vec4::new(1.0, 0.0, 0.0, -1.0); 3];
        let uvs = vec![];

        TriangleMesh::new(indices, vertices, normals, tangents, uvs)
    }

    /// Create a new [`TriangleMesh`] consisting of a single plane (square)
    /// with normals pointing in +Y direction.
    pub fn plane(half_size: f64) -> TriangleMesh {
        let indices = vec![0, 1, 2, 0, 3, 1];
        let vertices = vec![
            Point3::new(half_size, 0.0, -half_size),
            Point3::new(-half_size, 0.0, half_size),
            Point3::new(half_size, 0.0, half_size),
            Point3::new(-half_size, 0.0, -half_size),
        ];
        let normals = vec![Vec3::Y; 4];
        let tangents = vec![Vec4::new(1.0, 0.0, 0.0, 1.0); 4];
        let uvs = vec![];

        TriangleMesh::new(indices, vertices, normals, tangents, uvs)
    }

    /// Create a new [`TriangleMesh`] consisting of a single cube.
    pub fn cube(half_size: f64) -> TriangleMesh {
        let indices = vec![
            0, 1, 2, 0, 3, 1, 4, 5, 6, 4, 7, 5, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16,
            17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23,
        ];
        let vertices = vec![
            Point3::new(half_size, half_size, -half_size),
            Point3::new(-half_size, half_size, half_size),
            Point3::new(half_size, half_size, half_size),
            Point3::new(-half_size, half_size, -half_size),
            Point3::new(half_size, -half_size, -half_size),
            Point3::new(-half_size, -half_size, half_size),
            Point3::new(half_size, -half_size, half_size),
            Point3::new(-half_size, -half_size, -half_size),
            Point3::new(half_size, -half_size, half_size),
            Point3::new(half_size, half_size, half_size),
            Point3::new(half_size, half_size, -half_size),
            Point3::new(half_size, -half_size, -half_size),
            Point3::new(-half_size, -half_size, half_size),
            Point3::new(-half_size, half_size, half_size),
            Point3::new(-half_size, half_size, -half_size),
            Point3::new(-half_size, -half_size, -half_size),
            Point3::new(half_size, -half_size, half_size),
            Point3::new(-half_size, -half_size, half_size),
            Point3::new(-half_size, half_size, half_size),
            Point3::new(half_size, half_size, half_size),
            Point3::new(half_size, -half_size, -half_size),
            Point3::new(-half_size, -half_size, -half_size),
            Point3::new(-half_size, half_size, -half_size),
            Point3::new(half_size, half_size, -half_size),
        ];
        let normals = vec![
            Vec3::Y,
            Vec3::Y,
            Vec3::Y,
            Vec3::Y,
            -Vec3::Y,
            -Vec3::Y,
            -Vec3::Y,
            -Vec3::Y,
            Vec3::X,
            Vec3::X,
            Vec3::X,
            Vec3::X,
            -Vec3::X,
            -Vec3::X,
            -Vec3::X,
            -Vec3::X,
            Vec3::Z,
            Vec3::Z,
            Vec3::Z,
            Vec3::Z,
            -Vec3::Z,
            -Vec3::Z,
            -Vec3::Z,
            -Vec3::Z,
        ];
        let tangents = vec![
            Vec4::new(-1.0, 0.0, 0.0, 1.0),
            Vec4::new(-1.0, 0.0, 0.0, 1.0),
            Vec4::new(-1.0, 0.0, 0.0, 1.0),
            Vec4::new(-1.0, 0.0, 0.0, 1.0),
            Vec4::new(1.0, 0.0, 0.0, 1.0),
            Vec4::new(1.0, 0.0, 0.0, 1.0),
            Vec4::new(1.0, 0.0, 0.0, 1.0),
            Vec4::new(1.0, 0.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
            Vec4::new(0.0, 1.0, 0.0, 1.0),
        ];
        let uvs = vec![];

        TriangleMesh::new(indices, vertices, normals, tangents, uvs)
    }

    /// Calculate the triangle mesh's vertex mean.
    pub fn vertex_mean(&self) -> Point3 {
        self.vertices.iter().fold(Point3::ZERO, |acc, p| acc + *p) / self.vertices.len() as f64
    }

    /// Return an iterator over the triangle mesh's individual triangles.
    ///
    /// These triangles can then be used to construct a [`Mesh`].
    pub fn triangles(self) -> impl Iterator<Item = Triangle> {
        let triangle_count = self.indices.len() / 3;
        let triangle_mesh = Arc::new(self);

        (0..triangle_count).map(move |i| Triangle::new(Arc::clone(&triangle_mesh), i))
    }
}

impl Transformable for TriangleMesh {
    fn translate(mut self, translation: Vec3) -> Self {
        for vertex in self.vertices.iter_mut() {
            *vertex += translation;
        }

        self
    }

    fn rotate(mut self, origin: Point3, axis: Vec3, angle: f64) -> Self {
        let transform = Transform::rotate(origin, axis, angle);

        for vertex in self.vertices.iter_mut() {
            *vertex = transform.mat4 * *vertex;
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.mat4 * *normal;
        }

        for tangent in self.tangents.iter_mut() {
            *tangent = transform.transform_tangent(*tangent);
        }

        self
    }

    fn rotate_x(mut self, angle: f64) -> Self {
        let vertex_mean = self.vertex_mean();
        let transform = Transform::rotate_x(vertex_mean, angle);

        for vertex in self.vertices.iter_mut() {
            *vertex = transform.mat4 * *vertex;
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.mat4 * *normal;
        }

        for tangent in self.tangents.iter_mut() {
            *tangent = transform.transform_tangent(*tangent);
        }

        self
    }

    fn rotate_y(mut self, angle: f64) -> Self {
        let vertex_mean = self.vertex_mean();
        let transform = Transform::rotate_y(vertex_mean, angle);

        for vertex in self.vertices.iter_mut() {
            *vertex = transform.mat4 * *vertex;
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.mat4 * *normal;
        }

        for tangent in self.tangents.iter_mut() {
            *tangent = transform.transform_tangent(*tangent);
        }

        self
    }

    fn rotate_z(mut self, angle: f64) -> Self {
        let vertex_mean = self.vertex_mean();
        let transform = Transform::rotate_z(vertex_mean, angle);

        for vertex in self.vertices.iter_mut() {
            *vertex = transform.mat4 * *vertex;
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.mat4 * *normal;
        }

        for tangent in self.tangents.iter_mut() {
            *tangent = transform.transform_tangent(*tangent);
        }

        self
    }

    fn scale_x(self, factor: f64) -> Self {
        let vertex_mean = self.vertex_mean();

        self.scale(vertex_mean, Vec3::new(factor, 1.0, 1.0))
    }

    fn scale_y(self, factor: f64) -> Self {
        let vertex_mean = self.vertex_mean();

        self.scale(vertex_mean, Vec3::new(1.0, factor, 1.0))
    }

    fn scale_z(self, factor: f64) -> Self {
        let vertex_mean = self.vertex_mean();

        self.scale(vertex_mean, Vec3::new(1.0, 1.0, factor))
    }

    fn scale_xyz(self, scale: Vec3) -> Self {
        let vertex_mean = self.vertex_mean();

        self.scale(vertex_mean, scale)
    }

    fn scale(mut self, origin: Point3, scale: Vec3) -> Self {
        let transform = Transform::scale(origin, scale);

        for vertex in self.vertices.iter_mut() {
            *vertex = transform.mat4 * *vertex;
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.transform_normal(*normal);
        }

        for tangent in self.tangents.iter_mut() {
            *tangent = transform.transform_tangent(*tangent);
        }

        self
    }

    // TODO: Implement when I figured out how to preserve the scale
    /// This method is currently ***not*** implemented.
    fn look_at(self, _target: Point3, _view_up: Vec3) -> Self {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use std::f64::consts::{FRAC_PI_2, FRAC_PI_4};

    use approx::assert_relative_eq;

    use crystal_ball_math::{Point3, Transformable, Vec3, Vec4};

    use crate::TriangleMesh;

    #[test]
    fn triangle_mesh_translate() {
        let translation = Vec3::new(2.0, -1.0, 0.0);
        let cube = TriangleMesh::cube(1.0);
        let cube_translated = cube.clone().translate(translation);

        assert_relative_eq!(cube_translated.vertex_mean(), translation.to_point3());

        for (translated_vertex, vertex) in cube_translated.vertices.iter().zip(&cube.vertices) {
            assert_relative_eq!(*translated_vertex, *vertex + translation);
        }

        for (translated_normal, normal) in cube_translated.normals.iter().zip(&cube.normals) {
            assert_relative_eq!(*translated_normal, *normal);
        }

        for (translated_tangent, tangent) in cube_translated.tangents.iter().zip(&cube.tangents) {
            assert_relative_eq!(*translated_tangent, *tangent);
        }
    }

    #[test]
    fn triangle_mesh_rotate() {
        let cube = TriangleMesh::cube(1.0);
        let cube_rotated = cube
            .clone()
            .rotate_x(FRAC_PI_2)
            .rotate_y(FRAC_PI_2)
            .rotate_z(FRAC_PI_2);

        assert_relative_eq!(cube_rotated.vertex_mean(), Point3::ZERO);

        //  1  1 -1: -1  1 -1
        // -1  1  1:  1  1  1
        //  1  1  1:  1  1 -1
        // -1  1 -1: -1  1  1
        //  1 -1 -1: -1 -1 -1
        // -1 -1  1:  1 -1  1
        //  1 -1  1:  1 -1 -1
        // -1 -1 -1: -1 -1  1

        assert_relative_eq!(cube_rotated.vertices[0], Point3::new(-1.0, 1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[10], Point3::new(-1.0, 1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[23], Point3::new(-1.0, 1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[1], Point3::new(1.0, 1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[13], Point3::new(1.0, 1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[18], Point3::new(1.0, 1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[2], Point3::new(1.0, 1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[9], Point3::new(1.0, 1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[19], Point3::new(1.0, 1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[3], Point3::new(-1.0, 1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[14], Point3::new(-1.0, 1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[22], Point3::new(-1.0, 1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[4], Point3::new(-1.0, -1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[11], Point3::new(-1.0, -1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[20], Point3::new(-1.0, -1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[5], Point3::new(1.0, -1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[12], Point3::new(1.0, -1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[17], Point3::new(1.0, -1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[6], Point3::new(1.0, -1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[8], Point3::new(1.0, -1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[16], Point3::new(1.0, -1.0, -1.0));
        assert_relative_eq!(cube_rotated.vertices[7], Point3::new(-1.0, -1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[15], Point3::new(-1.0, -1.0, 1.0));
        assert_relative_eq!(cube_rotated.vertices[21], Point3::new(-1.0, -1.0, 1.0));

        for normal in &cube_rotated.normals[0..4] {
            assert_relative_eq!(*normal, Vec3::Y);
        }
        for normal in &cube_rotated.normals[4..8] {
            assert_relative_eq!(*normal, -Vec3::Y);
        }
        for normal in &cube_rotated.normals[8..12] {
            assert_relative_eq!(*normal, -Vec3::Z);
        }
        for normal in &cube_rotated.normals[12..16] {
            assert_relative_eq!(*normal, Vec3::Z);
        }
        for normal in &cube_rotated.normals[16..20] {
            assert_relative_eq!(*normal, Vec3::X);
        }
        for normal in &cube_rotated.normals[20..24] {
            assert_relative_eq!(*normal, -Vec3::X);
        }

        for tangent in &cube_rotated.tangents[0..4] {
            assert_relative_eq!(*tangent, Vec4::new(0.0, 0.0, 1.0, 1.0));
        }
        for tangent in &cube_rotated.tangents[4..8] {
            assert_relative_eq!(*tangent, Vec4::new(0.0, 0.0, -1.0, 1.0));
        }
        for tangent in &cube_rotated.tangents[8..24] {
            assert_relative_eq!(*tangent, Vec4::new(0.0, 1.0, 0.0, 1.0));
        }
    }

    #[test]
    fn triangle_mesh_scale() {
        let scale = Vec3::new(2.0, -1.0, 0.5);
        let cube = TriangleMesh::cube(1.0).rotate_y(FRAC_PI_4);
        let cube_scaled = cube.clone().scale_xyz(scale);

        assert_relative_eq!(cube_scaled.vertex_mean(), Point3::ZERO);

        for (scaled_vertex, vertex) in cube_scaled.vertices.iter().zip(&cube.vertices) {
            assert_relative_eq!(
                *scaled_vertex,
                Point3::new(vertex.x * scale.x, vertex.y * scale.y, vertex.z * scale.z)
            );
        }

        for normal in &cube_scaled.normals[0..4] {
            assert_relative_eq!(*normal, -Vec3::Y);
        }
        for normal in &cube_scaled.normals[4..8] {
            assert_relative_eq!(*normal, Vec3::Y);
        }
        for normal in &cube_scaled.normals[8..12] {
            assert_relative_eq!(
                *normal,
                Vec3::new(0.2425359578, 0.0, -0.9701424170),
                max_relative = 1e-5
            );
        }
        for normal in &cube_scaled.normals[12..16] {
            assert_relative_eq!(
                *normal,
                Vec3::new(-0.2425359578, 0.0, 0.9701424170),
                max_relative = 1e-5
            );
        }
        for normal in &cube_scaled.normals[16..20] {
            assert_relative_eq!(
                *normal,
                Vec3::new(0.2425359578, 0.0, 0.9701424170),
                max_relative = 1e-5
            );
        }
        for normal in &cube_scaled.normals[20..24] {
            assert_relative_eq!(
                *normal,
                Vec3::new(-0.2425359578, 0.0, -0.9701424170),
                max_relative = 1e-5
            );
        }

        for tangent in &cube_scaled.tangents[0..4] {
            assert_relative_eq!(
                *tangent,
                Vec4::new(-0.9701420966, 0.0, 0.2425372391, 1.0),
                max_relative = 1e-5
            );
        }
        for tangent in &cube_scaled.tangents[4..8] {
            assert_relative_eq!(
                *tangent,
                Vec4::new(0.9701420966, 0.0, -0.2425372391, 1.0),
                max_relative = 1e-5
            );
        }
        for tangent in &cube_scaled.tangents[8..24] {
            assert_relative_eq!(*tangent, Vec4::new(0.0, -1.0, 0.0, 1.0));
        }
    }
}
