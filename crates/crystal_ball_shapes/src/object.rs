use std::sync::Arc;

use crystal_ball_materials::{IntoArcDynMaterial, Material};
use crystal_ball_math::{Bounds3, Hit, Ray};

use crate::{IntoArcDynShape, Shape};

/// A [`Shape`] paired with a [`Material`].
#[derive(Clone)]
pub struct Object {
    // TODO: Arc ist pointless, cause you need to create a new shape if you want to change it's transform
    pub shape: Arc<dyn Shape>,
    pub material: Arc<dyn Material>,
}

impl Object {
    /// Create a new [`Object`].
    pub fn new(shape: impl IntoArcDynShape, material: impl IntoArcDynMaterial) -> Self {
        Object {
            shape: shape.into_arc_dyn_shape(),
            material: material.into_arc_dyn_material(),
        }
    }
}

impl Shape for Object {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        self.shape.intersects(ray)
    }

    fn bounds(&self) -> Bounds3 {
        self.shape.bounds()
    }
}
