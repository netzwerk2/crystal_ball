#![cfg_attr(doc, feature(doc_auto_cfg))]

//! glTF loading for Crystal Ball.

pub use objects::*;
pub use scene::*;

mod objects;
mod scene;
