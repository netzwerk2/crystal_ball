use std::path::Path;

use gltf::camera::Projection;
use gltf::Document;

use crate::objects::load_gltf_document;
use crystal_ball_error::Error;
use crystal_ball_math::{Mat4, Transform};
use crystal_ball_rendering::{Camera, Scene};
use crystal_ball_shapes::BVH;

/// Load a scene from a [glTF](https://registry.khronos.org/glTF/specs/2.0/glTF-2.0.html) file.
///
/// This loads the camera as well as all meshes.
/// If there are multiple cameras, the first one will be chosen.
/// If there is no camera, [`Camera::default`] will be used.
///
/// Orthographic camera projections are currently not supported.
/// For more information see [`crate::load_gltf_objects`].
///
/// Returns an [`Error`] if the document can not be parsed.
pub fn load_gltf_scene<P: AsRef<Path>>(path: P) -> Result<Scene, Error> {
    let (document, buffers, images) = gltf::import(path)?;

    let camera = load_camera(&document).unwrap_or_default();

    let objects = load_gltf_document(&document, &buffers, &images)?;
    let bvh = BVH::init(4, objects);

    Ok(Scene {
        camera,
        bvh,
        ..Default::default()
    })
}

/// Load the first camera from a glTF file.
fn load_camera(document: &Document) -> Option<Camera> {
    let camera = document.cameras().next()?;

    if document.cameras().len() > 1 {
        eprintln!("WARNING: multiple cameras found. Using the first one");
    }

    let projection = match camera.projection() {
        Projection::Orthographic(_) => {
            eprintln!("ERROR: orthographic camera projection is not supported");
            return None;
        }
        Projection::Perspective(p) => p,
    };

    let fov = projection.yfov() as f64;

    let fov = match projection.aspect_ratio() {
        Some(aspect_ratio) if aspect_ratio < 1.0 => fov * aspect_ratio as f64,
        _ => fov,
    };

    let camera_node = document
        .nodes()
        .find(|n| {
            if let Some(c) = n.camera() {
                c.index() == camera.index()
            } else {
                false
            }
        })
        .unwrap();
    let mat4 = Mat4::from(camera_node.transform().matrix().map(|r| r.map(f64::from))).transpose();
    let mat4_inverse = mat4.inverse();

    let transform = Transform::new(mat4, mat4_inverse);

    Some(Camera {
        transform,
        fov,
        ..Default::default()
    })
}
