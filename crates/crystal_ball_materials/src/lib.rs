#![cfg_attr(doc, feature(doc_auto_cfg))]

//! Data types and functions related to materials for Crystal Ball.
use std::sync::Arc;

use nanorand::tls::TlsWyRand;

use crystal_ball_color::{Color, Texture};
use crystal_ball_math::{Hit, Point2, Ray, Vec3};
pub use pbr_material::*;

mod pbr_material;

/// Characterize optical properties of a surface.
pub trait Material: Send + Sync {
    /// Generate the next ray to be traced.
    ///
    /// If the path is to be terminated, [`None`] is returned.
    fn next_ray(&self, ray: Ray, hit: Hit, rng: &mut TlsWyRand) -> Option<Ray>;

    /// Return the color at the given UV coordinates.
    ///
    /// The first item is the reflected color, the second item is the emitted color.
    fn get_color(&self, uv: Point2) -> (Color, Color);
}

/// Compute the combination of a [`Color`], an optional [`Texture`] and a given `strength`
/// for the given UV coordinates.
pub fn compute_color(
    color: Color,
    texture: &Option<Arc<dyn Texture>>,
    strength: f64,
    uv: Point2,
) -> Color {
    strength as f32
        * color
        * match texture {
            None => Color::WHITE,
            Some(t) => t.get_pixel(uv.x, uv.y),
        }
}

/// Compute the normal at the given UV coordinates using the `normal_texture`.
///
/// If `normal_texture` is [`None`], `normal` is returned.
pub fn compute_normal(
    normal: Vec3,
    tangent: Vec3,
    bitangent: Vec3,
    normal_texture: &Option<Arc<dyn Texture>>,
    uv: Point2,
) -> Vec3 {
    match normal_texture {
        None => normal,
        Some(texture) => {
            let texture_normal = Vec3::from(texture.get_pixel(uv.x, uv.y)) * 2.0 - Vec3::splat(1.0);

            (texture_normal.x * tangent + texture_normal.y * bitangent + texture_normal.z * normal)
                .normalize()
        }
    }
}

/// A struct containing common index of refraction values as constants.
#[non_exhaustive]
pub struct IOR;

impl IOR {
    pub const ACRYLIC_GLASS: f64 = 1.491;
    pub const AIR: f64 = 1.0;
    pub const ALCOHOL: f64 = 1.36;
    pub const BEER: f64 = 1.345;
    pub const CRYSTAL: f64 = 2.0;
    pub const DIAMOND: f64 = 2.418;
    pub const EMERALD: f64 = 1.583;
    pub const GLASS: f64 = 1.5;
    pub const ICE: f64 = 1.309;
    pub const LUCITE: f64 = 1.495;
    pub const MERCURY: f64 = 1.62;
    pub const PEARL: f64 = 1.61;
    pub const VODKA: f64 = 1.363;
    pub const WATER: f64 = 1.325;
}

/// A trait allowing an `Arc<dyn Material>` to be constructed either from a struct
/// implementing [`Material`] or from an [`Arc`] itself.
pub trait IntoArcDynMaterial {
    /// Perform the conversion of `self` into `Arc<dyn Material>`.
    fn into_arc_dyn_material(self) -> Arc<dyn Material>;
}

impl<T: Material + 'static> IntoArcDynMaterial for T {
    fn into_arc_dyn_material(self) -> Arc<dyn Material> {
        Arc::new(self)
    }
}

impl<T: Material + 'static> IntoArcDynMaterial for Arc<T> {
    fn into_arc_dyn_material(self) -> Arc<dyn Material> {
        self
    }
}

impl IntoArcDynMaterial for Arc<dyn Material> {
    fn into_arc_dyn_material(self) -> Arc<dyn Material> {
        self
    }
}
