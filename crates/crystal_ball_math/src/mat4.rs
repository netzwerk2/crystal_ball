#[cfg(doc)]
use crate::Transform;
use crate::{Point3, Vec3};
#[cfg(feature = "approx")]
use approx::{AbsDiffEq, RelativeEq, UlpsEq};
use std::ops::{Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Sub, SubAssign};

/// A 4x4 row-major matrix.
///
/// For constructing transformations see [`Transform`].
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Mat4 {
    pub matrix: [[f64; 4]; 4],
}

impl Mat4 {
    #[rustfmt::skip]
    pub const IDENTITY: Mat4 = Mat4 {
        matrix: [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ],
    };

    /// Create a new [`Mat4`] from individual elements.
    #[rustfmt::skip]
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        a00: f64, a01: f64, a02: f64, a03: f64,
        a10: f64, a11: f64, a12: f64, a13: f64,
        a20: f64, a21: f64, a22: f64, a23: f64,
        a30: f64, a31: f64, a32: f64, a33: f64,
    ) -> Self {
        Mat4 {
            matrix: [
                [a00, a01, a02, a03],
                [a10, a11, a12, a13],
                [a20, a21, a22, a23],
                [a30, a31, a32, a33],
            ]
        }
    }

    /// Calculate the determinant of a 2x2 matrix.
    fn determinant_2x2(matrix: [[f64; 2]; 2]) -> f64 {
        matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]
    }

    /// Calculate the determinant of a 3x3 matrix using Laplace expansion.
    fn determinant_3x3(matrix: [[f64; 3]; 3]) -> f64 {
        matrix[0][0]
            * Self::determinant_2x2([[matrix[1][1], matrix[1][2]], [matrix[2][1], matrix[2][2]]])
            - matrix[0][1]
                * Self::determinant_2x2([
                    [matrix[1][0], matrix[1][2]],
                    [matrix[2][0], matrix[2][2]],
                ])
            + matrix[0][2]
                * Self::determinant_2x2([
                    [matrix[1][0], matrix[1][1]],
                    [matrix[2][0], matrix[2][1]],
                ])
    }

    /// Calculate the determinant of a 3x3 submatrix, excluding the specified row and column.
    fn minor(self, row: usize, column: usize) -> f64 {
        let mut matrix = [[0.0; 3]; 3];

        for (j, y) in (0..4).filter(|y| *y != row).enumerate() {
            for (i, x) in (0..4).filter(|x| *x != column).enumerate() {
                matrix[j][i] = self[y][x];
            }
        }

        Self::determinant_3x3(matrix)
    }

    /// Calculate the cofactor (signed minor of the 3x3 submatrix, excluding the specified row and column).
    fn cofactor(&self, row: usize, column: usize) -> f64 {
        (-1i32).pow((row + column) as u32) as f64 * self.minor(row, column)
    }

    /// Calculate the matrix's determinant using Laplace expansion.
    pub fn determinant(&self) -> f64 {
        (0..4).map(|y| self.cofactor(y, 0) * self[y][0]).sum()
    }

    /// Calculate the matrix's inverse using [Cramer's rule](https://en.wikipedia.org/wiki/Cramer%27s_rule#Finding_inverse_matrix).
    ///
    /// # Panics
    /// Panics if the matrix can not be inverted (determinant = 0).
    pub fn inverse(&self) -> Self {
        let mut matrix = [[0.0; 4]; 4];

        let determinant = self.determinant();
        assert_ne!(determinant, 0.0);

        let determinant_inverse = 1.0 / determinant;

        for (y, row) in matrix.iter_mut().enumerate() {
            for (x, value) in row.iter_mut().enumerate() {
                *value = self.cofactor(x, y) * determinant_inverse;
            }
        }

        Self { matrix }
    }

    /// Transpose the matrix.
    pub fn transpose(&self) -> Self {
        Self {
            #[rustfmt::skip]
            matrix: [
                [self[0][0], self[1][0], self[2][0], self[3][0]],
                [self[0][1], self[1][1], self[2][1], self[3][1]],
                [self[0][2], self[1][2], self[2][2], self[3][2]],
                [self[0][3], self[1][3], self[2][3], self[3][3]],
            ],
        }
    }
}

impl Add<Mat4> for Mat4 {
    type Output = Mat4;

    fn add(self, rhs: Mat4) -> Self::Output {
        Self {
            matrix: [
                [
                    self[0][0] + rhs[0][0],
                    self[0][1] + rhs[0][1],
                    self[0][2] + rhs[0][2],
                    self[0][3] + rhs[0][3],
                ],
                [
                    self[1][0] + rhs[1][0],
                    self[1][1] + rhs[1][1],
                    self[1][2] + rhs[1][2],
                    self[1][3] + rhs[1][3],
                ],
                [
                    self[2][0] + rhs[2][0],
                    self[2][1] + rhs[2][1],
                    self[2][2] + rhs[2][2],
                    self[2][3] + rhs[2][3],
                ],
                [
                    self[3][0] + rhs[3][0],
                    self[3][1] + rhs[3][1],
                    self[3][2] + rhs[3][2],
                    self[3][3] + rhs[3][3],
                ],
            ],
        }
    }
}

impl AddAssign<Mat4> for Mat4 {
    fn add_assign(&mut self, rhs: Mat4) {
        *self = *self + rhs
    }
}

impl Sub<Mat4> for Mat4 {
    type Output = Mat4;

    fn sub(self, rhs: Mat4) -> Self::Output {
        Self {
            matrix: [
                [
                    self[0][0] - rhs[0][0],
                    self[0][1] - rhs[0][1],
                    self[0][2] - rhs[0][2],
                    self[0][3] - rhs[0][3],
                ],
                [
                    self[1][0] - rhs[1][0],
                    self[1][1] - rhs[1][1],
                    self[1][2] - rhs[1][2],
                    self[1][3] - rhs[1][3],
                ],
                [
                    self[2][0] - rhs[2][0],
                    self[2][1] - rhs[2][1],
                    self[2][2] - rhs[2][2],
                    self[2][3] - rhs[2][3],
                ],
                [
                    self[3][0] - rhs[3][0],
                    self[3][1] - rhs[3][1],
                    self[3][2] - rhs[3][2],
                    self[3][3] - rhs[3][3],
                ],
            ],
        }
    }
}

impl SubAssign<Mat4> for Mat4 {
    fn sub_assign(&mut self, rhs: Mat4) {
        *self = *self - rhs
    }
}

impl Mul<Mat4> for Mat4 {
    type Output = Mat4;

    fn mul(self, rhs: Mat4) -> Self::Output {
        let mut matrix = [[0.0; 4]; 4];
        for y in 0..4 {
            for x in 0..4 {
                for i in 0..4 {
                    matrix[y][x] += self[y][i] * rhs[i][x]
                }
            }
        }

        Mat4 { matrix }
    }
}

impl Mul<Vec3> for Mat4 {
    type Output = Vec3;

    fn mul(self, rhs: Vec3) -> Self::Output {
        Vec3::new(
            self[0][0] * rhs.x + self[0][1] * rhs.y + self[0][2] * rhs.z,
            self[1][0] * rhs.x + self[1][1] * rhs.y + self[1][2] * rhs.z,
            self[2][0] * rhs.x + self[2][1] * rhs.y + self[2][2] * rhs.z,
        )
    }
}

impl Mul<Point3> for Mat4 {
    type Output = Point3;

    fn mul(self, rhs: Point3) -> Self::Output {
        let x = self[0][0] * rhs.x + self[0][1] * rhs.y + self[0][2] * rhs.z + self[0][3];
        let y = self[1][0] * rhs.x + self[1][1] * rhs.y + self[1][2] * rhs.z + self[1][3];
        let z = self[2][0] * rhs.x + self[2][1] * rhs.y + self[2][2] * rhs.z + self[2][3];
        let w = self[3][0] * rhs.x + self[3][1] * rhs.y + self[3][2] * rhs.z + self[3][3];

        let point = Point3::new(x, y, z);

        if w == 1.0 {
            point
        } else {
            point / w
        }
    }
}

impl Mul<f64> for Mat4 {
    type Output = Mat4;

    fn mul(self, rhs: f64) -> Self::Output {
        let mut matrix = [[0.0; 4]; 4];
        for y in 0..4 {
            for x in 0..4 {
                matrix[y][x] = self[y][x] * rhs;
            }
        }

        Mat4 { matrix }
    }
}

impl MulAssign<f64> for Mat4 {
    fn mul_assign(&mut self, rhs: f64) {
        *self = *self * rhs
    }
}

impl Mul<Mat4> for f64 {
    type Output = Mat4;

    fn mul(self, rhs: Mat4) -> Self::Output {
        let mut matrix = [[0.0; 4]; 4];
        for y in 0..4 {
            for x in 0..4 {
                matrix[y][x] = rhs[y][x] * self;
            }
        }

        Mat4 { matrix }
    }
}

impl MulAssign<Mat4> for Mat4 {
    fn mul_assign(&mut self, rhs: Mat4) {
        *self = *self * rhs
    }
}

impl Div<f64> for Mat4 {
    type Output = Mat4;

    fn div(self, rhs: f64) -> Self::Output {
        let rhs_inverse = 1.0 / rhs;

        let mut matrix = [[0.0; 4]; 4];
        for y in 0..4 {
            for x in 0..4 {
                matrix[y][x] = self[y][x] * rhs_inverse;
            }
        }

        Mat4 { matrix }
    }
}

impl DivAssign<f64> for Mat4 {
    fn div_assign(&mut self, rhs: f64) {
        *self = *self / rhs
    }
}

impl Default for Mat4 {
    fn default() -> Self {
        Mat4::IDENTITY
    }
}

impl From<[[f64; 4]; 4]> for Mat4 {
    fn from(a: [[f64; 4]; 4]) -> Self {
        Mat4 { matrix: a }
    }
}

impl Index<usize> for Mat4 {
    type Output = [f64; 4];

    fn index(&self, index: usize) -> &Self::Output {
        &self.matrix[index]
    }
}

impl IndexMut<usize> for Mat4 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.matrix[index]
    }
}

#[cfg(feature = "approx")]
impl AbsDiffEq for Mat4 {
    type Epsilon = <f64 as AbsDiffEq>::Epsilon;

    fn default_epsilon() -> Self::Epsilon {
        f64::default_epsilon()
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        let matrix = self
            .matrix
            .iter()
            .flat_map(|r| r.as_slice())
            .copied()
            .collect::<Vec<f64>>();
        let other_matrix = other
            .matrix
            .iter()
            .flat_map(|r| r.as_slice())
            .copied()
            .collect::<Vec<f64>>();

        <[f64]>::abs_diff_eq(&matrix, &other_matrix, epsilon)
    }
}

#[cfg(feature = "approx")]
impl RelativeEq for Mat4 {
    fn default_max_relative() -> Self::Epsilon {
        f64::default_max_relative()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        let matrix = self
            .matrix
            .iter()
            .flat_map(|r| r.as_slice())
            .copied()
            .collect::<Vec<f64>>();
        let other_matrix = other
            .matrix
            .iter()
            .flat_map(|r| r.as_slice())
            .copied()
            .collect::<Vec<f64>>();

        <[f64]>::relative_eq(&matrix, &other_matrix, epsilon, max_relative)
    }
}

#[cfg(feature = "approx")]
impl UlpsEq for Mat4 {
    fn default_max_ulps() -> u32 {
        f64::default_max_ulps()
    }

    fn ulps_eq(&self, other: &Self, epsilon: <f64 as AbsDiffEq>::Epsilon, max_ulps: u32) -> bool {
        let matrix = self
            .matrix
            .iter()
            .flat_map(|r| r.as_slice())
            .copied()
            .collect::<Vec<f64>>();
        let other_matrix = other
            .matrix
            .iter()
            .flat_map(|r| r.as_slice())
            .copied()
            .collect::<Vec<f64>>();

        <[f64]>::ulps_eq(&matrix, &other_matrix, epsilon, max_ulps)
    }
}

#[cfg(test)]
mod tests {
    use std::f64::consts::FRAC_PI_2;

    use approx::{assert_relative_eq, AbsDiffEq};

    use crate::{Mat4, Point3, Transform, Vec3};

    #[test]
    fn mat4_mul_mat4() {
        assert_relative_eq!(Mat4::IDENTITY * Mat4::IDENTITY, Mat4::IDENTITY);

        let mat4 = Mat4::from([
            [-3.0, -1.0, 2.0, -3.0],
            [-3.0, 1.0, 2.0, -2.0],
            [-2.0, 3.0, 0.0, 1.0],
            [1.0, -2.0, -3.0, 1.0],
        ]);

        assert_relative_eq!(Mat4::IDENTITY * mat4, mat4);
        assert_relative_eq!(mat4 * Mat4::IDENTITY, mat4);

        assert_relative_eq!(
            mat4 * Mat4::from([
                [1.0, 0.0, 2.0, 3.0],
                [4.0, 7.0, 8.0, 1.0],
                [5.0, 0.0, 1.0, 1.0],
                [6.0, 1.0, 2.0, 1.0]
            ]),
            Mat4::from([
                [-15.0, -10.0, -18.0, -11.0],
                [-1.0, 5.0, 0.0, -8.0],
                [16.0, 22.0, 22.0, -2.0],
                [-16.0, -13.0, -15.0, -1.0]
            ])
        );
    }

    #[test]
    fn mat4_associativity() {
        let vec3 = Vec3::new(-1.5, 24.537, -4.12);

        let phi = 4.217;

        let origin = Point3::new(0.3, 1.4, 2.9);

        let rotate_x_mat4 = Transform::rotate_x(origin, phi).mat4;
        let rotate_y_mat4 = Transform::rotate_y(origin, phi).mat4;
        let rotate_z_mat4 = Transform::rotate_z(origin, phi).mat4;

        assert_relative_eq!(
            (rotate_x_mat4 * rotate_y_mat4 * rotate_z_mat4) * vec3,
            (rotate_x_mat4 * (rotate_y_mat4 * (rotate_z_mat4 * vec3))),
            max_relative = 1e-14
        );
    }

    #[test]
    fn mat4_mul_vec3() {
        let vec3 = Vec3::new(4.0, 2.0, -2.0);

        assert_relative_eq!(Mat4::IDENTITY * vec3, vec3);

        let mat4 = Mat4::from([
            [-3.0, -1.0, 2.0, -3.0],
            [-3.0, 1.0, 2.0, -2.0],
            [-2.0, 3.0, 0.0, 1.0],
            [1.0, -2.0, -3.0, 1.0],
        ]);

        assert_relative_eq!(mat4 * vec3, Vec3::new(-18.0, -14.0, -2.0));

        let translation_mat4 = Transform::translate(Vec3::new(3.0, -2.0, -1.0)).mat4;
        assert_relative_eq!(translation_mat4 * vec3, vec3);

        let rotation_mat4 = Transform::rotate_x(Point3::new(-7.0, 2.0, 5.0), FRAC_PI_2).mat4;
        assert_relative_eq!(rotation_mat4 * vec3, Vec3::new(4.0, 2.0, 2.0));
    }

    #[test]
    fn mat4_mul_point3() {
        let point3 = Point3::new(4.0, 2.0, -2.0);

        assert_relative_eq!(Mat4::IDENTITY * point3, point3);

        let mat4 = Mat4::from([
            [-3.0, -1.0, 2.0, -3.0],
            [-3.0, 1.0, 2.0, -2.0],
            [-2.0, 3.0, 0.0, 1.0],
            [1.0, -2.0, -3.0, 1.0],
        ]);

        assert_relative_eq!(mat4 * point3, Point3::new(-3.0, -16.0 / 7.0, -1.0 / 7.0));

        let offset = Vec3::new(3.0, -2.0, -1.0);
        let translation_mat4 = Transform::translate(offset).mat4;
        assert_relative_eq!(translation_mat4 * point3, point3 + offset);

        let rotation_mat4 = Transform::rotate_x(Point3::new(-7.0, 2.0, 5.0), FRAC_PI_2).mat4;
        assert_relative_eq!(rotation_mat4 * point3, Point3::new(4.0, 9.0, 5.0));
    }

    #[test]
    fn determinant_2x2() {
        assert_relative_eq!(Mat4::determinant_2x2([[1.0, 0.0], [0.0, 1.0]]), 1.0);
        assert_relative_eq!(Mat4::determinant_2x2([[0.0; 2]; 2]), 0.0);
        assert_relative_eq!(Mat4::determinant_2x2([[42.0, 0.0]; 2]), 0.0);
        assert_relative_eq!(Mat4::determinant_2x2([[1.0, 3.0], [-2.0, 5.0]]), 11.0);
        assert_relative_eq!(Mat4::determinant_2x2([[-2.0, 5.0], [1.0, 3.0]]), -11.0);
        assert_relative_eq!(
            Mat4::determinant_2x2([[1.0 * 2.0, 3.0], [-2.0 * 2.0, 5.0]]),
            11.0 * 2.0
        );
        assert_relative_eq!(
            Mat4::determinant_2x2([[1.0 * 2.0, 3.0], [-2.0 * 2.0, 5.0]]),
            11.0 * 2.0
        );
        assert_relative_eq!(
            Mat4::determinant_2x2([[1.0, 3.0 + 2.0], [-2.0, 5.0 - 1.0]]),
            Mat4::determinant_2x2([[1.0, 3.0], [-2.0, 5.0]])
                + Mat4::determinant_2x2([[1.0, 2.0], [-2.0, -1.0]])
        );
    }

    #[test]
    fn determinant_3x3() {
        assert_relative_eq!(
            Mat4::determinant_3x3([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
            1.0
        );
        assert_relative_eq!(Mat4::determinant_3x3([[0.0; 3]; 3]), 0.0);
        assert_relative_eq!(Mat4::determinant_3x3([[42.0, 5.0, 3.0]; 3]), 0.0);
        assert_relative_eq!(
            Mat4::determinant_3x3([[1.0, 2.0, -3.0], [3.0, 2.0, 1.0], [2.0, 1.0, 3.0]]),
            -6.0
        );
        assert_relative_eq!(
            Mat4::determinant_3x3([[2.0, 1.0, 3.0], [3.0, 2.0, 1.0], [1.0, 2.0, -3.0]]),
            6.0
        );
        assert_relative_eq!(
            Mat4::determinant_3x3([
                [1.0, 2.0 * -0.5, -3.0],
                [3.0, 2.0 * -0.5, 1.0],
                [2.0, 1.0 * -0.5, 3.0]
            ]),
            -6.0 * -0.5
        );
        assert_relative_eq!(
            Mat4::determinant_3x3([
                [1.0, 2.0 * -0.5, -3.0],
                [3.0, 2.0 * -0.5, 1.0],
                [2.0, 1.0 * -0.5, 3.0]
            ]),
            -6.0 * -0.5
        );
    }

    #[test]
    fn mat4_determinant() {
        assert_relative_eq!(Mat4::IDENTITY.determinant(), 1.0);
        assert_relative_eq!(Mat4::from([[0.0; 4]; 4]).determinant(), 0.0);
        assert_relative_eq!(Mat4::from([[3.0, 2.0, 1.0, 0.0]; 4]).determinant(), 0.0);
        assert_relative_eq!(
            Mat4::from([
                [1.0, 0.0, 2.0, 3.0],
                [4.0, 7.0, 8.0, 1.0],
                [5.0, 0.0, 1.0, 1.0],
                [6.0, 1.0, 2.0, 1.0]
            ])
            .determinant(),
            -8.0
        );
        assert_relative_eq!(
            Mat4::from([
                [4.0, 7.0, 8.0, 1.0],
                [1.0, 0.0, 2.0, 3.0],
                [5.0, 0.0, 1.0, 1.0],
                [6.0, 1.0, 2.0, 1.0]
            ])
            .determinant(),
            8.0
        );
        assert_relative_eq!(
            Mat4::from([
                [1.0, 0.0, 2.0, 3.0 * 3.0],
                [4.0, 7.0, 8.0, 1.0 * 3.0],
                [5.0, 0.0, 1.0, 1.0 * 3.0],
                [6.0, 1.0, 2.0, 1.0 * 3.0]
            ])
            .determinant(),
            -8.0 * 3.0
        );
        assert_relative_eq!(
            Mat4::from([
                [1.0, 0.0, 2.0, 3.0],
                [4.0, 7.0, 8.0, 1.0],
                [5.0 * 3.0, 0.0 * 3.0, 1.0 * 3.0, 1.0 * 3.0],
                [6.0, 1.0, 2.0, 1.0]
            ])
            .determinant(),
            -8.0 * 3.0
        );
    }

    #[test]
    fn mat4_inverse() {
        assert_relative_eq!(Mat4::IDENTITY, Mat4::IDENTITY.inverse());

        let mat4 = Mat4::from([
            [-3.0, -1.0, 2.0, -3.0],
            [-3.0, 1.0, 2.0, -2.0],
            [-2.0, 3.0, 0.0, 1.0],
            [1.0, -2.0, -3.0, 1.0],
        ]);
        let mat4_inverse = Mat4::from([
            [-11.0 / 7.0, 2.0, -1.0, 2.0 / 7.0],
            [-15.0 / 7.0, 3.0, -1.0, 4.0 / 7.0],
            [2.0, -3.0, 1.0, -1.0],
            [23.0 / 7.0, -5.0, 2.0, -8.0 / 7.0],
        ]);

        assert_relative_eq!(mat4_inverse, mat4.inverse());
        assert_relative_eq!(
            mat4 * mat4_inverse,
            Mat4::IDENTITY,
            epsilon = 2.0 * Mat4::default_epsilon()
        );
        assert_relative_eq!(
            mat4_inverse * mat4,
            Mat4::IDENTITY,
            epsilon = 4.0 * Mat4::default_epsilon()
        );
        assert_relative_eq!(
            mat4,
            mat4_inverse.inverse(),
            max_relative = 1e-13,
            epsilon = 15.0 * Mat4::default_epsilon()
        );

        assert_relative_eq!(
            Mat4::from(mat4.matrix.map(|row| row.map(|x| x * -2.0))).inverse(),
            1.0 / -2f64 * mat4_inverse
        );

        assert_relative_eq!(
            mat4_inverse.determinant(),
            1.0 / mat4.determinant(),
            max_relative = 1e-14
        );
    }

    #[test]
    fn mat4_transpose() {
        assert_relative_eq!(Mat4::IDENTITY, Mat4::IDENTITY.transpose());

        let mat4 = Mat4::from([
            [-3.0, -1.0, 2.0, -3.0],
            [-3.0, 1.0, 2.0, -2.0],
            [-2.0, 3.0, 0.0, 1.0],
            [1.0, -2.0, -3.0, 1.0],
        ]);
        let mat4_inverse = Mat4::from([
            [-11.0 / 7.0, 2.0, -1.0, 2.0 / 7.0],
            [-15.0 / 7.0, 3.0, -1.0, 4.0 / 7.0],
            [2.0, -3.0, 1.0, -1.0],
            [23.0 / 7.0, -5.0, 2.0, -8.0 / 7.0],
        ]);

        assert_relative_eq!(mat4.transpose().transpose(), mat4);
        assert_relative_eq!(mat4.transpose().inverse(), mat4_inverse.transpose());
    }
}
