use crate::Point2;

/// A 2-dimensional bounding rectangle represented by its minimum and maximum corner.
///
/// Defaults to [`f64::INFINITY`] for `min` and [`f64::NEG_INFINITY`] for `max`.
/// This way, including *any* point will result in the correct bounding rectangle.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Bounds2 {
    pub min: Point2,
    pub max: Point2,
}

impl Default for Bounds2 {
    fn default() -> Self {
        Bounds2 {
            min: Point2::splat(f64::INFINITY),
            max: Point2::splat(f64::NEG_INFINITY),
        }
    }
}

impl Bounds2 {
    /// Create a new [`Bounds2`].
    pub fn new(point_a: Point2, point_b: Point2) -> Self {
        Bounds2 {
            min: Point2::new(point_a.x.min(point_b.x), point_a.y.min(point_b.y)),
            max: Point2::new(point_a.x.max(point_b.x), point_a.y.max(point_b.y)),
        }
    }
}
