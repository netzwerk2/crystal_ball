#![cfg_attr(doc, feature(doc_auto_cfg))]

//! Data types and functions related to math for Crystal Ball.

use nanorand::tls::TlsWyRand;
use nanorand::Rng;

pub use bounds2::*;
pub use bounds3::*;
pub use hit::*;
pub use mat4::*;
pub use point2::*;
pub use point3::*;
pub use ray::*;
pub use transform::*;
pub use vec2::*;
pub use vec3::*;
pub use vec4::*;

mod bounds2;
mod bounds3;
mod hit;
mod mat4;
mod point2;
mod point3;
mod ray;
mod transform;
mod vec2;
mod vec3;
mod vec4;

/// X, Y, and Z directions expressed by indices.
#[derive(Copy, Clone, Debug, Default)]
pub enum XYZEnum {
    #[default]
    X = 0,
    Y = 1,
    Z = 2,
}

/// Factor for converting random u64 to uniformly distributed double.
const FACTOR: f64 = 5.421010862427522e-20; // 2.0_f64.powi(-64)

/// Generate a random floating point number between min and max.
pub fn random_float(rng: &mut TlsWyRand, min: f64, max: f64) -> f64 {
    // Casting to signed before casting to float can be faster depending on your hardware.
    // See Doornik, J.A.: Conversion of high-period random numbers to floating point,
    // CM transactions on modeling and computer simulation, 17(1) (2007)
    (rng.generate::<u64>() as i64 as f64 * FACTOR + 0.5) * (max - min) + min
}
