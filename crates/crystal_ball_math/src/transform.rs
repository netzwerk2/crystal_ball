use std::fmt::Debug;
use std::ops::Mul;

use crate::{Bounds3, Mat4, Point3, Ray, Vec3, Vec4};

/// A transform in 3D space consisting of a transformation matrix and its inverse.
#[derive(Copy, Clone, Default, Debug, PartialEq)]
pub struct Transform {
    pub mat4: Mat4,
    pub mat4_inverse: Mat4,
}

impl Transform {
    /// Create a new [`Transform`].
    pub fn new(mat4: Mat4, mat4_inverse: Mat4) -> Self {
        Transform { mat4, mat4_inverse }
    }

    /// Return the inverse of `self`.
    ///
    /// This switches `mat4` and `mat4_inverse`.
    pub fn inverse(&self) -> Self {
        Transform::new(self.mat4_inverse, self.mat4)
    }

    /// Create a new [`Transform`] from a translation.
    pub fn translate(offset: Vec3) -> Self {
        #[rustfmt::skip]
            let mat4 = Mat4::new(
            1.0, 0.0, 0.0, offset.x,
            0.0, 1.0, 0.0, offset.y,
            0.0, 0.0, 1.0, offset.z,
            0.0, 0.0, 0.0, 1.0,
        );
        #[rustfmt::skip]
            let mat4_inverse = Mat4::new(
            1.0, 0.0, 0.0, -offset.x,
            0.0, 1.0, 0.0, -offset.y,
            0.0, 0.0, 1.0, -offset.z,
            0.0, 0.0, 0.0, 1.0,
        );

        Transform { mat4, mat4_inverse }
    }

    /// Create a new [`Transform`] from a rotation around a specified [`Point3`] along the +X axis.
    ///
    /// The angle is given in radians.
    pub fn rotate_x(origin: Point3, angle: f64) -> Self {
        let angle_sin = angle.sin();
        let angle_cos = angle.cos();

        #[rustfmt::skip]
            let mat4 = Mat4::new(
            1.0, 0.0, 0.0, 0.0,
            0.0, angle_cos, -angle_sin, origin.y - angle_cos * origin.y + angle_sin * origin.z,
            0.0, angle_sin, angle_cos, origin.z - angle_sin * origin.y - angle_cos * origin.z,
            0.0, 0.0, 0.0, 1.0,
        );
        #[rustfmt::skip]
            let mat4_inverse = Mat4::new(
            1.0, 0.0, 0.0, 0.0,
            0.0, angle_cos, angle_sin, origin.y - angle_cos * origin.y - angle_sin * origin.z,
            0.0, -angle_sin, angle_cos, origin.z + angle_sin * origin.y - angle_cos * origin.z,
            0.0, 0.0, 0.0, 1.0,
        );

        Transform { mat4, mat4_inverse }
    }

    /// Create a new [`Transform`] from a rotation around a specified [`Point3`] along the +Y axis.
    ///
    /// The angle is given in radians.
    pub fn rotate_y(origin: Point3, angle: f64) -> Self {
        let angle_sin = angle.sin();
        let angle_cos = angle.cos();

        #[rustfmt::skip]
            let mat4 = Mat4::new(
            angle_cos, 0.0, angle_sin, origin.x - angle_cos * origin.x - angle_sin * origin.z,
            0.0, 1.0, 0.0, 0.0,
            -angle_sin, 0.0, angle_cos, origin.z + angle_sin * origin.x - angle_cos * origin.z,
            0.0, 0.0, 0.0, 1.0,
        );
        #[rustfmt::skip]
            let mat4_inverse = Mat4::new(
            angle_cos, 0.0, -angle_sin, origin.x - angle_cos * origin.x + angle_sin * origin.z,
            0.0, 1.0, 0.0, 0.0,
            angle_sin, 0.0, angle_cos, origin.z - angle_sin * origin.x - angle_cos * origin.z,
            0.0, 0.0, 0.0, 1.0,
        );

        Transform { mat4, mat4_inverse }
    }

    /// Create a new [`Transform`] from a rotation around a specified [`Point3`] along the +Z axis.
    ///
    /// The angle is given in radians.
    pub fn rotate_z(origin: Point3, angle: f64) -> Self {
        let angle_sin = angle.sin();
        let angle_cos = angle.cos();

        #[rustfmt::skip]
            let mat4 = Mat4::new(
            angle_cos, -angle_sin, 0.0, origin.x - angle_cos * origin.x + angle_sin * origin.y,
            angle_sin, angle_cos, 0.0, origin.y - angle_sin * origin.x - angle_cos * origin.y,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0,
        );
        #[rustfmt::skip]
            let mat4_inverse = Mat4::new(
            angle_cos, angle_sin, 0.0, origin.x - angle_cos * origin.x - angle_sin * origin.y,
            -angle_sin, angle_cos, 0.0, origin.y + angle_sin * origin.x - angle_cos * origin.y,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0,
        );

        Transform { mat4, mat4_inverse }
    }

    /// Create a new [`Transform`] from a rotation around a specified [`Point3`] along an arbitrary axis.
    ///
    /// The angle is given in radians.
    pub fn rotate(origin: Point3, axis: Vec3, angle: f64) -> Self {
        let n = axis.normalize();

        let n_xx = n.x.powi(2);
        let n_xy = n.x * n.y;
        let n_xz = n.x * n.z;
        let n_yy = n.y.powi(2);
        let n_yz = n.y * n.z;
        let n_zz = n.z.powi(2);

        let sin = angle.sin();
        let cos = angle.cos();

        let a00 = n_xx + (1.0 - n_xx) * cos;
        let a01 = n_xy * (1.0 - cos) - n.z * sin;
        let a02 = n_xz * (1.0 - cos) + n.y * sin;
        let a10 = n_xy * (1.0 - cos) + n.z * sin;
        let a11 = n_yy + (1.0 - n_yy) * cos;
        let a12 = n_yz * (1.0 - cos) - n.x * sin;
        let a20 = n_xz * (1.0 - cos) - n.y * sin;
        let a21 = n_yz * (1.0 - cos) + n.x * sin;
        let a22 = n_zz + (1.0 - n_zz) * cos;

        #[rustfmt::skip]
            let mat4 = Mat4::new(
            a00, a01, a02, origin.x - a00 * origin.x - a01 * origin.y - a02 * origin.z,
            a10, a11, a12, origin.y - a10 * origin.x - a11 * origin.y - a12 * origin.z,
            a20, a21, a22, origin.z - a20 * origin.x - a21 * origin.y - a22 * origin.z,
            0.0, 0.0, 0.0, 1.0,
        );

        #[rustfmt::skip]
            let mat4_inverse = Mat4::new(
            a00, a10, a20, origin.x - a00 * origin.x - a10 * origin.y - a20 * origin.z,
            a01, a11, a21, origin.y - a01 * origin.x - a11 * origin.y - a21 * origin.z,
            a02, a12, a22, origin.z - a02 * origin.x - a12 * origin.y - a22 * origin.z,
            0.0, 0.0, 0.0, 1.0,
        );

        Transform { mat4, mat4_inverse }
    }

    /// Create a new [`Transform`] from a scale relative to a specified [`Point3`].
    pub fn scale(origin: Point3, scale: Vec3) -> Self {
        let scale_inverse = Vec3::new(1.0 / scale.x, 1.0 / scale.y, 1.0 / scale.z);

        #[rustfmt::skip]
            let mat4 = Mat4::new(
            scale.x, 0.0, 0.0, (1.0 - scale.x) * origin.x,
            0.0, scale.y, 0.0, (1.0 - scale.y) * origin.y,
            0.0, 0.0, scale.z, (1.0 - scale.z) * origin.z,
            0.0, 0.0, 0.0, 1.0,
        );
        #[rustfmt::skip]
            let mat4_inverse = Mat4::new(
            scale_inverse.x, 0.0, 0.0, (1.0 - scale_inverse.x) * origin.x,
            0.0, scale_inverse.y, 0.0, (1.0 - scale_inverse.y) * origin.y,
            0.0, 0.0, scale_inverse.z, (1.0 - scale_inverse.z) * origin.z,
            0.0, 0.0, 0.0, 1.0,
        );

        Transform { mat4, mat4_inverse }
    }

    /// Create a new [`Transform`] looking at a specified [`Point3`].
    pub fn look_at(origin: Point3, target: Point3, view_up: Vec3) -> Self {
        let back = (origin - target).normalize();
        let right = Vec3::cross(view_up, back).normalize();
        let up = Vec3::cross(back, right);

        #[rustfmt::skip]
            let mat4 = Mat4::new(
            right.x, up.x, back.x, origin.x,
            right.y, up.y, back.y, origin.y,
            right.z, up.z, back.z, origin.z,
            0.0, 0.0, 0.0, 1.0,
        );

        Transform {
            mat4,
            mat4_inverse: mat4.inverse(),
        }
    }

    /// Create a new perspective [`Transform`] for a Camera.
    pub fn perspective(fov: f64, near: f64, far: f64) -> Self {
        #[rustfmt::skip]
            let persp = Mat4::new(
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, far / (far - near), -far * near / (far - near),
            0.0, 0.0, 1.0, 0.0,
        );

        let inv_tan_ang = 1.0 / (fov * 0.5).tan();

        Transform::scale(Point3::ZERO, Vec3::new(inv_tan_ang, inv_tan_ang, 1.0))
            * Transform::new(persp, persp.inverse())
    }

    /// Transform a given [`Ray`].
    pub fn transform_ray(&self, ray: Ray) -> Ray {
        Ray::new(
            self.mat4 * ray.origin,
            self.mat4 * ray.direction,
            ray.last_ior,
        )
    }

    /// Transform a given [`Ray`] inversely.
    pub fn inverse_transform_ray(&self, ray: Ray) -> Ray {
        Ray::new(
            self.mat4_inverse * ray.origin,
            self.mat4_inverse * ray.direction,
            ray.last_ior,
        )
    }

    /// Transform a given [`Bounds3`].
    ///
    /// This first transforms `min` and `max` and then creates a new [`Bounds3`]
    /// including all 6 of the transformed corners.
    pub fn transform_bounds(&self, bounds: Bounds3) -> Bounds3 {
        let mut transformed_bounds = Bounds3::default();

        for point in bounds.corners() {
            transformed_bounds = transformed_bounds.include_point(self.mat4 * point);
        }

        transformed_bounds
    }

    /// Transform a given normal vector.
    ///
    /// This multiplies the normal by the transpose inverse matrix and then normalizes the result.
    ///
    /// See [PBRT](https://pbr-book.org/3ed-2018/Geometry_and_Transformations/Applying_Transformations#Normals)
    /// for more information.
    pub fn transform_normal(&self, normal: Vec3) -> Vec3 {
        (self.mat4_inverse.transpose() * normal).normalize()
    }

    /// Transform a given tangent.
    ///
    /// This transforms the X, Y and Z components and copies the W component.
    pub fn transform_tangent(&self, tangent: Vec4) -> Vec4 {
        let tangent_xyz = (self.mat4 * tangent.xyz()).normalize();
        Vec4::new(tangent_xyz.x, tangent_xyz.y, tangent_xyz.z, tangent.w)
    }
}

impl Transformable for Transform {
    fn translate(mut self, translation: Vec3) -> Self {
        let transform = Transform::translate(translation);
        self = Transform::new(
            transform.mat4 * self.mat4,
            self.mat4_inverse * transform.mat4_inverse,
        );

        self
    }

    fn rotate(mut self, origin: Point3, axis: Vec3, angle: f64) -> Self {
        let transform = Transform::rotate(origin, axis, angle);
        self = Transform::new(
            transform.mat4 * self.mat4,
            self.mat4_inverse * transform.mat4_inverse,
        );

        self
    }

    fn rotate_x(mut self, angle: f64) -> Self {
        let mat4 = self.mat4;
        let origin = Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);
        let transform = Transform::rotate_x(origin, angle);
        self = Transform::new(
            transform.mat4 * self.mat4,
            self.mat4_inverse * transform.mat4_inverse,
        );

        self
    }

    fn rotate_y(mut self, angle: f64) -> Self {
        let mat4 = self.mat4;
        let origin = Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);
        let transform = Transform::rotate_y(origin, angle);
        self = Transform::new(
            transform.mat4 * self.mat4,
            self.mat4_inverse * transform.mat4_inverse,
        );

        self
    }

    fn rotate_z(mut self, angle: f64) -> Self {
        let mat4 = self.mat4;
        let origin = Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);
        let transform = Transform::rotate_z(origin, angle);
        self = Transform::new(
            transform.mat4 * self.mat4,
            self.mat4_inverse * transform.mat4_inverse,
        );

        self
    }

    fn scale_x(self, factor: f64) -> Self {
        let mat4 = self.mat4;
        let origin = Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

        self.scale(origin, Vec3::new(factor, 1.0, 1.0))
    }

    fn scale_y(self, factor: f64) -> Self {
        let mat4 = self.mat4;
        let origin = Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

        self.scale(origin, Vec3::new(1.0, factor, 1.0))
    }

    fn scale_z(self, factor: f64) -> Self {
        let mat4 = self.mat4;
        let origin = Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

        self.scale(origin, Vec3::new(1.0, 1.0, factor))
    }

    fn scale_xyz(self, scale: Vec3) -> Self {
        let mat4 = self.mat4;
        let origin = Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

        self.scale(origin, scale)
    }

    fn scale(mut self, origin: Point3, scale: Vec3) -> Self {
        let transform = Transform::scale(origin, scale);
        self = Transform::new(
            transform.mat4 * self.mat4,
            self.mat4_inverse * transform.mat4_inverse,
        );

        self
    }

    fn look_at(mut self, target: Point3, view_up: Vec3) -> Self {
        let mat4 = self.mat4;
        let origin = Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);
        self = Transform::look_at(origin, target, view_up);

        self
    }
}

impl Mul<Transform> for Transform {
    type Output = Transform;

    fn mul(self, rhs: Transform) -> Self::Output {
        Transform {
            mat4: self.mat4 * rhs.mat4,
            mat4_inverse: rhs.mat4_inverse * self.mat4_inverse,
        }
    }
}

// TODO: Add transform method which takes a Transform
/// An interface to easily apply transformations.
pub trait Transformable: Send + Sync {
    /// Translate `self` by the given `translation`.
    ///
    /// This does ***not*** set the position but shifts it.
    fn translate(self, translation: Vec3) -> Self
    where
        Self: Sized;

    /// Rotate `self` around the specified `origin` along an arbitrary axis.
    ///
    /// `angle` is assumed to be in radians.
    fn rotate(self, origin: Point3, axis: Vec3, angle: f64) -> Self
    where
        Self: Sized;

    /// Rotate `self` around its center along the +X axis.
    ///
    /// `angle` is assumed to be in radians.
    fn rotate_x(self, angle: f64) -> Self
    where
        Self: Sized;

    /// Rotate `self` around its center along the +Y axis.
    ///
    /// `angle` is assumed to be in radians.
    fn rotate_y(self, angle: f64) -> Self
    where
        Self: Sized;

    /// Rotate `self` around its center along the +Z axis.
    ///
    /// `angle` is assumed to be in radians.
    fn rotate_z(self, angle: f64) -> Self
    where
        Self: Sized;

    /// Scale `self` relative to its center along the +X axis.
    fn scale_x(self, factor: f64) -> Self
    where
        Self: Sized;

    /// Scale `self` relative to its center along the +Y axis.
    fn scale_y(self, factor: f64) -> Self
    where
        Self: Sized;

    /// Scale `self` relative to its center along the +Z axis.
    fn scale_z(self, factor: f64) -> Self
    where
        Self: Sized;

    /// Scale `self` relative to its center along all 3 dimensions.
    fn scale_xyz(self, scale: Vec3) -> Self
    where
        Self: Sized;

    /// Scale `self` relative to the specified `origin` along all 3 dimensions.
    fn scale(self, origin: Point3, scale: Vec3) -> Self
    where
        Self: Sized;

    /// Make `self` look at specified `target`.
    fn look_at(self, target: Point3, view_up: Vec3) -> Self
    where
        Self: Sized;
}

#[cfg(test)]
mod tests {
    use std::f64::consts::FRAC_PI_4;

    use approx::assert_relative_eq;

    use crate::{Bounds3, Point3, Transform, Vec3};

    #[test]
    fn transform_isometry() {
        let vec3 = Vec3::new(4.2, 1.7, -2.22);

        let origin = Point3::new(7.32, 10.5, -2.0);
        let phi = 1.215_f64;

        assert_relative_eq!(
            (Transform::rotate_x(origin, phi).mat4 * vec3).magnitude(),
            vec3.magnitude()
        );
        assert_relative_eq!(
            (Transform::rotate_y(origin, phi).mat4 * vec3).magnitude(),
            vec3.magnitude()
        );
        assert_relative_eq!(
            (Transform::rotate_z(origin, phi).mat4 * vec3).magnitude(),
            vec3.magnitude()
        );
    }

    #[test]
    fn transform_similarity() {
        let vec3 = Vec3::new(2.0, -1.0, 5.0);

        assert_relative_eq!(
            Transform::scale(Point3::ZERO, Vec3::ZERO).mat4 * vec3,
            Vec3::ZERO
        );
        assert_relative_eq!(
            Transform::scale(Point3::ZERO, Vec3::splat(1.0)).mat4 * vec3,
            vec3
        );
        assert_relative_eq!(
            Transform::scale(Point3::ZERO, Vec3::splat(-0.5)).mat4 * vec3,
            vec3 * -0.5
        );

        let point3 = vec3.to_point3();

        assert_relative_eq!(
            Transform::scale(Point3::ZERO, Vec3::ZERO).mat4 * point3,
            Point3::ZERO
        );
        assert_relative_eq!(
            Transform::scale(Point3::ZERO, Vec3::splat(1.0)).mat4 * point3,
            point3
        );
        assert_relative_eq!(
            Transform::scale(Point3::ZERO, Vec3::splat(-0.5)).mat4 * point3,
            point3 * -0.5
        );
    }

    #[test]
    fn transform_orthogonality() {
        let rotation = Transform::rotate(Point3::ZERO, Vec3::new(2.8, 0.0, -4.2), 1.0);
        let rotation_x = Transform::rotate_x(Point3::ZERO, 7.4);
        let rotation_y = Transform::rotate_y(Point3::ZERO, -2.1);
        let rotation_z = Transform::rotate_z(Point3::ZERO, 8.2);

        assert_eq!(rotation.mat4.transpose(), rotation.mat4_inverse);
        assert_eq!(rotation_x.mat4.transpose(), rotation_x.mat4_inverse);
        assert_eq!(rotation_y.mat4.transpose(), rotation_y.mat4_inverse);
        assert_eq!(rotation_z.mat4.transpose(), rotation_z.mat4_inverse);
    }

    #[test]
    fn transform_inverse() {
        let origin = Point3::new(6.3, -3.04, 1.0);

        let translation = Transform::translate(Vec3::new(23.8, 12.0, -3.2));
        let rotation = Transform::rotate(origin, Vec3::new(2.8, 0.0, -4.2), 1.0);
        let rotation_x = Transform::rotate_x(origin, 7.4);
        let rotation_y = Transform::rotate_y(origin, -2.1);
        let rotation_z = Transform::rotate_z(origin, 8.2);
        let scale = Transform::scale(origin, Vec3::new(9.81, -2.762, 1.0));

        assert_relative_eq!(translation.mat4_inverse, translation.mat4.inverse());
        assert_relative_eq!(
            rotation.mat4_inverse,
            rotation.mat4.inverse(),
            max_relative = 1e-14
        );
        assert_relative_eq!(rotation_x.mat4_inverse, rotation_x.mat4.inverse());
        assert_relative_eq!(
            rotation_y.mat4_inverse,
            rotation_y.mat4.inverse(),
            max_relative = 1e-15
        );
        assert_relative_eq!(
            rotation_z.mat4_inverse,
            rotation_z.mat4.inverse(),
            max_relative = 1e-15
        );
        assert_relative_eq!(scale.mat4_inverse, scale.mat4.inverse());
    }

    #[test]
    fn transform_bounds() {
        let transform = Transform::rotate_z(Point3::ZERO, FRAC_PI_4)
            * Transform::rotate_x(Point3::ZERO, FRAC_PI_4);

        let bounds = Bounds3::new(Point3::new(-2.0, 1.0, 3.0), Point3::new(-5.0, 2.0, -1.0));
        let transformed_bounds = transform.transform_bounds(bounds);

        assert_relative_eq!(
            transformed_bounds.min,
            Point3::new(-5.035533905932738, -4.535533905932738, 0.0)
        );
        assert_relative_eq!(
            transformed_bounds.max,
            Point3::new(-0.4142135623730956, 0.0857864376269053, 3.5355339059327378)
        );
    }
}
