#[cfg(doc)]
use crate::Ray;
use crate::{Point2, Point3, Vec3, Vec4};

/// The intersection of a [`Ray`] with an Object.
#[derive(Copy, Clone, Debug)]
pub struct Hit {
    /// The hit's position in world space.
    pub position: Point3,
    /// The hit's normal in world space.
    pub normal: Vec3,
    /// The hit's tangent in world space, if any.
    pub tangent: Option<Vec4>,
    /// The distance between the [`Ray`]'s origin and the hit's position.
    pub distance: f64,
    /// The hit's UV coordinates on the Object.
    pub uv: Point2,
}

impl Hit {
    /// Create a new [`Hit`].
    pub fn new(
        position: Point3,
        normal: Vec3,
        tangent: Option<Vec4>,
        distance: f64,
        uv: Point2,
    ) -> Self {
        Hit {
            position,
            normal,
            tangent,
            distance,
            uv,
        }
    }
}
