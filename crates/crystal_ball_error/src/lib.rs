#![cfg_attr(doc, feature(doc_auto_cfg))]

//! Error handling for Crystal Ball.

use std::{error, fmt, io};

#[cfg(feature = "gltf")]
use gltf::image::Format;
use image::ImageError;

// TODO: Use some crate to do this
/// The various types of errors which can occur when using Crystal Ball.
#[derive(Debug)]
pub enum Error {
    Image(ImageError),
    Io(io::Error),
    #[cfg(feature = "gltf")]
    Gltf(gltf::Error),
    #[cfg(feature = "oidn")]
    Oidn(oidn::Error),
    #[cfg(feature = "gltf")]
    UnsupportedColorFormat(Format),
    Custom(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Image(e) => e.fmt(f),
            Error::Io(e) => e.fmt(f),
            #[cfg(feature = "gltf")]
            Error::Gltf(e) => e.fmt(f),
            #[cfg(feature = "oidn")]
            Error::Oidn(e) => write!(f, "{:?}", e),
            #[cfg(feature = "gltf")]
            Error::UnsupportedColorFormat(format) => {
                write!(f, "unsupported color format '{:?}'", format)
            }
            Error::Custom(e) => write!(f, "{}", e),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Error::Image(e) => Some(e),
            Error::Io(e) => Some(e),
            #[cfg(feature = "gltf")]
            Error::Gltf(e) => Some(e),
            #[cfg(feature = "oidn")]
            Error::Oidn(_) => None,
            #[cfg(feature = "gltf")]
            Error::UnsupportedColorFormat(_) => None,
            Error::Custom(_) => None,
        }
    }
}

impl From<ImageError> for Error {
    fn from(e: ImageError) -> Self {
        Self::Image(e)
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Self::Io(e)
    }
}

#[cfg(feature = "gltf")]
impl From<gltf::Error> for Error {
    fn from(e: gltf::Error) -> Self {
        Self::Gltf(e)
    }
}

#[cfg(feature = "oidn")]
impl From<oidn::Error> for Error {
    fn from(e: oidn::Error) -> Self {
        Self::Oidn(e)
    }
}

#[cfg(feature = "gltf")]
impl From<Format> for Error {
    fn from(f: Format) -> Self {
        Self::UnsupportedColorFormat(f)
    }
}

impl From<String> for Error {
    fn from(e: String) -> Self {
        Self::Custom(e)
    }
}
