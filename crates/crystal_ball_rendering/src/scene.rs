use std::sync::Arc;

use crystal_ball_color::{Color, Texture};
use crystal_ball_math::Transform;
use crystal_ball_shapes::{Object, BVH};

use crate::Camera;

/// The scene, consisting of a camera, objects and a background.
pub struct Scene {
    /// The scene's camera.
    ///
    /// Defaults to [`Camera::default`].
    pub camera: Camera,
    /// The scene's objects stored in a [`BVH`] to massively increase performance.
    ///
    /// Defaults to an empty [`BVH`].
    pub bvh: BVH<Object>,
    /// The scene's background color.
    ///
    /// Defaults to `(0.8, 0.8, 0.8)`.
    pub background_color: Color,
    /// The scene's base color texture.
    ///
    /// If this is None, `background_color` remains unchanged.
    /// Otherwise, each pixel value is multiplied with `background_color`.
    ///
    /// Defaults to [`None`].
    pub background_texture: Option<Arc<dyn Texture>>,
    /// The material's emissive strength.
    ///
    /// The resulting color of `background_color` and `background_texture`
    /// is multiplied with `background_strength`.
    ///
    /// Defaults to `1.0`.
    pub background_strength: f64,
    /// The material's background transform.
    ///
    /// While translation and scale also have impacts,
    /// it is only really useful to use this for rotating background images.
    ///
    /// Defaults to [`Transform::default`].
    pub background_transform: Transform,
}

impl Default for Scene {
    fn default() -> Self {
        Scene {
            camera: Camera::default(),
            bvh: BVH::init(4, vec![]),
            background_color: Color::splat(0.8),
            background_texture: None,
            background_strength: 1.0,
            background_transform: Transform::default(),
        }
    }
}
