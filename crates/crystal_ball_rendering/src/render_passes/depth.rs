use crate::render_passes::RenderPass;
#[cfg(doc)]
use crate::RenderPassData;
use crate::Scene;
use crystal_ball_color::{Color, Image, Interpolation};
use crystal_ball_derive::RenderPassData;
use crystal_ball_math::Ray;
use nanorand::tls::TlsWyRand;

/// A render pass storing depth values.
///
/// Depth values will be mapped to the range \[start, end\].
/// Values exceeding this range will be clamped.
///
/// The R, G and B values correspond to the mapped distance.
/// Therefore, depth images will always be black-white.
#[derive(RenderPassData)]
pub struct DepthPass {
    /// The sample count per pixel.
    ///
    /// Defaults to `64`.
    #[samples]
    pub samples: usize,
    /// The distance at which the depth fading starts.
    ///
    /// The values at this distance will be `1`:
    ///
    /// Defaults to `5.0`.
    pub start: f64,
    /// The distance at which the depth fading ends.
    ///
    /// The values at this distance will be `0`:
    ///
    /// Defaults to `25.0`.
    pub end: f64,
    /// The image to which the pass renders.
    ///
    /// It will be overwritten in the render process.
    /// For more information see [`RenderPassData::image_mut`].
    #[image]
    pub image: Image,
}

impl Default for DepthPass {
    fn default() -> Self {
        Self {
            samples: 64,
            start: 5.0,
            end: 25.0,
            image: Image::new(0, 0, Interpolation::Closest),
        }
    }
}

impl DepthPass {
    fn render(&self, scene: &Scene, ray: Ray, _bounces: usize, _rng: &mut TlsWyRand) -> Color {
        let closest_hit = scene.bvh.intersects(ray);

        match closest_hit {
            None => Color::default(),
            Some((hit, _object)) => {
                let distance = hit.distance.clamp(self.start, self.end);
                let value = 1.0 - (distance - self.start) / (self.end - self.start);

                Color::splat(value as f32)
            }
        }
    }
}

impl RenderPass for DepthPass {
    fn trace_ray(&self, scene: &Scene, ray: Ray, rng: &mut TlsWyRand) -> Color {
        self.render(scene, ray, 0, rng)
    }
}

#[cfg(test)]
mod tests {
    use crate::{DepthPass, RenderEngine, RenderPasses, Scene};
    use approx::assert_relative_eq;
    use crystal_ball_color::Color;
    use crystal_ball_materials::PbrMaterial;
    use crystal_ball_math::{Point3, Ray, Vec3};
    use crystal_ball_shapes::{Mesh, Object, TriangleMesh, BVH};

    #[test]
    fn depth_pass() {
        let plane = TriangleMesh::plane(1e3);
        let objects = vec![Object::new(Mesh::from(plane), PbrMaterial::default())];
        let bvh = BVH::init(4, objects);
        let scene = Scene {
            bvh,
            ..Default::default()
        };
        let mut rng = nanorand::tls_rng();

        let height = 1.0;
        let origin = Point3::new(0.0, height, 0.0);

        let angle_1 = -60f64.to_radians();
        let ray_1 = Ray::new(origin, Vec3::new(angle_1.cos(), angle_1.sin(), 0.0), 1.0);

        let angle_2 = -45f64.to_radians();
        let ray_2 = Ray::new(origin, Vec3::new(angle_2.cos(), angle_2.sin(), 0.0), 1.0);

        let angle_3 = -30f64.to_radians();
        let ray_3 = Ray::new(origin, Vec3::new(angle_3.cos(), angle_3.sin(), 0.0), 1.0);

        let depth_pass = DepthPass {
            start: (height / ray_1.direction.y).abs(),
            end: (height / ray_3.direction.y).abs(),
            ..Default::default()
        };

        let color_1 = depth_pass.render(&scene, ray_1, 0, &mut rng);
        let color_2 = depth_pass.render(&scene, ray_2, 0, &mut rng);
        let color_3 = depth_pass.render(&scene, ray_3, 0, &mut rng);

        assert_relative_eq!(color_1, Color::splat(1.0));
        assert_relative_eq!(color_2, Color::splat(0.6929927963088229));
        assert_relative_eq!(color_3, Color::splat(0.0));
    }

    #[test]
    fn depth_pass_clamping() {
        let plane = TriangleMesh::plane(1e3);
        let objects = vec![Object::new(Mesh::from(plane), PbrMaterial::default())];
        let bvh = BVH::init(4, objects);
        let scene = Scene {
            bvh,
            ..Default::default()
        };
        let mut rng = nanorand::tls_rng();

        let height = 1.0;
        let origin = Point3::new(0.0, height, 0.0);

        let angle_1 = -65f64.to_radians();
        let ray_1 = Ray::new(origin, Vec3::new(angle_1.cos(), angle_1.sin(), 0.0), 1.0);

        let angle_2 = -25f64.to_radians();
        let ray_2 = Ray::new(origin, Vec3::new(angle_2.cos(), angle_2.sin(), 0.0), 1.0);

        let depth_pass = DepthPass {
            start: (height / (-60f64.to_radians()).sin()).abs(),
            end: (height / (-30f64.to_radians()).sin()).abs(),
            ..Default::default()
        };

        let color_1 = depth_pass.render(&scene, ray_1, 0, &mut rng);
        let color_2 = depth_pass.render(&scene, ray_2, 0, &mut rng);

        assert_relative_eq!(color_1, Color::splat(1.0));
        assert_relative_eq!(color_2, Color::splat(0.0));
    }

    #[test]
    fn depth_pass_background() {
        let scene = Scene::default();

        let render_engine = RenderEngine {
            width: 1,
            height: 1,
            ..Default::default()
        };
        let mut render_passes = RenderPasses::new(DepthPass {
            samples: 1,
            ..Default::default()
        });
        render_engine.render(&scene, &mut render_passes);

        let depth_image = render_passes.get_image::<DepthPass>().unwrap();
        let color = depth_image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color, Color::default());
    }
}
