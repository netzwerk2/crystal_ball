use crate::render_passes::RenderPass;
#[cfg(doc)]
use crate::RenderPassData;
use crate::Scene;
use crystal_ball_color::{Color, Image, Interpolation};
use crystal_ball_derive::RenderPassData;
use crystal_ball_math::Ray;
use nanorand::tls::TlsWyRand;

/// A normal render pass intended to be used for denoising with [OpenImageDenoise](https://www.openimagedenoise.org/).
///
/// The normal of the first hit will always be returned,
/// meaning transparent surfaces will not be treated differently.
/// For more details see the `Normals` section in [OpenImageDenoise's documentation](https://www.openimagedenoise.org/documentation.html).
#[derive(RenderPassData)]
pub struct OidnNormalPass {
    /// The sample count per pixel.
    ///
    /// Defaults to `64`.
    #[samples]
    pub samples: usize,
    /// Whether the normals should be rendered in view or world space.
    ///
    /// Defaults to [`NormalPassMode::View`].
    pub mode: NormalPassMode,
    /// The image to which the pass renders.
    ///
    /// It will be overwritten in the render process.
    /// For more information see [`RenderPassData::image_mut`].
    #[image]
    pub image: Image,
}

impl OidnNormalPass {
    pub fn view() -> Self {
        Self {
            samples: 64,
            mode: NormalPassMode::View,
            image: Image::new(0, 0, Interpolation::Closest),
        }
    }

    pub fn world() -> Self {
        Self {
            samples: 64,
            mode: NormalPassMode::World,
            image: Image::new(0, 0, Interpolation::Closest),
        }
    }
}

impl RenderPass for OidnNormalPass {
    fn trace_ray(&self, scene: &Scene, ray: Ray, _rng: &mut TlsWyRand) -> Color {
        let closest_hit = scene.bvh.intersects(ray);

        match closest_hit {
            None => Color::splat(0.0),
            Some((hit, _)) => {
                let normal = match self.mode {
                    NormalPassMode::World => hit.normal,
                    NormalPassMode::View => scene
                        .camera
                        .transform
                        .inverse()
                        .transform_normal(hit.normal),
                };

                Color::new(normal.x as f32, normal.y as f32, normal.z as f32)
            }
        }
    }
}

/// The method used for rendering normals in [`OidnNormalPass`].
#[derive(Default)]
pub enum NormalPassMode {
    /// Process the normals in view space.
    #[default]
    View,
    /// Process the normals in world space.
    World,
}

#[cfg(test)]
mod tests {
    use crate::{Camera, OidnNormalPass, RenderEngine, RenderPasses, Scene};
    use approx::assert_relative_eq;
    use crystal_ball_materials::PbrMaterial;
    use crystal_ball_math::{Transformable, Vec3};
    use crystal_ball_shapes::{Mesh, Object, TriangleMesh, BVH};
    use std::f64::consts::{FRAC_1_SQRT_2, FRAC_PI_2, FRAC_PI_4, PI};

    #[test]
    fn oidn_normal_pass_view() {
        let plane = TriangleMesh::plane(1.0)
            .rotate_x(FRAC_PI_2)
            .rotate_y(FRAC_PI_4);
        let objects = vec![Object::new(Mesh::from(plane), PbrMaterial::default())];
        let bvh = BVH::init(4, objects);
        let mut scene = Scene {
            camera: Camera {
                fov: 1f64.to_radians(),
                ..Default::default()
            }
            .translate(Vec3::new(0.0, 0.0, 1.0)),
            bvh,
            ..Default::default()
        };

        let mut render_passes = RenderPasses::new(OidnNormalPass {
            samples: 1,
            ..OidnNormalPass::view()
        });

        let render_engine = RenderEngine {
            width: 1,
            height: 1,
            ..Default::default()
        };
        render_engine.render(&scene, &mut render_passes);

        let normal_image = render_passes.get_image::<OidnNormalPass>().unwrap();
        let normal = Vec3::from(normal_image.get_pixel(0.0, 0.0));
        assert_relative_eq!(normal.magnitude(), 1.0, epsilon = f32::EPSILON as f64);
        assert_relative_eq!(
            normal,
            Vec3::new(FRAC_1_SQRT_2, 0.0, FRAC_1_SQRT_2),
            epsilon = f32::EPSILON as f64
        );

        scene.camera = scene
            .camera
            .translate(Vec3::new(0.0, 0.0, -2.0))
            .rotate_y(PI);

        render_engine.render(&scene, &mut render_passes);

        let normal_image = render_passes.get_image::<OidnNormalPass>().unwrap();
        let normal = Vec3::from(normal_image.get_pixel(0.0, 0.0));
        assert_relative_eq!(normal.magnitude(), 1.0, epsilon = f32::EPSILON as f64);
        assert_relative_eq!(
            normal,
            Vec3::new(FRAC_1_SQRT_2, 0.0, FRAC_1_SQRT_2),
            epsilon = f32::EPSILON as f64
        );
    }

    #[test]
    fn oidn_normal_pass_world() {
        let plane = TriangleMesh::plane(1.0)
            .rotate_x(FRAC_PI_2)
            .rotate_y(FRAC_PI_4);
        let objects = vec![Object::new(Mesh::from(plane), PbrMaterial::default())];
        let bvh = BVH::init(4, objects);
        let mut scene = Scene {
            camera: Camera {
                fov: 1f64.to_radians(),
                ..Default::default()
            }
            .translate(Vec3::new(0.0, 0.0, 1.0)),
            bvh,
            ..Default::default()
        };

        let mut render_passes = RenderPasses::new(OidnNormalPass {
            samples: 1,
            ..OidnNormalPass::world()
        });

        let render_engine = RenderEngine {
            width: 1,
            height: 1,
            ..Default::default()
        };
        render_engine.render(&scene, &mut render_passes);

        let normal_image = render_passes.get_image::<OidnNormalPass>().unwrap();
        let normal = Vec3::from(normal_image.get_pixel(0.0, 0.0));
        assert_relative_eq!(normal.magnitude(), 1.0, epsilon = f32::EPSILON as f64);
        assert_relative_eq!(
            normal,
            Vec3::new(FRAC_1_SQRT_2, 0.0, FRAC_1_SQRT_2),
            epsilon = f32::EPSILON as f64
        );

        scene.camera = scene
            .camera
            .translate(Vec3::new(0.0, 0.0, -2.0))
            .rotate_y(PI);

        render_engine.render(&scene, &mut render_passes);

        let normal_image = render_passes.get_image::<OidnNormalPass>().unwrap();
        let normal = Vec3::from(normal_image.get_pixel(0.0, 0.0));
        assert_relative_eq!(normal.magnitude(), 1.0, epsilon = f32::EPSILON as f64);
        assert_relative_eq!(
            normal,
            Vec3::new(-FRAC_1_SQRT_2, 0.0, -FRAC_1_SQRT_2),
            epsilon = f32::EPSILON as f64
        );
    }
}
