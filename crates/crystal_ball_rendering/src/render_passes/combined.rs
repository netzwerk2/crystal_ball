use crate::render_passes::RenderPass;
#[cfg(doc)]
use crate::RenderPassData;
use crate::Scene;
use crystal_ball_color::{Color, Image, Interpolation};
use crystal_ball_derive::RenderPassData;
use crystal_ball_materials::compute_color;
use crystal_ball_math::{Ray, Vec3};
use crystal_ball_shapes::Sphere;
use nanorand::tls::TlsWyRand;

/// The default path-traced render pass.
#[derive(RenderPassData)]
pub struct CombinedPass {
    /// The sample count per pixel.
    ///
    /// Defaults to `64`.
    #[samples]
    pub samples: usize,
    /// The maximum amount of bounces before a [`Ray`] is terminated.
    ///
    /// Defaults to `4`.
    pub max_bounces: usize,
    /// The image to which the pass renders.
    ///
    /// It will be overwritten in the render process.
    /// For more information see [`RenderPassData::image_mut`].
    #[image]
    pub image: Image,
}

impl Default for CombinedPass {
    fn default() -> Self {
        Self {
            samples: 64,
            max_bounces: 4,
            image: Image::new(0, 0, Interpolation::Closest),
        }
    }
}

impl CombinedPass {
    fn render(&self, scene: &Scene, ray: Ray, bounces: usize, rng: &mut TlsWyRand) -> Color {
        if bounces > self.max_bounces {
            return Color::default();
        }

        let closest_hit = scene.bvh.intersects(ray);

        match closest_hit {
            None => {
                // Since we are inside the sphere, left and right are flipped.
                let ray_direction = Vec3::new(-ray.direction.x, ray.direction.y, ray.direction.z);
                let uv = Sphere::new().uv_map(
                    (scene.background_transform.mat4 * ray_direction)
                        .normalize()
                        .to_point3(),
                );

                compute_color(
                    scene.background_color,
                    &scene.background_texture,
                    scene.background_strength,
                    uv,
                )
            }
            Some((hit, object)) => {
                let (diffuse_color, emissive_color) = object.material.get_color(hit.uv);

                match object.material.next_ray(ray, hit, rng) {
                    None => emissive_color,
                    Some(ray) => {
                        self.render(scene, ray, bounces + 1, rng) * diffuse_color + emissive_color
                    }
                }
            }
        }
    }
}

impl RenderPass for CombinedPass {
    fn trace_ray(&self, scene: &Scene, ray: Ray, rng: &mut TlsWyRand) -> Color {
        self.render(scene, ray, 0, rng)
    }
}
