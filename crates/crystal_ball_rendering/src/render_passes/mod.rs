use crate::Scene;
use crystal_ball_color::{Color, Image};
use crystal_ball_math::Ray;
use downcast_rs::{impl_downcast, Downcast};
use nanorand::tls::TlsWyRand;
use std::ops::{Deref, DerefMut};

mod combined;
mod depth;
mod oidn_albedo;
mod oidn_normal;
mod uv;

#[cfg(doc)]
use crate::RenderEngine;
pub use combined::*;
pub use depth::*;
pub use oidn_albedo::*;
pub use oidn_normal::*;
pub use uv::*;

/// Data which has nothing to do with rendering the color of a pixel
/// but is needed for a [`RenderPass`].
///
/// This is a separate trait to allow it to be derived using macros.
pub trait RenderPassData {
    /// Return the number of samples per pixel.
    ///
    /// A number of 4 samples means the color of a pixel is averaged over 4 rays,
    /// generated in that pixel.
    fn samples(&self) -> usize;

    /// Return a reference to the underlying image.
    ///
    /// The image will be overwritten when rendering.
    /// For more information see [`RenderPassData::image_mut`].
    fn image(&self) -> &Image;

    /// Return a mutable reference to the underlying image.
    ///
    /// This method is internally used by [`RenderEngine`] to render each image.
    /// Therefore, any data previously stored in the image (dimensions as well as pixel data)
    /// will be overwritten when rendering.
    /// If you want to change the render resolution,
    /// use the corresponding `width` and `height` fields of [`RenderEngine`].
    fn image_mut(&mut self) -> &mut Image;
}

/// A render pass which can be executed by [`RenderEngine::render`].
///
/// Render passes allow extracting additional information of a scene,
/// by rendering that information to an image.
/// This can e.g. be used to render the normals of a scene.
///
/// Each render pass is rendered independently of all other render passes.
/// This means, for the same pixel different rays will be generated,
/// which may be noticeable when using a small amount of samples.
pub trait RenderPass: RenderPassData + Send + Sync + Downcast {
    /// Calculate color of the render pass for a given camera ray.
    ///
    /// This method will be called for each sample specified in [`RenderPassData::samples`],
    /// therefore in the resulting image pixels will be anti-aliased
    /// (assuming a large enough amount of samples).
    fn trace_ray(&self, scene: &Scene, ray: Ray, rng: &mut TlsWyRand) -> Color;
}

impl_downcast!(RenderPass);

/// A struct for easily handling multiple render passes.
///
/// It allows easy adding, accessing and removing of render passes
/// and ensures there's always only one type of [`RenderPass`] stored.
/// For more information see [`RenderPasses::add`].
///
/// Defaults to just a [`CombinedPass`].
pub struct RenderPasses {
    /// The underlying render passes.
    passes: Vec<Box<dyn RenderPass>>,
}

impl Default for RenderPasses {
    fn default() -> Self {
        Self {
            passes: vec![Box::new(CombinedPass::default())],
        }
    }
}

impl RenderPasses {
    /// Create a new [`RenderPasses`] instance, which only holds the given [`RenderPass`].
    pub fn new(pass: impl RenderPass) -> Self {
        Self {
            passes: vec![Box::new(pass)],
        }
    }

    /// Create an empty [`RenderPasses`] instance.
    pub fn empty() -> Self {
        Self { passes: vec![] }
    }

    /// Check if the `self` contains a [`RenderPass`] of the given type.
    pub fn contains<T: RenderPass>(&self) -> bool {
        self.get::<T>().is_some()
    }

    /// Add a new [`RenderPass`].
    ///
    /// If a [`RenderPass`] with the same type already exists, it is overwritten.
    /// This ensures there's no ambiguity when calling [`RenderPasses::get`].
    pub fn add<T: RenderPass>(&mut self, render_pass: T) {
        match self.get_mut::<T>() {
            None => {
                self.passes.push(Box::new(render_pass));
            }
            Some(pass) => *pass = render_pass,
        }
    }

    /// Return a reference to the given [`RenderPass`] if it exists.
    pub fn get<T: RenderPass>(&self) -> Option<&T> {
        self.passes.iter().find_map(|x| x.downcast_ref::<T>())
    }

    /// Return a mutable reference to the given [`RenderPass`] if it exists.
    pub fn get_mut<T: RenderPass>(&mut self) -> Option<&mut T> {
        self.passes.iter_mut().find_map(|x| x.downcast_mut::<T>())
    }

    /// Return a reference to the underlying image of the given [`RenderPass`] if it exists.
    pub fn get_image<T: RenderPass>(&self) -> Option<&Image> {
        self.get::<T>().map(|r| r.image())
    }

    /// Return a mutable reference to the underlying image of the given [`RenderPass`] if it exists.
    pub fn get_image_mut<T: RenderPass>(&mut self) -> Option<&mut Image> {
        self.get_mut::<T>().map(|r| r.image_mut())
    }

    /// Remove a [`RenderPass`] and return it if it exists.
    pub fn remove<T: RenderPass>(&mut self) -> Option<T> {
        let index = self
            .passes
            .iter()
            .position(|x| x.downcast_ref::<T>().is_some())?;
        let render_pass = self.passes.remove(index);

        Some(
            *render_pass
                .downcast::<T>()
                .unwrap_or_else(|_| unreachable!()),
        )
    }

    /// Return an iterator of all stored [`RenderPass`]es in order of addition.
    pub fn iter(&self) -> impl Iterator<Item = &Box<dyn RenderPass>> {
        self.passes.iter()
    }
}

impl Deref for RenderPasses {
    type Target = Vec<Box<dyn RenderPass>>;

    fn deref(&self) -> &Self::Target {
        &self.passes
    }
}

impl DerefMut for RenderPasses {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.passes
    }
}

#[cfg(test)]
mod tests {
    use crate::{RenderPass, RenderPasses, Scene};
    use crystal_ball_color::{Color, Image, Interpolation};
    use crystal_ball_derive::RenderPassData;
    use crystal_ball_math::Ray;
    use nanorand::tls::TlsWyRand;

    #[derive(RenderPassData)]
    struct FooPass {
        #[samples]
        foo: usize,
        #[image]
        image: Image,
    }

    impl Default for FooPass {
        fn default() -> Self {
            Self {
                foo: 0,
                image: Image::new(0, 0, Interpolation::Closest),
            }
        }
    }

    impl PartialEq<Self> for FooPass {
        fn eq(&self, other: &Self) -> bool {
            self.foo == other.foo
        }
    }

    impl Eq for FooPass {}

    impl RenderPass for FooPass {
        fn trace_ray(&self, _scene: &Scene, _ray: Ray, _rng: &mut TlsWyRand) -> Color {
            Color::default()
        }
    }

    #[derive(RenderPassData)]
    struct BarPass {
        #[samples]
        bar: usize,
        #[image]
        image: Image,
    }

    impl Default for BarPass {
        fn default() -> Self {
            Self {
                bar: 0,
                image: Image::new(0, 0, Interpolation::Closest),
            }
        }
    }

    impl PartialEq<Self> for BarPass {
        fn eq(&self, other: &Self) -> bool {
            self.bar == other.bar
        }
    }

    impl Eq for BarPass {}

    impl RenderPass for BarPass {
        fn trace_ray(&self, _scene: &Scene, _ray: Ray, _rng: &mut TlsWyRand) -> Color {
            Color::default()
        }
    }

    #[test]
    fn render_passes_add() {
        let mut render_passes = RenderPasses::empty();
        render_passes.add(FooPass::default());
        assert_eq!(render_passes.len(), 1);

        render_passes.add(BarPass::default());
        assert_eq!(render_passes.len(), 2);

        render_passes.add(FooPass::default());
        assert_eq!(render_passes.len(), 2);
    }

    #[test]
    fn render_passes_get() {
        let mut render_passes = RenderPasses::new(FooPass::default());
        assert_eq!(render_passes.len(), 1);
        assert_eq!(render_passes.get::<FooPass>().unwrap().foo, 0);

        render_passes.add(BarPass {
            bar: 42,
            ..Default::default()
        });
        assert_eq!(render_passes.get::<BarPass>().unwrap().bar, 42);
        assert_eq!(render_passes.len(), 2);
        render_passes.add(FooPass {
            foo: 17,
            ..Default::default()
        });

        assert_eq!(render_passes.len(), 2);
        assert_eq!(render_passes.get::<FooPass>().unwrap().foo, 17);

        render_passes.get_mut::<BarPass>().unwrap().bar = 69;
        assert_eq!(render_passes.get::<BarPass>().unwrap().bar, 69);
    }

    #[test]
    fn render_passes_remove() {
        let mut render_passes = RenderPasses::new(FooPass {
            foo: 42,
            ..Default::default()
        });
        render_passes.add(BarPass::default());
        assert_eq!(render_passes.len(), 2);
        assert_eq!(render_passes.get::<FooPass>().unwrap().foo, 42);

        let foo_pass = render_passes.remove::<FooPass>().unwrap();
        assert!(render_passes.get::<FooPass>().is_none());
        assert_eq!(render_passes.len(), 1);
        assert_eq!(foo_pass.foo, 42);

        render_passes.remove::<BarPass>();
        assert!(render_passes.get::<BarPass>().is_none());
        assert_eq!(render_passes.len(), 0);
    }
}
