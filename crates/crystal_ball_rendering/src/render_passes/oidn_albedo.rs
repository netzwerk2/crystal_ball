use crate::render_passes::RenderPass;
#[cfg(doc)]
use crate::RenderPassData;
use crate::Scene;
use crystal_ball_color::{Color, Image, Interpolation};
use crystal_ball_derive::RenderPassData;
use crystal_ball_materials::compute_color;
use crystal_ball_math::Ray;
use crystal_ball_shapes::Sphere;
use nanorand::tls::TlsWyRand;

/// An albedo render pass intended to be used for denoising with [OpenImageDenoise](https://www.openimagedenoise.org/).
///
/// The albedo of the first hit will always be returned,
/// meaning transparent surfaces will not be treated differently.
/// For more details see the `Albedos` section in [OpenImageDenoise's documentation](https://www.openimagedenoise.org/documentation.html).
#[derive(RenderPassData)]
pub struct OidnAlbedoPass {
    /// The sample count per pixel.
    ///
    /// Defaults to `64`.
    #[samples]
    pub samples: usize,
    /// The image to which the pass renders.
    ///
    /// It will be overwritten in the render process.
    /// For more information see [`RenderPassData::image_mut`].
    #[image]
    pub image: Image,
}

impl Default for OidnAlbedoPass {
    fn default() -> Self {
        Self {
            samples: 64,
            image: Image::new(0, 0, Interpolation::Closest),
        }
    }
}

impl RenderPass for OidnAlbedoPass {
    fn trace_ray(&self, scene: &Scene, ray: Ray, _rng: &mut TlsWyRand) -> Color {
        let closest_hit = scene.bvh.intersects(ray);

        let color = match closest_hit {
            None => {
                let uv = Sphere::new().uv_map(
                    (scene.background_transform.mat4 * ray.direction)
                        .normalize()
                        .to_point3(),
                );

                compute_color(
                    scene.background_color,
                    &scene.background_texture,
                    scene.background_strength,
                    uv,
                )
            }
            Some((hit, object)) => {
                let (diffuse_color, emissive_color) = object.material.get_color(hit.uv);

                // In theory there should be a differentiation for transparent surfaces,
                // but this would require changes to the material system.
                diffuse_color + emissive_color
            }
        };

        Color::new(
            color.r.clamp(0.0, 1.0),
            color.g.clamp(0.0, 1.0),
            color.b.clamp(0.0, 1.0),
        )
    }
}

#[cfg(test)]
mod tests {
    use crate::{Camera, OidnAlbedoPass, RenderEngine, RenderPasses, Scene};
    use approx::assert_relative_eq;
    use crystal_ball_color::{Color, Image, Interpolation, IntoArcDynTexture};
    use crystal_ball_materials::PbrMaterial;
    use crystal_ball_math::{Transformable, Vec3};
    use crystal_ball_shapes::{Mesh, Object, TriangleMesh, BVH};
    use std::f64::consts::FRAC_PI_2;

    #[test]
    fn oidn_albedo_pass() {
        let plane = TriangleMesh::plane(1.0).rotate_x(FRAC_PI_2);
        let objects = vec![Object::new(
            Mesh::from(plane),
            PbrMaterial {
                base_color: Color::splat(0.8),
                base_color_texture: Some(
                    Image::from_pixels(
                        1,
                        1,
                        vec![Color::new(0.8, 0.42, 0.17)],
                        Interpolation::Closest,
                    )
                    .into_arc_dyn_texture(),
                ),
                emissive_color: Color::splat(0.5),
                emissive_texture: Some(
                    Image::from_pixels(
                        1,
                        1,
                        vec![Color::new(1.0, 0.2, 0.5)],
                        Interpolation::Closest,
                    )
                    .into_arc_dyn_texture(),
                ),
                ..Default::default()
            },
        )];
        let bvh = BVH::init(4, objects);
        let scene = Scene {
            camera: Camera {
                fov: 1f64.to_radians(),
                ..Default::default()
            }
            .translate(Vec3::new(0.0, 0.0, 1.0)),
            bvh,
            ..Default::default()
        };

        let mut render_passes = RenderPasses::new(OidnAlbedoPass {
            samples: 1,
            ..Default::default()
        });

        let render_engine = RenderEngine {
            width: 1,
            height: 1,
            ..Default::default()
        };
        render_engine.render(&scene, &mut render_passes);

        let albedo_image = render_passes.get_image::<OidnAlbedoPass>().unwrap();
        let color = albedo_image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color, Color::new(1.0, 0.436, 0.386));
    }

    #[test]
    fn albedo_pass_background() {
        let scene = Scene {
            camera: Camera {
                fov: 1f64.to_radians(),
                ..Default::default()
            }
            .translate(Vec3::new(0.0, 0.0, 1.0)),
            background_color: Color::splat(0.8),
            background_texture: Some(
                Image::from_pixels(
                    1,
                    1,
                    vec![Color::new(0.8, 0.42, 0.17)],
                    Interpolation::Closest,
                )
                .into_arc_dyn_texture(),
            ),
            background_strength: 2.0,
            ..Default::default()
        };

        let mut render_passes = RenderPasses::new(OidnAlbedoPass {
            samples: 1,
            ..Default::default()
        });

        let render_engine = RenderEngine {
            width: 1,
            height: 1,
            ..Default::default()
        };
        render_engine.render(&scene, &mut render_passes);

        let albedo_image = render_passes.get_image::<OidnAlbedoPass>().unwrap();
        let color = albedo_image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color, Color::new(1.0, 0.672, 0.272));
    }
}
