use crate::render_passes::RenderPass;
#[cfg(doc)]
use crate::RenderPassData;
use crate::Scene;
use crystal_ball_color::{Color, Image, Interpolation};
use crystal_ball_derive::RenderPassData;
use crystal_ball_math::Ray;
use nanorand::tls::TlsWyRand;

/// A render pass storing UV values.
///
/// The R and G values correspond to the U and V values, while B will always be `0`.
#[derive(RenderPassData)]
pub struct UVPass {
    /// The sample count per pixel.
    ///
    /// Defaults to `64`.
    #[samples]
    pub samples: usize,
    /// The image to which the pass renders.
    ///
    /// It will be overwritten in the render process.
    /// For more information see [`RenderPassData::image_mut`].
    #[image]
    pub image: Image,
}

impl Default for UVPass {
    fn default() -> Self {
        Self {
            samples: 64,
            image: Image::new(0, 0, Interpolation::Closest),
        }
    }
}

impl UVPass {
    fn render(&self, scene: &Scene, ray: Ray, _bounces: usize, _rng: &mut TlsWyRand) -> Color {
        let closest_hit = scene.bvh.intersects(ray);

        match closest_hit {
            None => Color::default(),
            Some((hit, _object)) => {
                let uv = hit.uv;

                Color::new(uv.x as f32, uv.y as f32, 0.0)
            }
        }
    }
}

impl RenderPass for UVPass {
    fn trace_ray(&self, scene: &Scene, ray: Ray, rng: &mut TlsWyRand) -> Color {
        self.render(scene, ray, 0, rng)
    }
}

#[cfg(test)]
mod tests {
    use crate::{Camera, RenderEngine, RenderPasses, Scene, UVPass};
    use approx::assert_relative_eq;
    use crystal_ball_color::Color;
    use crystal_ball_materials::PbrMaterial;
    use crystal_ball_math::{Point3, Transform, Transformable, Vec3};
    use crystal_ball_shapes::{Object, Sphere, BVH};
    use std::f64::consts::FRAC_PI_2;

    #[test]
    fn uv_pass_u() {
        let objects = vec![Object::new(Sphere::new(), PbrMaterial::default())];
        let bvh = BVH::init(4, objects);
        let mut scene = Scene {
            camera: Camera {
                fov: 0.0,
                ..Default::default()
            },
            bvh,
            ..Default::default()
        };

        let render_engine = RenderEngine {
            width: 1,
            height: 1,
            ..Default::default()
        };
        let mut render_passes = RenderPasses::new(UVPass {
            samples: 1,
            ..Default::default()
        });

        scene.camera.transform =
            Transform::translate(Vec3::new(2.0, 0.0, 0.0)).look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.r, 0.5);

        scene.camera.transform =
            Transform::translate(Vec3::new(0.0, 0.0, -2.0)).look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.r, 0.75);

        scene.camera.transform =
            Transform::translate(Vec3::new(-2.0, 0.0, -1e-6)).look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.r, 1.0);

        scene.camera.transform =
            Transform::translate(Vec3::new(-2.0, 0.0, 1e-6)).look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.r, 0.0);

        scene.camera.transform =
            Transform::translate(Vec3::new(0.0, 0.0, 2.0)).look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.r, 0.25);
    }

    #[test]
    fn uv_pass_v() {
        let objects = vec![Object::new(Sphere::new(), PbrMaterial::default())];
        let bvh = BVH::init(4, objects);
        let mut scene = Scene {
            camera: Camera {
                fov: 0.0,
                ..Default::default()
            },
            bvh,
            ..Default::default()
        };

        let render_engine = RenderEngine {
            width: 1,
            height: 1,
            ..Default::default()
        };
        let mut render_passes = RenderPasses::new(UVPass {
            samples: 1,
            ..Default::default()
        });

        scene.camera.transform =
            Transform::translate(Vec3::new(0.0, 2.0, 0.0)).look_at(Point3::ZERO, Vec3::X);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.g, 0.0);

        scene.camera.transform =
            Transform::translate(Vec3::new(2.0, 0.0, 0.0)).look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.g, 0.5);

        scene.camera.transform =
            Transform::translate(Vec3::new(0.0, -2.0, 0.0)).look_at(Point3::ZERO, Vec3::X);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.g, 1.0);
    }

    #[test]
    fn uv_pass_u_transformed() {
        let objects = vec![Object::new(
            Sphere::new().rotate_y(FRAC_PI_2),
            PbrMaterial::default(),
        )];
        let bvh = BVH::init(4, objects);
        let mut scene = Scene {
            camera: Camera {
                fov: 0.0,
                ..Default::default()
            },
            bvh,
            ..Default::default()
        };

        let render_engine = RenderEngine {
            width: 1,
            height: 1,
            ..Default::default()
        };
        let mut render_passes = RenderPasses::new(UVPass {
            samples: 1,
            ..Default::default()
        });

        scene.camera.transform = Transform::translate(Vec3::new(2.0, 0.0, 0.0))
            .rotate(Point3::ZERO, Vec3::Y, FRAC_PI_2)
            .look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color, Color::new(0.5, 0.5, 0.0));

        scene.camera.transform = Transform::translate(Vec3::new(0.0, 0.0, -2.0))
            .rotate(Point3::ZERO, Vec3::Y, FRAC_PI_2)
            .look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color, Color::new(0.75, 0.5, 0.0));

        scene.camera.transform = Transform::translate(Vec3::new(-2.0, 0.0, -1e-6))
            .rotate(Point3::ZERO, Vec3::Y, FRAC_PI_2)
            .look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color, Color::new(1.0, 0.5, 0.0));

        scene.camera.transform = Transform::translate(Vec3::new(-2.0, 0.0, 1e-6))
            .rotate(Point3::ZERO, Vec3::Y, FRAC_PI_2)
            .look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color, Color::new(0.0, 0.5, 0.0));

        scene.camera.transform = Transform::translate(Vec3::new(0.0, 0.0, 2.0))
            .rotate(Point3::ZERO, Vec3::Y, FRAC_PI_2)
            .look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color, Color::new(0.25, 0.5, 0.0));
    }

    #[test]
    fn uv_pass_v_transformed() {
        let objects = vec![Object::new(
            Sphere::new().rotate_x(FRAC_PI_2),
            PbrMaterial::default(),
        )];
        let bvh = BVH::init(4, objects);
        let mut scene = Scene {
            camera: Camera {
                fov: 0.0,
                ..Default::default()
            },
            bvh,
            ..Default::default()
        };

        let render_engine = RenderEngine {
            width: 1,
            height: 1,
            ..Default::default()
        };
        let mut render_passes = RenderPasses::new(UVPass {
            samples: 1,
            ..Default::default()
        });

        scene.camera.transform = Transform::translate(Vec3::new(0.0, 2.0, 0.0))
            .rotate(Point3::ZERO, Vec3::X, FRAC_PI_2)
            .look_at(Point3::ZERO, Vec3::X);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.g, 0.0);

        scene.camera.transform = Transform::translate(Vec3::new(2.0, 0.0, 0.0))
            .rotate(Point3::ZERO, Vec3::X, FRAC_PI_2)
            .look_at(Point3::ZERO, Vec3::Y);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.g, 0.5);

        scene.camera.transform = Transform::translate(Vec3::new(0.0, -2.0, 0.0))
            .rotate(Point3::ZERO, Vec3::X, FRAC_PI_2)
            .look_at(Point3::ZERO, Vec3::X);
        render_engine.render(&scene, &mut render_passes);
        let image = render_passes.get_image::<UVPass>().unwrap();
        let color = image.get_pixel(0.0, 0.0);
        assert_relative_eq!(color.g, 1.0);
    }
}
