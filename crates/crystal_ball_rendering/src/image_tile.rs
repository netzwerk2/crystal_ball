use crystal_ball_color::{Color, Image, Interpolation};

/// A tile of an [`Image`] storing its position and its chunk of the original image.
#[derive(Clone, Debug)]
pub(crate) struct ImageTile {
    pub x: u32,
    pub y: u32,
    pub image: Image,
}

impl ImageTile {
    /// Create a new [`ImageTile`].
    pub fn new(x: u32, y: u32, image: Image) -> Self {
        ImageTile { x, y, image }
    }
}

pub(crate) trait FromTiles {
    fn from_tiles(
        width: u32,
        height: u32,
        tiles: &[ImageTile],
        interpolation: Interpolation,
    ) -> Self;
}

impl FromTiles for Image {
    /// Create an image from a [`Vec`] of [`ImageTile`].
    fn from_tiles(
        width: u32,
        height: u32,
        tiles: &[ImageTile],
        interpolation: Interpolation,
    ) -> Self {
        let mut pixels = vec![Color::default(); (width * height) as usize];

        for tile in tiles {
            for y in 0..tile.image.height {
                let start = (tile.x + (tile.y + y) * width) as usize;
                let end = start + tile.image.width as usize;

                let tiles_start = (y * tile.image.width) as usize;
                let tiles_end = tiles_start + tile.image.width as usize;

                pixels[start..end].copy_from_slice(&tile.image.pixels[tiles_start..tiles_end])
            }
        }

        Image {
            width,
            height,
            pixels,
            interpolation,
        }
    }
}
