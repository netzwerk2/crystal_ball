use crate::camera::PrecalculatedCamera;
use crate::image_tile::{FromTiles, ImageTile};
#[cfg(doc)]
use crate::{Camera, RenderPasses};
use crate::{RenderPass, Scene};
use crystal_ball_color::{Color, Image, Interpolation};
use crystal_ball_math::random_float;
use indicatif::{ProgressBar, ProgressStyle};
use nanorand::tls_rng;
use rayon::prelude::*;
use std::sync::{Arc, Mutex};
use std::time::Instant;

/// The render engine.
///
/// Rendering is performed by splitting the image into tiles (small subsets of the image).
/// These tiles are then rendered in parallel to increase performance.
/// The best tile size varies depending on your scene,
/// therefore Crystal Ball uses default values,
/// that have proven to be a good compromise for many different scenes.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct RenderEngine {
    /// The width of the rendered image.
    ///
    /// Defaults to `1920`.
    pub width: u32,
    /// The height of the rendered image.
    ///
    /// Defaults to `1080`.
    pub height: u32,
    /// The width of a tile.
    ///
    /// If it's greater than the [`Camera`]'s width, the width is clamped.
    /// If the [`Camera`]'s width is not a multiple of the tile width,
    /// the last tile will be truncated.
    ///
    /// Defaults to `16`.
    pub tile_size_x: u32,
    /// The height of a tile.
    ///
    /// If it's greater than the [`Camera`]'s height, the height is clamped.
    /// If the [`Camera`]'s height is not a multiple of the tile height,
    /// the last tile will be truncated.
    ///
    /// Defaults to `16`.
    pub tile_size_y: u32,
}

impl Default for RenderEngine {
    fn default() -> Self {
        Self {
            width: 1920,
            height: 1080,
            tile_size_x: 16,
            tile_size_y: 16,
        }
    }
}

impl RenderEngine {
    fn image_tiles(&self, width: u32, height: u32) -> Vec<ImageTile> {
        let tile_size_x = self.tile_size_x.min(width);
        let tile_size_y = self.tile_size_y.min(height);

        let mut tiles = vec![];

        for x in 0..(width as f32 / tile_size_x as f32).ceil() as u32 {
            for y in 0..(height as f32 / tile_size_y as f32).ceil() as u32 {
                let tile_width = if (x + 1) * tile_size_x > width {
                    width - tile_size_x * x
                } else {
                    tile_size_x
                };
                let tile_height = if (y + 1) * tile_size_y >= height {
                    height - tile_size_y * y
                } else {
                    tile_size_y
                };

                tiles.push(ImageTile::new(
                    x * tile_size_x,
                    y * tile_size_y,
                    Image::new(tile_width, tile_height, Interpolation::Closest),
                ));
            }
        }

        tiles
    }

    /// Render the given scene to an image.
    ///
    /// The render passes are rendered one after another in the given order.
    /// It is recommended to use [`RenderPasses`] to manage your render passes.
    pub fn render(&self, scene: &Scene, render_passes: &mut [Box<dyn RenderPass>]) {
        let camera = PrecalculatedCamera::from_camera(scene.camera, self.width, self.height);

        let (width, height) = camera.dimensions();

        println!(
            "Rendering {} object(s) with a resolution of {}x{}.",
            scene.bvh.shapes.len(),
            width,
            height
        );

        let tiles = self.image_tiles(width, height);

        let progress_bar = ProgressBar::new((tiles.len() * render_passes.len()) as u64);
        progress_bar.set_style(
            ProgressStyle::with_template(
                "[{elapsed_precise}] [{bar:60}] {percent:>5}% (ETA: ~{eta_precise})\n{msg}",
            )
            .unwrap()
            .progress_chars("=> "),
        );
        progress_bar.set_position(0);
        let progress_bar = Arc::new(Mutex::new(progress_bar));

        let start_time = Instant::now();

        for render_pass in render_passes.iter_mut() {
            let mut tiles = tiles.clone();

            tiles.par_iter_mut().for_each(|tile| {
                let tile_x = tile.x;
                let tile_y = tile.y;
                let tile_width = tile.image.width;

                let samples = render_pass.samples();

                tile.image
                    .pixels
                    .iter_mut()
                    .enumerate()
                    .for_each(|(i, pixel)| {
                        let x = (i as u32 % tile_width) + tile_x;
                        let y = (i as u32 / tile_width) + tile_y;

                        let mut rng = tls_rng();
                        let mut color = Color::default();

                        for _ in 0..samples {
                            let ray = camera.get_ray(
                                x as f64 + random_float(&mut rng, 0.0, 1.0),
                                height as f64 - y as f64 + random_float(&mut rng, 0.0, 1.0),
                                &mut rng,
                            );

                            color += render_pass.trace_ray(scene, ray, &mut rng);
                        }

                        *pixel = color / samples as f32;
                    });

                progress_bar.lock().unwrap().inc(1);
            });

            *render_pass.image_mut() =
                Image::from_tiles(width, height, &tiles, Interpolation::Closest);
        }

        progress_bar.lock().unwrap().finish_with_message(format!(
            "Rendering finished after {:.2?}",
            Instant::now() - start_time
        ));
    }
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;

    use crystal_ball_color::{Color, Image, Interpolation};

    use crate::image_tile::FromTiles;
    use crate::RenderEngine;

    #[test]
    fn image_tiles_multiple() {
        let render_engine = RenderEngine {
            width: 1024,
            height: 1024,
            ..Default::default()
        };

        let tiles = render_engine.image_tiles(render_engine.width, render_engine.height);
        assert_eq!(
            tiles.len() as u32,
            render_engine.width / render_engine.tile_size_x * render_engine.height
                / render_engine.tile_size_y
        );
    }

    #[test]
    fn image_tiles_no_multiple() {
        let mut render_engine = RenderEngine {
            width: 1920,
            height: 1080,
            ..Default::default()
        };

        let tiles = render_engine.image_tiles(render_engine.width, render_engine.height);
        assert_eq!(
            tiles.len() as u32,
            render_engine.width / render_engine.tile_size_x
                * (render_engine.height / render_engine.tile_size_y)
                + render_engine.width / render_engine.tile_size_x
        );

        render_engine.width = 1080;
        render_engine.height = 1920;

        let tiles = render_engine.image_tiles(render_engine.width, render_engine.height);
        assert_eq!(
            tiles.len() as u32,
            render_engine.width / render_engine.tile_size_x
                * (render_engine.height / render_engine.tile_size_y)
                + render_engine.height / render_engine.tile_size_y
        );

        render_engine.width = 1080;
        render_engine.height = 1080;

        let tiles = render_engine.image_tiles(render_engine.width, render_engine.height);
        assert_eq!(
            tiles.len() as u32,
            render_engine.width / render_engine.tile_size_x
                * (render_engine.height / render_engine.tile_size_y)
                + render_engine.width / render_engine.tile_size_x
                + render_engine.height / render_engine.tile_size_y
                + 1
        );
    }

    // TODO: use RenderEngine::render (probably an emissive plane)
    #[test]
    fn image_tile_to_image() {
        let width = 12;
        let height = 10;

        let render_engine = RenderEngine {
            width,
            height,
            ..Default::default()
        };

        let mut tiles = render_engine.image_tiles(render_engine.width, render_engine.height);

        let colors = (0..width * height)
            .map(|i| {
                let x = i % width;
                let y = i / width;

                Color::new(
                    x as f32 / (width - 1) as f32,
                    y as f32 / (height - 1) as f32,
                    1.0,
                )
            })
            .collect::<Vec<Color>>();

        for tile in tiles.iter_mut() {
            tile.image
                .pixels
                .iter_mut()
                .enumerate()
                .for_each(|(i, pixel)| {
                    let x = (i as u32 % tile.image.width) + tile.x;
                    let y = (i as u32 / tile.image.width) + tile.y;

                    *pixel = colors[(x + y * width) as usize];
                });
        }

        let image = Image::from_tiles(width, height, &tiles, Interpolation::Closest);

        for (pixel, color) in image.pixels.iter().zip(&colors) {
            assert_relative_eq!(*pixel, *color);
        }
    }

    #[test]
    fn image_tiles_to_image() {
        let width = 30;
        let height = 10;

        let render_engine = RenderEngine {
            width,
            height,
            tile_size_x: 8,
            tile_size_y: 8,
        };

        let mut tiles = render_engine.image_tiles(render_engine.width, render_engine.height);

        let colors = (0..width * height)
            .map(|i| {
                let x = i % width;
                let y = i / width;

                Color::new(
                    x as f32 / (width - 1) as f32,
                    y as f32 / (height - 1) as f32,
                    1.0,
                )
            })
            .collect::<Vec<Color>>();

        for tile in tiles.iter_mut() {
            tile.image
                .pixels
                .iter_mut()
                .enumerate()
                .for_each(|(i, pixel)| {
                    let x = (i as u32 % tile.image.width) + tile.x;
                    let y = (i as u32 / tile.image.width) + tile.y;

                    *pixel = colors[(x + y * width) as usize];
                });
        }

        let image = Image::from_tiles(width, height, &tiles, Interpolation::Closest);

        for (pixel, color) in image.pixels.iter().zip(&colors) {
            assert_relative_eq!(*pixel, *color);
        }
    }
}
