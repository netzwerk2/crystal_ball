# Changelog

## Version 0.3
- API redesign: public fields instead of builder patterns
- Support for triangle meshes has been added
- Initial glTF support has been added
- Transforms have been added. With them comes the `Transformable` trait,
  to easily apply them and a respective derive macro
- A BVH is automatically constructed before rendering
- Background images can now be transformed (e.g. rotated)
- Images use a `Vec<Color>` instead of a `Vec<Vec<Color>>`
- Support for normal mapping has been added
- `Image::from_file` now automatically detects HDRIs
- `ImageTile` is only exported publicly in the crate
- The interpolation method for an image can now be chosen
- Colors use `f32` instead of `f64`
- A new module for materials was created
- `Diffuse`, `Metallic`, `Emissive` and `Refractive` have been removed in favor of the new `PbrMaterial`, 
  which combines all of them
- `Hit` does not store a reference to the `Material` anymore
- `Ray::get_point` has been renamed to `Ray::get`
- A bunch of math types have been added: `Bounds2`, `Bounds3`, `Mat4`, `Point2`, `Point3`, `Transform`, `Vec2`, `Vec4`.
  Some methods have been moved to these new types or were renamed.
  Some types were also replaced with their respective counterparts (e.g. `Vec2` with `Point2`)
- The camera code has been rewritten to use projection matrices and support transforms
- The render engine now stores the tile size
- `Color` does not implement `Texture` anymore
- `Sphere` only holds a `Transform`
- `Shape`s are now logically separated from `Material`s.
  A new `Object` struct has been introduced to combine them for rendering
- `Shape::uv_map` has been removed
- `Shape::bounds` has been added to build BVHs
- Update to Rust edition 2021
- Update dependencies

## Version 0.2

- Faster render times by using a [faster random number generation library](https://github.com/aspenluxxxy/nanorand-rs)
- Use tiled rendering instead of row-based
- Spheres can now also have textures
- Change license from GPLv3 to LGPLv3