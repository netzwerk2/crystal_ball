#![cfg_attr(doc, feature(doc_auto_cfg))]

//! *Crystal Ball* is a path tracing library written in Rust.
//!
//! It uses [rayon](https://github.com/rayon-rs/rayon) for parallelization
//! and can save the rendered image in various formats thanks to the [image](https://github.com/image-rs/image) crate.
//!
//! Note that Crystal Ball is a hobby project
//! and will most likely see a lot of API changes in future versions.
//!
//! ## Features
//!
//! - Multithreaded CPU rendering
//! - Save rendered images in [various formats](https://github.com/image-rs/image#supported-image-formats)
//! - Environment textures
//! - General purpose PBR material
//! - Shapes: spheres and triangle meshes
//! - Easily create your own textures, materials, and shapes
//! - Load [glTF](https://registry.khronos.org/glTF/specs/2.0/glTF-2.0.html) files
//! - Depth of field
//! - Bounding Volume Hierarchies
//! - Optional denoising using [Open Image Denoise](https://www.openimagedenoise.org/)
//!
//! ## Usage
//!
//! ### Basic Example
//!
//! A basic example rendering two spheres.
//!
//! ```no_run
//! use crystal_ball::prelude::*;
//!
//! fn main() -> Result<(), Error> {
//!     let objects = vec![
//!         Object::new(
//!             Sphere::new(),
//!             PbrMaterial {
//!                 base_color: Color::new(1.0, 0.45, 0.31),
//!                 ..Default::default()
//!             },
//!         ),
//!         Object::new(
//!             Sphere::new()
//!                 .translate(Vec3::new(0.0, -101.0, 0.0))
//!                 .scale_xyz(Vec3::splat(100.0)),
//!             PbrMaterial::default(),
//!         ),
//!     ];
//!     let bvh = BVH::init(4, objects);
//!
//!     let scene = Scene {
//!         bvh,
//!         camera: Camera::default().translate(Vec3::new(0.0, 0.0, 5.0)),
//!         ..Default::default()
//!     };
//!
//!     let engine = RenderEngine::default();
//!     let mut render_passes = RenderPasses::default();
//!     engine.render(&scene, &mut render_passes);
//!
//!     let image = render_passes.get_image::<CombinedPass>().unwrap();
//!     image.write("basic.png")?;
//!
//!     Ok(())
//! }
//! ```
//!
//! Take a look at the examples to see how to use Crystal Ball's different features.
//!
//! ### Coordinate System
//!
//! Crystal Ball uses a right-handed coordinate system where
//!
//! - +X points right
//! - +Y points up
//! - +Z points to the screen
//!
//! ## Feature Flags
//!
//! ### Default Features
//!
//! | Name   | description                                                                  |
//! |--------|------------------------------------------------------------------------------|
//! | `gltf` | Load [glTF](https://registry.khronos.org/glTF/specs/2.0/glTF-2.0.html) files |
//!
//! ### Optional Features
//!
//! | Name     | description                                                                   |
//! |----------|-------------------------------------------------------------------------------|
//! | `oidn`   | Image denoising using [Open Image Denoise](https://www.openimagedenoise.org/) |
//! | `approx` | Add `approx` support for all math types and `Color`                           |

pub mod color {
    //! Data types and functions related to colors and images.
    pub use crystal_ball_color::*;
}

pub mod derive {
    //! Derive macros for Crystal Ball.
    pub use crystal_ball_derive::*;
}

pub mod error {
    //! Error handling utilities.
    pub use crystal_ball_error::*;
}

#[cfg(feature = "gltf")]
pub mod gltf {
    //! Load glTF objects and scenes.
    pub use crystal_ball_gltf::*;
}

pub mod materials {
    //! Data types and functions related to materials.
    pub use crystal_ball_materials::*;
}

pub mod math {
    //! Data types and functions related to math.
    pub use crystal_ball_math::*;
}

pub mod rendering {
    //! Data types and functions related to rendering.
    pub use crystal_ball_rendering::*;
}

pub mod shapes {
    //! Geometric shapes that can be placed into the scene.
    pub use crystal_ball_shapes::*;
}

pub mod prelude;
