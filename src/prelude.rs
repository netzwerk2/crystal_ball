//! Commonly used structs and traits for easy access.

pub use crate::color::{Color, Image, Interpolation, IntoArcDynTexture};
pub use crate::error::Error;
pub use crate::materials::{IntoArcDynMaterial, PbrMaterial, IOR};
pub use crate::math::{Point3, Transform, Transformable, Vec3};
pub use crate::rendering::{Camera, CombinedPass, RenderEngine, RenderPasses, Scene};
pub use crate::shapes::{IntoArcDynShape, Mesh, Object, Sphere, Triangle, TriangleMesh, BVH};
