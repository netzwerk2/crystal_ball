[package]
name = "crystal_ball"
version.workspace = true
authors.workspace = true
edition.workspace = true
description = "A path tracing library written in Rust."
homepage.workspace = true
repository.workspace = true
documentation = "https://docs.rs/crystal_ball"
readme = "README.md"
keywords = ["path-tracing", "ray-tracing", "rendering", "graphics"]
categories = ["graphics", "rendering::engine", "multimedia::images"]
license.workspace = true
exclude = ["crates", "examples/"]

[lib]
doc-scrape-examples = true

[workspace]
members = ["crates/*"]

[workspace.package]
version = "0.3.0"
authors = ["Philipp Dobler <binksmrs@gmail.com>", "Green Lemonade <green.lemonade.x@gmail.com>"]
edition = "2021"
homepage = "https://gitlab.com/netzwerk2/crystal_ball"
repository = "https://gitlab.com/netzwerk2/crystal_ball"
license = "LGPL-3.0"

[workspace.dependencies]
image = "0.24.8"
oidn = "2.2.4"
gltf = { version = "1.4.0", default-features = false, features = ["import"] }
approx = "0.5.1"
nanorand = { version = "0.7.0", default-features = false, features = ["std", "wyrand", "tls"] }
indicatif = "0.17.8"
rayon = "1.8.1"
downcast-rs = "1.2.1"

# macros
proc-macro-crate = "3.1.0"
syn = "2.0.76"
proc-macro2 = "1.0.86"
quote = "1.0.37"
trybuild = { version = "1.0.99", features = ["diff"] }

# examples
roots = "0.0.8"
colorgrad = "0.6.2"
noise = "0.8.2"
fast_poisson = "1.0.0"

crystal_ball_color = { path = "crates/crystal_ball_color", version = "0.3.0" }
crystal_ball_derive = { path = "crates/crystal_ball_derive", version = "0.3.0" }
crystal_ball_error = { path = "crates/crystal_ball_error", version = "0.3.0" }
crystal_ball_gltf = { path = "crates/crystal_ball_gltf", version = "0.3.0" }
crystal_ball_materials = { path = "crates/crystal_ball_materials", version = "0.3.0" }
crystal_ball_math = { path = "crates/crystal_ball_math", version = "0.3.0" }
crystal_ball_rendering = { path = "crates/crystal_ball_rendering", version = "0.3.0" }
crystal_ball_shapes = { path = "crates/crystal_ball_shapes", version = "0.3.0" }

# Using the git version of 1.4.0 as 1.4.1 contained a breaking change that wouldn't load glTFs if required extensions weren't present.
[patch.crates-io]
gltf = { git = "https://github.com/gltf-rs/gltf.git", rev = "5d8154eb47316d7f8ab6d89e7da0ca1d56e89e87" }

[dependencies]
crystal_ball_color = { workspace = true }
crystal_ball_derive = { workspace = true }
crystal_ball_error = { workspace = true }
crystal_ball_gltf = { workspace = true, optional = true }
crystal_ball_materials = { workspace = true }
crystal_ball_math = { workspace = true }
crystal_ball_rendering = { workspace = true }
crystal_ball_shapes = { workspace = true }

[dev-dependencies]
roots = { workspace = true }
colorgrad = { workspace = true }
noise = { workspace = true }
fast_poisson = { workspace = true }
nanorand = { workspace = true }

[features]
default = ["gltf"]

oidn = ["crystal_ball_color/oidn", "crystal_ball_error/oidn"]
gltf = ["dep:crystal_ball_gltf", "crystal_ball_color/gltf", "crystal_ball_error/gltf"]
approx = ["crystal_ball_color/approx", "crystal_ball_math/approx"]

[[example]]
name = "basic"
path = "examples/basic/main.rs"
doc-scrape-examples = true

[[example]]
name = "cornell_box"
path = "examples/cornell_box/main.rs"

[[example]]
name = "denoise"
path = "examples/denoise/main.rs"
required-features = ["oidn"]

[[example]]
name = "donut"
path = "examples/donut/main.rs"

[[example]]
name = "environment_texture"
path = "examples/environment_texture/main.rs"

[[example]]
name = "gltf"
path = "examples/gltf/main.rs"
required-features = ["gltf"]

[[example]]
name = "hdri_preview"
path = "examples/hdri_preview/main.rs"

[[example]]
name = "image_texture"
path = "examples/image_texture/main.rs"

[[example]]
name = "render_passes"
path = "examples/render_passes/main.rs"

[profile.dev]
opt-level = 3

[profile.release]
# Reduce binary size
panic = "abort"
strip = "symbols"
# Improve performance but increase compile time
lto = "fat"
codegen-units = 1

[package.metadata.docs.rs]
cargo-args = ["-Zunstable-options", "-Zrustdoc-scrape-examples"]
rustdoc-args = ["--cfg", "docsrs"]
all-features = true
