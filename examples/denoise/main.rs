use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let objects = vec![
        Object::new(
            Sphere::new()
                .translate(Vec3::new(1.0, 0.0, 0.0))
                .scale_xyz(Vec3::splat(0.5)),
            PbrMaterial {
                base_color: Color::new(0.8, 0.6, 0.2),
                metallic: 1.0,
                ..Default::default()
            },
        ),
        Object::new(
            Sphere::new().scale_xyz(Vec3::splat(0.5)),
            PbrMaterial {
                base_color: Color::new(1.0, 0.45, 0.31),
                ..Default::default()
            },
        ),
        Object::new(
            Sphere::new()
                .translate(Vec3::new(0.1, 1.0, 0.0))
                .scale_xyz(Vec3::splat(0.1)),
            PbrMaterial {
                base_color: Color::BLACK,
                emissive_color: Color::WHITE,
                emissive_strength: 20.0,
                ..Default::default()
            },
        ),
        Object::new(
            Sphere::new()
                .translate(Vec3::new(-1.0, 0.0, 0.0))
                .scale_xyz(Vec3::splat(0.5)),
            PbrMaterial {
                base_color: Color::splat(0.9),
                metallic: 1.0,
                roughness: 0.3,
                ..Default::default()
            },
        ),
        Object::new(
            Sphere::new()
                .translate(Vec3::new(0.0, -100.5, 0.0))
                .scale_xyz(Vec3::splat(100.0)),
            PbrMaterial {
                base_color: Color::splat(0.7),
                ..Default::default()
            },
        ),
    ];
    let bvh = BVH::init(4, objects);

    let position = Point3::new(3.0, 2.0, 1.0);
    let target = Point3::ZERO;

    let scene = Scene {
        camera: Camera {
            aperture: 0.2,
            focal_distance: (target - position).magnitude(),
            ..Default::default()
        }
        .translate(position.to_vec3())
        .look_at(target, Vec3::Y),
        bvh,
        ..Default::default()
    };

    let engine = RenderEngine::default();
    let mut render_passes = RenderPasses::new(CombinedPass {
        max_bounces: 8,
        ..Default::default()
    });
    engine.render(&scene, &mut render_passes);

    let image = render_passes.get_image_mut::<CombinedPass>().unwrap();
    image.write("image.png")?;

    image.denoise_simple()?;
    image.write("image_denoised.png")?;

    Ok(())
}
