# Denoise

Example showing how to use denoising.

Note: denoising requires the `oidn` feature.

Use `cargo run --example denoise --features oidn` to run.

| Output                | Denoised Output                         |
|-----------------------|-----------------------------------------|
| ![Output](render.png) | ![Denoised Output](render_denoised.png) |