use std::f64::consts::PI;

use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let objects = vec![Object::new(
        Sphere::new(),
        PbrMaterial {
            base_color: Color::WHITE,
            metallic: 1.0,
            ..Default::default()
        },
    )];
    let bvh = BVH::init(4, objects);

    let scene = Scene {
        background_color: Color::WHITE,
        background_texture: Some(
            Image::from_file(
                "examples/environment_texture/spruit_sunrise_1k.hdr",
                Interpolation::Bilinear,
            )?
            .into_arc_dyn_texture(),
        ),
        background_transform: Transform::default().rotate_y(PI),
        bvh,
        camera: Camera::default()
            .translate(Vec3::new(0.0, 0.0, -5.0))
            .look_at(Point3::ZERO, Vec3::Y),
        ..Default::default()
    };

    let render_engine = RenderEngine::default();
    let mut render_passes = RenderPasses::default();
    render_engine.render(&scene, &mut render_passes);

    let image = render_passes.get_image::<CombinedPass>().unwrap();
    image.write("environment_texture.png")?;

    Ok(())
}
