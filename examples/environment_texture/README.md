# Environment Texture

Example showing how to use environment textures.

Use `cargo run --example environment_texture` to run.

## Output

![Environment Texture](render.png)