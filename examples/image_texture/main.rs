use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let objects = vec![
        Object::new(
            Sphere::new(),
            PbrMaterial {
                base_color: Color::BLACK,
                emissive_color: Color::WHITE,
                // Image texture by Solar System Scope (https://www.solarsystemscope.com/textures/download/2k_earth_daymap.jpg).
                emissive_texture: Some(
                    Image::from_file("examples/image_texture/earth.jpg", Interpolation::Bilinear)?
                        .into_arc_dyn_texture(),
                ),
                ..Default::default()
            },
        ),
        Object::new(
            Sphere::new()
                .translate(Vec3::new(2.5, 1.0, -2.0))
                .scale_xyz(Vec3::splat(0.1)),
            PbrMaterial {
                base_color: Color::BLACK,
                emissive_color: Color::WHITE,
                // Image texture by Solar System Scope (https://www.solarsystemscope.com/textures/download/2k_moon.jpg).
                emissive_texture: Some(
                    Image::from_file("examples/image_texture/moon.jpg", Interpolation::Bilinear)?
                        .into_arc_dyn_texture(),
                ),
                ..Default::default()
            },
        ),
        Object::new(
            Sphere::new()
                .translate(Vec3::new(0.0, -101.0, 0.0))
                .scale_xyz(Vec3::splat(100.0)),
            PbrMaterial {
                base_color: Color::WHITE,
                metallic: 1.0,
                roughness: 0.3,
                ..Default::default()
            },
        ),
    ];
    let bvh = BVH::init(4, objects);

    let scene = Scene {
        camera: Camera::default()
            .translate(Vec3::new(5.0, 1.0, -2.0))
            .look_at(Point3::ZERO, Vec3::Y),
        background_color: Color::splat(0.0),
        bvh,
        ..Default::default()
    };

    let engine = RenderEngine::default();
    let mut render_passes = RenderPasses::default();
    engine.render(&scene, &mut render_passes);

    let image = render_passes.get_image::<CombinedPass>().unwrap();
    image.write("image_texture.png")?;

    Ok(())
}
