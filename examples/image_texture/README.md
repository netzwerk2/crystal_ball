# Image Texture

An example showing how to use image textures.

Use ```cargo run --example image_texture``` to run.

## Output

![Basic](render.png)