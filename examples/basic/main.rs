use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let objects = vec![
        Object::new(
            Sphere::new(),
            PbrMaterial {
                base_color: Color::new(1.0, 0.45, 0.31),
                ..Default::default()
            },
        ),
        Object::new(
            Sphere::new()
                .translate(Vec3::new(0.0, -101.0, 0.0))
                .scale_xyz(Vec3::splat(100.0)),
            PbrMaterial {
                base_color: Color::splat(0.8),
                ..Default::default()
            },
        ),
    ];
    let bvh = BVH::init(4, objects);

    let scene = Scene {
        bvh,
        camera: Camera::default().translate(Vec3::new(0.0, 0.0, 5.0)),
        ..Default::default()
    };

    let engine = RenderEngine::default();
    let mut render_passes = RenderPasses::default();
    engine.render(&scene, &mut render_passes);

    let image = render_passes.get_image::<CombinedPass>().unwrap();
    image.write("basic.png")?;

    Ok(())
}
