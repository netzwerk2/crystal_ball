use crystal_ball::gltf::load_gltf_scene;
use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    // If you just want to load the models, use `Object::load_gltf` which returns Vec<Object>
    let scene = load_gltf_scene("examples/gltf/living_radio.glb")?;

    let engine = RenderEngine::default();
    let mut render_passes = RenderPasses::new(CombinedPass {
        max_bounces: 8,
        ..Default::default()
    });
    engine.render(&scene, &mut render_passes);

    let image = render_passes.get_image::<CombinedPass>().unwrap();
    image.write("gltf.png")?;

    Ok(())
}
