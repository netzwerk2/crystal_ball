# glTF

Example showing how to load a glTF file.<br>
Note: Not everything of the glTF spec is supported. Take a look at the documentation to find out what currently works.

Use `cargo run --example gltf` to run.

## Output

![glTF](render.png)