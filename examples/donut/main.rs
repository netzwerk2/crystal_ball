use crate::donut::Donut;
use crystal_ball::prelude::*;
#[cfg(feature = "oidn")]
use crystal_ball_rendering::OidnAlbedoPass;

mod donut;
mod shapes;
mod textures;

fn main() -> Result<(), Error> {
    let donut = Donut::default();
    let mut objects = donut.objects();

    let plane = TriangleMesh::plane(1.0)
        .scale_xyz(Vec3::splat(42.0))
        .translate(Vec3::new(0.0, -donut.dough.minor_radius, 0.0));
    objects.push(Object::new(Mesh::from(plane), PbrMaterial::default()));

    let bvh = BVH::init(4, objects);

    let scene = Scene {
        camera: Camera {
            fov: 25f64.to_radians(),
            ..Default::default()
        }
        .translate(Point3::new(0.0, 1.5, 3.0).to_vec3())
        .look_at(Point3::ZERO, Vec3::Y),
        background_texture: Some(
            Image::from_file(
                "examples/environment_texture/spruit_sunrise_1k.hdr",
                Interpolation::Bilinear,
            )?
            .into_arc_dyn_texture(),
        ),
        background_transform: Transform::default().rotate_y(90f64.to_radians()),
        bvh,
        ..Default::default()
    };

    let engine = RenderEngine::default();

    let mut render_passes = RenderPasses::new(CombinedPass {
        samples: 128,
        ..Default::default()
    });
    #[cfg(feature = "oidn")]
    {
        render_passes.add(OidnAlbedoPass {
            samples: 32,
            ..Default::default()
        });
    }

    engine.render(&scene, &mut render_passes);

    #[allow(unused_mut)]
    let mut image = render_passes
        .get_image_mut::<CombinedPass>()
        .unwrap()
        .clone();
    image.write("donut.png")?;

    #[cfg(feature = "oidn")]
    {
        let albedo_image = render_passes.get_image::<OidnAlbedoPass>().unwrap();

        // For this scene, denoising with a normal pass, gives worse results.
        image.denoise_albedo(albedo_image)?;
        image.write("donut_denoised.png")?;
    }

    println!("\nImage saved!");

    Ok(())
}
