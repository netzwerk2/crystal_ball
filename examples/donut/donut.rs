use crate::shapes::{Cylinder, Icing, Torus};
use crate::textures::DoughTexture;
use crystal_ball::derive::Transformable;
use crystal_ball::math::random_float;
use crystal_ball::prelude::*;
use fast_poisson::Poisson2D;
use nanorand::{Rng, SeedableRng};
use std::f64::consts::{FRAC_PI_2, TAU};
use std::ops::Range;
use std::sync::Arc;

/// The holy donut, consisting of dough, icing and sprinkles.
#[derive(Transformable)]
pub struct Donut {
    /// The donut's dough (a torus).
    ///
    /// The default geometry has a major radius of `0.53` and a minor radius of `0.25`.
    #[transformable]
    pub dough: Torus,
    /// The dough's texture.
    pub dough_texture: DoughTexture,
    /// The donut's icing.
    ///
    /// Defaults to a minor radius `0.02` thicker than `dough`'s minor radius.
    #[transformable]
    pub icing: Icing,
    /// The icing's color.
    ///
    /// Defaults to `(0.82, 0.44, 0.71)`.
    pub icing_color: Color,
    /// The range from which the sprinkle radii are sampled.
    ///
    /// Defaults to [`0.008`, `0.012`).
    pub sprinkle_radius: Range<f64>,
    /// The range from which the sprinkle lengths are sampled.
    ///
    /// Defaults to [`0.02`, `0.04`).
    pub sprinkle_length: Range<f64>,
    /// The minimum distance between sprinkles.
    ///
    /// Defaults to `0.06`.
    pub sprinkle_distance: f64,
    /// The sprinkle colors.
    ///
    /// The sprinkles are rendered as cylinders.
    pub sprinkle_colors: Vec<Color>,
}

impl Default for Donut {
    fn default() -> Self {
        let dough = Torus {
            major_radius: 0.53,
            ..Default::default()
        };
        let dough_texture = DoughTexture::default();

        let icing = Icing {
            torus: Torus {
                minor_radius: dough.minor_radius + 0.02,
                ..dough
            },
            ..Default::default()
        };
        let icing_color = Color::new(0.82, 0.44, 0.71);

        let sprinkle_radius = 0.008..0.012;
        let sprinkle_length = 0.02..0.04;
        let sprinkle_distance = 0.06;
        let sprinkle_colors = vec![
            Color::new(0.5, 0.79, 0.99),
            Color::new(0.74, 0.49, 0.99),
            Color::new(0.9, 0.46, 0.99),
            Color::new(0.99, 0.99, 0.99),
            Color::new(0.99, 0.95, 0.6),
        ];

        Self {
            dough,
            dough_texture,
            icing,
            icing_color,
            sprinkle_radius,
            sprinkle_length,
            sprinkle_distance,
            sprinkle_colors,
        }
    }
}

impl Donut {
    /// Return all objects making up the donut: dough, icing and sprinkles.
    pub fn objects(&self) -> Vec<Object> {
        let Self {
            dough,
            dough_texture,
            icing,
            icing_color,
            sprinkle_radius,
            sprinkle_length,
            sprinkle_distance,
            sprinkle_colors,
        } = &self;

        let mut objects = vec![];

        // Generate a material for each color.
        // Since the materials are wrapped in an `Arc`,
        // they can be reused instead of creating a new material for every sprinkle.
        let sprinkle_materials = sprinkle_colors
            .iter()
            .map(|c| {
                Arc::new(PbrMaterial {
                    base_color: *c,
                    ..Default::default()
                })
            })
            .collect::<Vec<Arc<PbrMaterial>>>();

        let mut rng = nanorand::tls_rng();
        rng.reseed(42u64.to_ne_bytes());

        // The max opening angle around the torus at which sprinkles can be distributed.
        // 180° corresponds to the upper half of the donut,
        // while 90° would correspond to a strip between 45° inwards and outwards
        // of the top circle.
        let sprinkles_max_opening_angle = 140f64.to_radians();

        let box_size = 2.0 * (dough.major_radius + dough.minor_radius);

        // For sprinkles' positions, we sample a 2D poisson disk distribution
        // and project the points onto the torus.
        // This should resemble how sprinkles get distributed on a real donut.
        let poisson = Poisson2D::new()
            .with_seed(42)
            .with_dimensions([box_size; 2], *sprinkle_distance);

        // Since the `fast_poisson` crate fixes one corner of the box to be (0, 0),
        // we need to subtract half the width and height to center the box at (0, 0).
        for [x, z] in poisson
            .iter()
            .map(|[x, z]| [x - 0.5 * box_size, z - 0.5 * box_size])
        {
            // We distribute and orient the sprinkles in the dough's local coordinate system
            // before transforming them to world space.

            let phi = z.atan2(x);
            let distance = x.hypot(z) - dough.major_radius;
            // Sprinkles will always be at the top (y > 0).
            let theta = (distance / dough.minor_radius).acos();

            // Skip the generated point, if it lies outside the max opening angle.
            // The opening angle is measured from the Y-axis while θ is measured from the X-axis.
            if !(-sprinkles_max_opening_angle * 0.5..sprinkles_max_opening_angle * 0.5)
                .contains(&(theta - FRAC_PI_2))
            {
                continue;
            }

            let local_position = icing.torus.point(phi, theta);

            let normal = icing
                .torus
                .transform
                .transform_normal(self.icing.torus.normal(local_position));
            let tangent = icing
                .torus
                .transform
                .transform_tangent(Torus::tangent(local_position));

            let normal_angle = random_float(&mut rng, 0.0, TAU);
            let tangent_angle = theta;

            let radius = random_float(&mut rng, sprinkle_radius.start, sprinkle_radius.end);
            let length = random_float(&mut rng, sprinkle_length.start, sprinkle_length.end);

            let material =
                Arc::clone(&sprinkle_materials[rng.generate_range(0..sprinkle_materials.len())]);

            let mut cylinder = Cylinder::default()
                // Scale the sprinkle to the generated dimensions.
                .scale_xyz(Vec3::new(radius, length, radius))
                // Rotate around the tangent, so the sprinkles lie tangent to the surface.
                .rotate(Point3::ZERO, tangent.xyz(), tangent_angle)
                // Rotate around the normal, so the sprinkles don't all point in the same direction.
                .rotate(Point3::ZERO, normal, normal_angle)
                // Translate to the respective point on the donut's surface.
                .translate(local_position.to_vec3());
            // Transform the cylinder to world space.
            cylinder.transform = dough.transform * cylinder.transform;

            objects.push(Object::new(cylinder, material));
        }

        objects.extend([
            Object::new(
                *dough,
                PbrMaterial {
                    base_color: Color::WHITE,
                    base_color_texture: Some(dough_texture.into_arc_dyn_texture()),
                    ..Default::default()
                },
            ),
            Object::new(
                *icing,
                PbrMaterial {
                    base_color: *icing_color,
                    ..Default::default()
                },
            ),
        ]);

        objects
    }
}
