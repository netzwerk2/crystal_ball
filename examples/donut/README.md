# Donut

A complex example showing how to create a procedural donut (geometrically, not using triangles).
It demonstrates how to:

- create custom shapes and textures
- use environment textures
- denoise the rendered image
- interact with different crates

Use `cargo run --example donut` to run.

If you want to enable denoising, use `cargo run --example donut --features oidn`

| Output                | Denoised Output                         |
|-----------------------|-----------------------------------------|
| ![Output](render.png) | ![Denoised Output](render_denoised.png) |
