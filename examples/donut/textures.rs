use std::f64::consts::TAU;

use colorgrad::CustomGradient;
use noise::{NoiseFn, SuperSimplex};

use crystal_ball::color::Texture;
use crystal_ball::prelude::*;

/// The texture of the donut's dough.
///
/// It uses a gradient which gets lighter in the middle
/// and applies some noise to make it look more natural.
#[derive(Copy, Clone)]
pub struct DoughTexture {
    /// The darker color.
    ///
    /// Defaults to `(1, 0.71, 0.41)`.
    pub outer_color: Color,
    /// The lighter color.
    ///
    /// Defaults to (1, 0.86, 0.73)`.
    pub inner_color: Color,
    /// The width of the transition between the darker and lighter part.
    ///
    /// Defaults to `0.1`.
    pub transition_width: f64,
    /// The width of the lighter part.
    ///
    /// `inner_width` + `transition_width` + `outer_width` = 1.
    /// Therefore, the sum of `transition_width` and `inner_width` should be < 1.
    ///
    /// Defaults to `0.05`.
    pub inner_width: f64,
    /// The noise used for offsetting the texture.
    ///
    /// Defaults to using the seed `42`.
    pub noise: SuperSimplex,
    /// The amplitude used to determine how much the noise should be offsetting the texture.
    ///
    /// Defaults to `0.015`.
    pub amplitude: f64,
    /// The frequency used to sample the noise texture in the x-direction.
    ///
    /// Defaults to `2.0`.
    pub frequency_u: f64,
    /// The frequency used to sample the noise texture in the y-direction.
    ///
    /// Defaults to `3.2`.
    pub frequency_v: f64,
}

impl Default for DoughTexture {
    fn default() -> Self {
        Self {
            outer_color: Color::new(1.0, 0.71, 0.41),
            inner_color: Color::new(1.0, 0.86, 0.73),
            transition_width: 0.1,
            inner_width: 0.05,
            noise: SuperSimplex::new(42),
            amplitude: 0.015,
            frequency_u: 2.0,
            frequency_v: 3.2,
        }
    }
}

impl Texture for DoughTexture {
    fn get_pixel(&self, u: f64, v: f64) -> Color {
        let outer_color = colorgrad::Color::new(
            self.outer_color.r as f64,
            self.outer_color.g as f64,
            self.outer_color.b as f64,
            1.0,
        );
        let inner_color = colorgrad::Color::new(
            self.inner_color.r as f64,
            self.inner_color.g as f64,
            self.inner_color.b as f64,
            1.0,
        );

        let border_width = 1.0 - self.transition_width - self.inner_width;
        let offset = self.amplitude
            * self.noise.get([
                self.frequency_u * (u * TAU).cos(),
                self.frequency_v * (v * TAU).sin(),
            ]);

        let gradient = CustomGradient::new()
            .colors(&[
                outer_color.clone(),
                outer_color.clone(),
                inner_color.clone(),
                inner_color,
                outer_color.clone(),
                outer_color,
            ])
            .domain(&[
                0.0,
                border_width * 0.5 + offset,
                border_width * 0.5 + self.transition_width * 0.5 + offset,
                border_width * 0.5 + self.transition_width * 0.5 + self.inner_width + offset,
                border_width * 0.5 + self.transition_width + self.inner_width + offset,
                1.0,
            ])
            .build()
            .unwrap();

        let color = gradient.at(v);

        Color::new(color.r as f32, color.g as f32, color.b as f32)
    }
}
