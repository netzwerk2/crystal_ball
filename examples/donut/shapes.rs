use crystal_ball::derive::Transformable;
use crystal_ball::math::{Bounds3, Hit, Point2, Ray, Vec2, Vec4};
use crystal_ball::prelude::*;
use crystal_ball::shapes::Shape;
use noise::{NoiseFn, SuperSimplex};
use roots::SearchError;
use std::f64::consts::{FRAC_1_PI, FRAC_PI_2, PI, TAU};

/// A torus defined by a major and minor radius.
///
/// For detailed information see [Wikipedia](https://en.wikipedia.org/wiki/Torus).
#[derive(Copy, Clone, Transformable)]
pub struct Torus {
    /// The torus' major radius.
    ///
    /// Basically, this describes the radius of the circle around which the smaller circle revolves.
    ///
    /// Defaults to `1.0`.
    pub major_radius: f64,
    /// The torus' minor radius.
    ///
    /// Basically, this describes the radius of the circle which revolves around the bigger circle.
    ///
    /// Defaults to `0.25`.
    pub minor_radius: f64,
    #[transformable]
    pub transform: Transform,
}

impl Default for Torus {
    fn default() -> Self {
        Self {
            major_radius: 1.0,
            minor_radius: 0.25,
            transform: Transform::default(),
        }
    }
}

impl Torus {
    /// Calculate φ and θ from a point on the torus' surface.
    ///
    /// φ describes the position on the major circle,
    /// while θ describes the position on the minor circle.
    /// θ=0 represents the outer "equator", while θ=π/2 corresponds to the topmost circle (radius R).
    pub fn angles(&self, point: Point3) -> (f64, f64) {
        let phi = point.z.atan2(point.x);

        let horizontal_distance = Vec2::new(point.x, point.z).magnitude() - self.major_radius;
        let theta = point.y.atan2(horizontal_distance).rem_euclid(TAU);

        (phi, theta)
    }

    /// Calculate the point on the torus' surface described by φ and θ.
    pub fn point(&self, phi: f64, theta: f64) -> Point3 {
        let horizontal_distance = self.major_radius + self.minor_radius * theta.cos();

        Point3::new(
            (horizontal_distance) * phi.cos(),
            self.minor_radius * theta.sin(),
            (horizontal_distance) * phi.sin(),
        )
    }

    /// Calculate the distance of a point from the torus' symmetry axis divided by its major radius.
    pub fn normalized_axis_distance(&self, point: Point3) -> f64 {
        Vec2::new(point.x, point.z).magnitude() / self.major_radius
    }

    /// Calculate the normal for a given point on the torus' surface.
    ///
    /// See http://cosinekitty.com/raytrace/chapter13_torus.html for details.
    pub fn normal(&self, point: Point3) -> Vec3 {
        let alpha = self.major_radius / Vec2::new(point.x, point.z).magnitude();

        Vec3::new((1.0 - alpha) * point.x, point.y, (1.0 - alpha) * point.z)
    }

    /// Calculate the tangent for a given point on the torus' surface.
    pub fn tangent(point: Point3) -> Vec4 {
        Vec3::cross(Vec3::new(point.x, 0.0, point.z), Vec3::Y)
            .normalize()
            .extend(1.0)
    }

    /// Calculate the UV coordinates for a given point on the torus' surface.
    pub fn uv_map(&self, point: Point3) -> Point2 {
        let (phi, theta) = self.angles(point);

        let u = (phi * 0.5 * FRAC_1_PI).rem_euclid(1.0);
        let v = (0.5 + theta * 0.5 * FRAC_1_PI).rem_euclid(1.0);

        Point2::new(u, v)
    }

    /// Solve the quartic equation of the ray-torus intersection.
    ///
    /// See http://cosinekitty.com/raytrace/chapter13_torus.html for details.
    fn find_roots(&self, ray: Ray) -> Vec<Result<f64, SearchError>> {
        let g =
            4.0 * self.major_radius.powi(2) * (ray.direction.x.powi(2) + ray.direction.z.powi(2));
        let h = 8.0
            * self.major_radius.powi(2)
            * (ray.origin.x * ray.direction.x + ray.origin.z * ray.direction.z);
        let i = 4.0 * self.major_radius.powi(2) * (ray.origin.x.powi(2) + ray.origin.z.powi(2));
        let j = ray.direction.magnitude_squared();
        let k = 2.0 * Vec3::dot(ray.origin.to_vec3(), ray.direction);
        let l = ray.origin.to_vec3().magnitude_squared()
            + (self.major_radius.powi(2) - self.minor_radius.powi(2));

        let a = j.powi(-2);
        let b = 2.0 * j * k;
        let c = 2.0 * j * l + k.powi(2) - g;
        let d = 2.0 * k * l - h;
        let e = l.powi(2) - i;

        // Using an iterative approach, as roots::find_roots_quartic leads to artifacts
        roots::find_roots_sturm(&[b * a, c * a, d * a, e * a], &mut 1e-10)
    }

    /// Find the smallest positive root.
    fn first_root(roots: &[Result<f64, SearchError>]) -> Option<f64> {
        roots
            .iter()
            .flatten()
            .filter(|x| **x > 0.0)
            .min_by(|x, y| x.partial_cmp(y).unwrap())
            .copied()
    }
}

impl Shape for Torus {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        let ray = self.transform.inverse_transform_ray(ray);

        let roots = self.find_roots(ray);

        let mut intersection_distance = Self::first_root(&roots)?;
        let mut intersection_point = ray.get(intersection_distance);

        let mut normal = self.normal(intersection_point);
        normal = self.transform.transform_normal(normal);

        let uv = self.uv_map(intersection_point);

        let ray = self.transform.transform_ray(ray);

        intersection_point = self.transform.mat4 * intersection_point;
        intersection_distance = (intersection_point - ray.origin).magnitude();

        if Vec3::dot(normal, ray.direction) > 0.0 {
            normal = -normal;
        }

        Some(Hit::new(
            intersection_point,
            normal,
            None,
            intersection_distance,
            uv,
        ))
    }

    fn bounds(&self) -> Bounds3 {
        let min = Point3::new(
            -self.major_radius - self.minor_radius,
            -self.minor_radius,
            -self.major_radius - self.minor_radius,
        );
        let max = -min;

        self.transform.transform_bounds(Bounds3::new(min, max))
    }
}

/// The donut's icing.
///
/// It uses a torus which is cut off at a certain angle.
/// The cutoff angle is driven by a noise texture.
#[derive(Copy, Clone, Transformable)]
pub struct Icing {
    /// The torus used for performing the ray intersections.
    #[transformable]
    pub torus: Torus,
    /// The opening angle (rad) around the torus, which the icing should cover.
    /// 180° corresponds to the upper half of the donut,
    /// while 90° would correspond to a strip between 45° inwards and outwards
    /// of the top circle.
    ///
    /// Defaults to π (180°).
    pub opening_angle: f64,
    /// The noise texture used for offsetting the opening angle.
    ///
    /// Defaults to using the seed `42`.
    pub noise: SuperSimplex,
    /// The amplitude used to determine how much the noise should be offsetting the opening angle.
    ///
    /// Defaults to `0.1`.
    pub amplitude: f64,
    /// The frequency used to sample the noise texture.
    ///
    /// Defaults to `1.8`.
    pub frequency: f64,
}

impl Default for Icing {
    fn default() -> Self {
        Self {
            torus: Torus::default(),
            opening_angle: PI,
            noise: SuperSimplex::new(42),
            amplitude: 0.1,
            frequency: 1.8,
        }
    }
}

impl Icing {
    /// Calculate the offset added to theta for a given φ.
    ///
    /// The `normalized_axis_distance` is used to compensate the fact that points
    /// closer to the torus' center are closer together.
    fn theta_offset(&self, phi: f64, normalized_axis_distance: f64) -> f64 {
        self.amplitude
            * self.noise.get([
                self.frequency * phi.cos() * normalized_axis_distance,
                self.frequency * phi.sin() * normalized_axis_distance,
            ])
    }

    /// Calculate the closest intersection with a ray,
    /// given the solutions of the quartic equation found by [`Torus::find_roots`].
    pub fn closest_intersection(
        &self,
        roots: &[Result<f64, SearchError>],
        ray: Ray,
    ) -> Option<Point3> {
        roots
            .iter()
            .flatten()
            .filter(|t| **t > 0.0)
            .flat_map(|t| {
                let intersection_point = ray.get(*t);
                let (phi, theta) = self.torus.angles(intersection_point);
                let normalized_axis_distance =
                    self.torus.normalized_axis_distance(intersection_point);

                let theta_offset = self.theta_offset(phi, normalized_axis_distance);

                // The opening angle is measured from the Y-axis while θ is measured from the X-axis.
                if theta.sin() > (FRAC_PI_2 - self.opening_angle * 0.5 + theta_offset).sin() {
                    return Some((*t, intersection_point));
                }

                None
            })
            .min_by(|(x, _), (y, _)| x.partial_cmp(y).unwrap())
            .map(|(_, p)| p)
    }
}

impl Shape for Icing {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        let ray = self.torus.transform.inverse_transform_ray(ray);

        let roots = self.torus.find_roots(ray);

        let mut intersection_point = self.closest_intersection(&roots, ray)?;

        let mut normal = self.torus.normal(intersection_point);
        normal = self.torus.transform.transform_normal(normal);

        let uv = self.torus.uv_map(intersection_point);

        let ray = self.torus.transform.transform_ray(ray);

        intersection_point = self.torus.transform.mat4 * intersection_point;
        let intersection_distance = (intersection_point - ray.origin).magnitude();

        if Vec3::dot(ray.direction, normal) > 0.0 {
            normal = -normal;
        }

        Some(Hit::new(
            intersection_point,
            normal,
            None,
            intersection_distance,
            uv,
        ))
    }

    fn bounds(&self) -> Bounds3 {
        let theta = FRAC_PI_2 - self.opening_angle * 0.5;

        let horizontal_distance = if theta.sin() > 0.0 {
            self.torus.major_radius + self.torus.minor_radius * theta.cos()
        } else {
            self.torus.major_radius + self.torus.minor_radius
        };
        let vertical_distance =
            (FRAC_PI_2 - self.opening_angle * 0.5 - self.amplitude).sin() * self.torus.minor_radius;

        let min = Point3::new(
            -horizontal_distance,
            vertical_distance,
            -horizontal_distance,
        );
        let max = Point3::new(
            horizontal_distance,
            self.torus.minor_radius,
            horizontal_distance,
        );

        self.torus
            .transform
            .transform_bounds(Bounds3::new(min, max))
    }
}

/// A cylinder with a radius of `1.0` and a length of `2.0`.
#[derive(Copy, Clone, Default, Transformable)]
pub struct Cylinder {
    #[transformable]
    pub transform: Transform,
}

impl Shape for Cylinder {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        let ray = self.transform.inverse_transform_ray(ray);

        // First, calculate the intersection with the cylinder's circle.

        let origin = Vec2::new(ray.origin.x, ray.origin.z);
        let direction = Vec2::new(ray.direction.x, ray.direction.z);

        let origin_squared = origin.magnitude_squared() - 1.0;
        let direction_squared = direction.magnitude_squared();
        let origin_dot_direction = Vec2::dot(origin, direction);

        let discriminant = origin_dot_direction.powi(2) - origin_squared * direction_squared;
        if discriminant < 0.0 {
            return None;
        }

        let intersection_distance = if origin.magnitude_squared() > 1.0 {
            (-origin_dot_direction - discriminant.sqrt()) / direction_squared
        } else {
            (-origin_dot_direction + discriminant.sqrt()) / direction_squared
        };
        let intersection_point = ray.get(intersection_distance);

        let normal = intersection_point.to_vec3().normalize();

        // Verify that the circle intersection happens inside the cylinder's height.
        let circle_intersection = if intersection_point.y.abs() < 1.0 {
            Some(Hit::new(
                intersection_point,
                normal,
                None,
                intersection_distance,
                Point2::default(),
            ))
        } else {
            None
        };

        // Then calculate the intersections with the top and bottom caps,
        // so the cylinder doesn't look hollow.

        let top_cylinder_cap = CylinderCapPlane::new(1.0, 1.0);
        let bottom_cylinder_cap = CylinderCapPlane::new(1.0, -1.0);

        let top_intersection = top_cylinder_cap.intersects(ray);
        let bottom_intersection = bottom_cylinder_cap.intersects(ray);

        let closest_intersection = [circle_intersection, top_intersection, bottom_intersection]
            .into_iter()
            .flatten()
            .filter(|h| h.distance > 0.0)
            .min_by(|x, y| x.distance.partial_cmp(&y.distance).unwrap())?;

        let ray = self.transform.transform_ray(ray);

        let intersection_point = self.transform.mat4 * closest_intersection.position;
        let normal = self.transform.transform_normal(closest_intersection.normal);
        let intersection_distance = (intersection_point - ray.origin).magnitude();

        Some(Hit::new(
            intersection_point,
            normal,
            None,
            intersection_distance,
            closest_intersection.uv,
        ))
    }

    fn bounds(&self) -> Bounds3 {
        let min = Point3::new(-1.0, -1.0, -1.0);
        let max = Point3::new(1.0, 1.0, 1.0);

        self.transform.transform_bounds(Bounds3::new(min, max))
    }
}

/// The cap of a cylinder.
///
/// This is simply a plane, where intersections are only valid inside a specified radius.
struct CylinderCapPlane {
    /// The height at which the plane is located.
    radius: f64,
    /// The radius which cuts off the plane.
    height: f64,
}

impl CylinderCapPlane {
    fn new(radius: f64, height: f64) -> Self {
        Self { radius, height }
    }
}

impl Shape for CylinderCapPlane {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        let intersection_distance = (self.height - ray.origin.y) / ray.direction.y;
        let intersection_point = ray.get(intersection_distance);

        if intersection_distance > 0.0
            && Vec2::new(intersection_point.x, intersection_point.z).magnitude() < self.radius
        {
            let normal = if ray.direction.y > 0.0 {
                -Vec3::Y
            } else {
                Vec3::Y
            };

            Some(Hit::new(
                intersection_point,
                normal,
                None,
                intersection_distance,
                Point2::splat(0.0),
            ))
        } else {
            None
        }
    }

    fn bounds(&self) -> Bounds3 {
        Bounds3::new(
            Point3::new(-self.radius, self.height, -self.radius),
            Point3::new(self.radius, self.height, self.radius),
        )
    }
}
