use crystal_ball::prelude::*;
use std::f64::consts::{FRAC_PI_2, FRAC_PI_8, PI};

fn main() -> Result<(), Error> {
    let box_half_size = 0.5;
    let cube_half_size = 0.1;

    let grey_walls = Object::new(
        Mesh::from_iter([
            TriangleMesh::plane(box_half_size).translate(Vec3::new(0.0, -box_half_size, 0.0)),
            TriangleMesh::plane(box_half_size)
                .translate(Vec3::new(0.0, 0.0, -box_half_size))
                .rotate_x(FRAC_PI_2),
            TriangleMesh::plane(box_half_size)
                .translate(Vec3::new(0.0, box_half_size, 0.0))
                .rotate_x(PI),
        ]),
        PbrMaterial {
            base_color: Color::WHITE,
            ..Default::default()
        },
    );
    let green_wall = Object::new(
        Mesh::from(
            TriangleMesh::plane(box_half_size)
                .clone()
                .translate(Vec3::new(box_half_size, 0.0, 0.0))
                .rotate_z(FRAC_PI_2),
        ),
        PbrMaterial {
            base_color: Color::new(0.2, 0.4, 0.2),
            ..Default::default()
        },
    );
    let red_wall = Object::new(
        Mesh::from(
            TriangleMesh::plane(box_half_size)
                .clone()
                .translate(Vec3::new(-box_half_size, 0.0, 0.0))
                .rotate_z(-FRAC_PI_2),
        ),
        PbrMaterial {
            base_color: Color::new(0.8, 0.2, 0.2),
            ..Default::default()
        },
    );

    let light = Object::new(
        Mesh::from(
            TriangleMesh::plane(box_half_size)
                .scale_xyz(Vec3::splat(0.3))
                .translate(Vec3::new(0.0, box_half_size - 0.001, 0.0)),
        ),
        PbrMaterial {
            base_color: Color::BLACK,
            emissive_color: Color::WHITE,
            emissive_strength: 10.0,
            ..Default::default()
        },
    );

    let cube = Object::new(
        Mesh::from(
            TriangleMesh::cube(cube_half_size)
                .scale_y(2.0)
                .rotate_y(-FRAC_PI_8)
                .translate(Vec3::new(-0.25, -0.3, -0.1)),
        ),
        PbrMaterial {
            base_color: Color::WHITE,
            ..Default::default()
        },
    );

    let spheres = vec![
        Object::new(
            Sphere::new()
                .translate(Vec3::new(0.3, -0.4, 0.15))
                .scale_xyz(Vec3::splat(0.1)),
            PbrMaterial {
                base_color: Color::WHITE,
                transmission: 1.0,
                ior: IOR::DIAMOND,
                ..Default::default()
            },
        ),
        Object::new(
            Sphere::new()
                .translate(Vec3::new(-0.25, -0.05, -0.1))
                .scale_xyz(Vec3::splat(0.05)),
            PbrMaterial {
                base_color: Color::WHITE,
                metallic: 1.0,
                ..Default::default()
            },
        ),
    ];

    let mut objects = vec![grey_walls, green_wall, red_wall, light, cube];

    for sphere in spheres {
        objects.push(sphere);
    }

    let bvh = BVH::init(4, objects);

    let scene = Scene {
        camera: Camera::default().translate(Vec3::new(0.0, 0.0, 1.5)),
        bvh,
        background_color: Color::BLACK,
        ..Default::default()
    };

    let engine = RenderEngine {
        width: 1024,
        height: 1024,
        ..Default::default()
    };
    let mut render_passes = RenderPasses::new(CombinedPass {
        samples: 256,
        max_bounces: 16,
        ..Default::default()
    });
    engine.render(&scene, &mut render_passes);

    let image = render_passes.get_image::<CombinedPass>().unwrap();
    image.write("cornell_box.png")?;

    println!("\nImage saved!");

    Ok(())
}
