# Cornell Box

Example showing how to create a Cornell box. It also demonstrates how to create meshes (from triangles) and use
transforms.

Use `cargo run --example cornell_box` to run.

## Output

![Cornell Box](render.png)