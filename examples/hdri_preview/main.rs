use std::path::PathBuf;
use std::{env, process};

use crystal_ball::color::Texture;
use crystal_ball::math::{Bounds3, Hit, Point2, Ray};
use crystal_ball::prelude::*;
use crystal_ball::shapes::Shape;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Checkerboard {
    pub scale: f64,
    pub color1: Color,
    pub color2: Color,
}

impl Default for Checkerboard {
    fn default() -> Self {
        Checkerboard {
            scale: 2.0,
            color1: Color::splat(0.6),
            color2: Color::splat(0.4),
        }
    }
}

impl Texture for Checkerboard {
    fn get_pixel(&self, u: f64, v: f64) -> Color {
        let (u, v) = (u.rem_euclid(self.scale), v.rem_euclid(self.scale));

        if (u - self.scale * 0.5) * (v - self.scale * 0.5) > 0.0 {
            self.color1
        } else {
            self.color2
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct InfinitePlane {
    pub origin: Point3,
    pub normal: Vec3,
    pub uv_scale: f64,
}

impl Default for InfinitePlane {
    fn default() -> Self {
        InfinitePlane {
            origin: Point3::splat(0.0),
            normal: Vec3::Y,
            uv_scale: 2.0,
        }
    }
}

impl Shape for InfinitePlane {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        let intersection_distance = Vec3::dot(self.normal, self.origin - ray.origin)
            / Vec3::dot(self.normal, ray.direction);

        if intersection_distance > 0.0 {
            let intersection_point = ray.get(intersection_distance);

            let a = Vec3::cross(self.normal, Vec3::X);
            let b = Vec3::cross(self.normal, Vec3::Y);

            let u = if Vec3::dot(a, a) > Vec3::dot(b, b) {
                a
            } else {
                b
            };
            let v = Vec3::cross(self.normal, u);

            let uv = Point2::new(
                Vec3::dot(u, intersection_point.into()).rem_euclid(self.uv_scale),
                Vec3::dot(v, intersection_point.into()).rem_euclid(self.uv_scale),
            );

            let normal = if Vec3::dot(ray.direction, self.normal) > 0.0 {
                -self.normal
            } else {
                self.normal
            };

            Some(Hit::new(
                intersection_point,
                normal,
                None,
                intersection_distance,
                uv,
            ))
        } else {
            None
        }
    }

    fn bounds(&self) -> Bounds3 {
        Bounds3::new(
            Point3::new(f64::NEG_INFINITY, 0.0, f64::NEG_INFINITY),
            Point3::new(f64::INFINITY, 0.0, f64::INFINITY),
        )
    }
}

pub struct HDRIPreview {
    pub hdri_path: PathBuf,
    pub output_path: PathBuf,
}

impl HDRIPreview {
    pub fn new(mut args: env::Args) -> Result<HDRIPreview, &'static str> {
        args.next();

        let hdri_string = match args.next() {
            Some(arg) => arg,
            None => return Err("No HDRI path specified."),
        };
        let hdri_path = PathBuf::from(hdri_string);

        let output_string = match args.next() {
            Some(arg) => arg,
            None => return Err("No output path specified."),
        };
        let output_path = PathBuf::from(output_string);

        Ok(HDRIPreview {
            hdri_path,
            output_path,
        })
    }

    pub fn run(&self) -> Result<(), Error> {
        let background_image = Image::from_file(&self.hdri_path, Interpolation::Bilinear)?;

        let objects = vec![
            Object::new(
                Sphere::new().translate(Vec3::new(4.5, 1.0, 0.0)),
                PbrMaterial {
                    base_color: Color::WHITE,
                    ior: IOR::DIAMOND,
                    transmission: 1.0,
                    ..Default::default()
                },
            ),
            Object::new(
                Sphere::new().translate(Vec3::new(1.5, 1.0, 0.0)),
                PbrMaterial {
                    base_color: Color::WHITE,
                    ..Default::default()
                },
            ),
            Object::new(
                Sphere::new().translate(Vec3::new(-1.5, 1.0, 0.0)),
                PbrMaterial {
                    base_color: Color::WHITE,
                    metallic: 1.0,
                    ..Default::default()
                },
            ),
            Object::new(
                Sphere::new().translate(Vec3::new(-4.5, 1.0, 0.0)),
                PbrMaterial {
                    base_color: Color::new(0.44, 0.87, 0.93),
                    metallic: 0.07,
                    roughness: 0.0,
                    transmission: 0.0,
                    ..Default::default()
                },
            ),
            Object::new(
                InfinitePlane::default(),
                PbrMaterial {
                    base_color: Color::WHITE,
                    base_color_texture: Some(Checkerboard::default().into_arc_dyn_texture()),
                    ..Default::default()
                },
            ),
        ];
        let bvh = BVH::init(4, objects);

        let scene = Scene {
            camera: Camera {
                fov: 12f64.to_radians(),
                ..Default::default()
            }
            .translate(Vec3::new(0.0, 8.0, -20.0))
            .look_at(Point3::new(0.0, 0.9, 0.0), Vec3::Y),
            background_color: Color::WHITE,
            background_texture: Some(background_image.into_arc_dyn_texture()),
            bvh,
            ..Default::default()
        };

        let render_engine = RenderEngine {
            width: 1920,
            height: 720,
            ..Default::default()
        };
        let mut render_passes = RenderPasses::new(CombinedPass {
            samples: 1024,
            max_bounces: 32,
            ..Default::default()
        });
        render_engine.render(&scene, &mut render_passes);

        let image = render_passes.get_image::<CombinedPass>().unwrap();
        image.write(&self.output_path)?;

        Ok(())
    }
}

fn main() {
    let hdri_preview = HDRIPreview::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Error: {}", err);
        process::exit(1);
    });

    if let Err(e) = hdri_preview.run() {
        eprintln!("Error: {}", e);
        process::exit(1);
    }
}
