# HDRI Preview

This is a more complex example showing you how to create an HDRI preview similar to those
on [HDRI Haven](https://hdrihaven.com). It also demonstrates how to create your own shapes and textures.

Environment texture by [HDRI Haven](https://hdrihaven.com/hdri/?h=parched_canal).

Use `cargo run --release --example hdri_preview examples/hdri_preview/parched_canal_1k.hdr hdri_preview.png` to run.

## Output

![HDRI Preview](render.png)