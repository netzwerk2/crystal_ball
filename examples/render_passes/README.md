# Basic

A simple example showcasing the different render passes and how to use them.

Use `cargo run --example render_passes` to run.

## Output

| Render Pass | Image                            |
|-------------|----------------------------------|
| Combined    | ![Combined](render_combined.png) |
| OidnAlbedo  | ![Albedo](render_albedo.png)     |
| OidnNormal  | ![Normal](render_normal.png)     |
| Depth       | ![Depth](render_depth.png)       |
| UV          | ![UV](render_uv.png)             |
