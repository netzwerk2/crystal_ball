use crystal_ball::prelude::*;
use crystal_ball::rendering::{DepthPass, OidnAlbedoPass, OidnNormalPass, UVPass};

fn main() -> Result<(), Error> {
    let objects = vec![
        Object::new(
            Sphere::new(),
            PbrMaterial {
                base_color: Color::new(1.0, 0.45, 0.31),
                ..Default::default()
            },
        ),
        Object::new(
            Sphere::new()
                .translate(Vec3::new(0.0, -101.0, 0.0))
                .scale_xyz(Vec3::splat(100.0)),
            PbrMaterial {
                base_color: Color::splat(0.8),
                ..Default::default()
            },
        ),
    ];
    let bvh = BVH::init(4, objects);

    let scene = Scene {
        bvh,
        camera: Camera::default().translate(Vec3::new(0.0, 0.0, 5.0)),
        ..Default::default()
    };

    let samples = 16;
    let engine = RenderEngine::default();

    let mut render_passes = RenderPasses::empty();
    render_passes.add(CombinedPass {
        samples,
        ..Default::default()
    });
    render_passes.add(OidnAlbedoPass {
        samples,
        ..Default::default()
    });
    render_passes.add(OidnNormalPass {
        samples,
        ..OidnNormalPass::view()
    });
    render_passes.add(DepthPass {
        start: 0.0,
        end: 10.0,
        samples,
        ..Default::default()
    });
    render_passes.add(UVPass {
        samples,
        ..Default::default()
    });
    engine.render(&scene, &mut render_passes);

    let combined_image = render_passes.get_image::<CombinedPass>().unwrap();
    let albedo_image = render_passes.get_image::<OidnAlbedoPass>().unwrap();
    let mut normal_image = render_passes.get_image::<OidnNormalPass>().unwrap().clone();
    let depth_image = render_passes.get_image::<DepthPass>().unwrap();
    let uv_image = render_passes.get_image::<UVPass>().unwrap();

    normal_image.pixels.iter_mut().for_each(|p| {
        if *p != Color::BLACK {
            *p = *p * 0.5 + Color::splat(0.5)
        }
    });

    combined_image.write("render_passes_combined.png")?;
    albedo_image.write("render_passes_albedo.png")?;
    // Note that images are always encoded in sRGB when written.
    normal_image.write("render_passes_normal.png")?;
    depth_image.write("render_passes_depth.png")?;
    uv_image.write("render_passes_uv.png")?;

    Ok(())
}
